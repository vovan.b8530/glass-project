<?php

use App\Models\Attribute;
use App\Models\AttributeOption;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttributeOptionsTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    AttributeOption::query()->truncate();

    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    Attribute::query()
      ->each(function ($attribute) {
        factory(AttributeOption::class, 2)->create()->each(function ($attributeOption) use ($attribute) {
          $attributeOption->attribute()->associate($attribute)->save();
        });
      });
  }
}
