<?php

use App\Models\SubCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubCategoriesTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    SubCategory::query()->truncate();

    DB::statement('SET FOREIGN_KEY_CHECKS=1;');


    SubCategory::create([
      'title' => 'Мужские Очки для ПК',
      'category_id' => 1,
      'attribute_option_url' => 'http://',
      'seo_title' => 'Мужские Очки, для ПК',
      'seo_description' => 'Самые кртутые очки, очки для ПК',
    ]);

    SubCategory::create([
      'title' => 'Женские Очки для ПК',
      'attribute_option_url' => 'http://',
      'category_id' => 1,
      'seo_title' => ' Женские Очки, для ПК',
      'seo_description' => 'Самые кртутые очки, очки для ПК',
    ]);

    SubCategory::create([
      'title' => 'Мужские Имидж очки',
      'category_id' => 2,
      'attribute_option_url' => 'http://',
      'seo_title' => 'Мужские Имидж очки',
      'seo_description' => 'Самые современные Имидж очки',
    ]);

    SubCategory::create([
      'title' => 'Женские Имидж очки',
      'category_id' => 2,
      'attribute_option_url' => 'http://',
      'seo_title' => 'Женские Имидж очки',
      'seo_description' => 'Самые современные Имидж очки',
    ]);
  }
}
