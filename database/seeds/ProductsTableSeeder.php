<?php


use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    Product::query()->truncate();

    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    Category::query()
      ->each(function ($category) {
        factory(Product::class, 20)->create()->each(function ($product) use ($category) {
          $product->category()->associate($category)->save();
        });
      });
  }
}
