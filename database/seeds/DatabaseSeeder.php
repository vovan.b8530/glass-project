<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run() {
    $this->call(UserTableSeeder::class);
    $this->call(CategoriesTableSeeder::class);
    $this->call(SubCategoriesTableSeeder::class);
    $this->call(AttributesTableSeeder::class);
    $this->call(AttributeOptionsTableSeeder::class);
    $this->call(ProductsTableSeeder::class);
    $this->call(ProductsTableSeeder::class);
  }
}
