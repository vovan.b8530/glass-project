<?php

use App\Models\Attribute;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttributesTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    Attribute::query()->truncate();

    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    factory(Attribute::class, 4)->create();
  }
}
