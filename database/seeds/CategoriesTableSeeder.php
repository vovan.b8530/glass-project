<?php

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    Category::query()->truncate();

    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    Category::create([
      'title' => 'Очки для ПК',
      'seo_title' => 'Очки, для ПК',
      'seo_description' => 'Самые кртутые очки, очки для ПК',
    ]);

    Category::create([
      'title' => 'Имидж очки',
      'seo_title' => 'Имидж очки',
      'seo_description' => 'Самые современные Имидж очки',
    ]);
  }
}
