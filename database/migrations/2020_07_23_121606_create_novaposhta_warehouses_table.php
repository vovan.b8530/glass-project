<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovaposhtaWarehousesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('novaposhta_warehouses', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->unsignedBigInteger('city_id');
      $table->foreign('city_id')->references('id')->on('novaposhta_cities');

      $table->string('site_key')->nullable();
      $table->string('description_uk')->nullable();
      $table->string('description_ru')->nullable();
      $table->string('phone')->nullable();
      $table->string('type_of_warehouse')->nullable();
      $table->string('ref')->nullable();
      $table->string('number')->nullable();
      $table->string('city_ref')->nullable();
      $table->string('city_description_uk')->nullable();
      $table->string('city_description_ru')->nullable();
      $table->string('longitude')->nullable();
      $table->string('latitude')->nullable();
      $table->string('post_finance')->nullable();
      $table->string('bicycle_parking')->nullable();
      $table->string('payment_access')->nullable();
      $table->string('pos_terminal')->nullable();
      $table->string('international_shipping')->nullable();
      $table->string('total_max_weight_allowed')->nullable();
      $table->string('place_max_weight_allowed')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('novaposhta_warehouses');
  }
}
