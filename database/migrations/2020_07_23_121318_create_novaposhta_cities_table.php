<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovaposhtaCitiesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('novaposhta_cities', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->string('description_uk')->nullable();
      $table->string('description_ru')->nullable();
      $table->string('ref')->nullable();
      $table->string('delivery1')->nullable();
      $table->string('delivery2')->nullable();
      $table->string('delivery3')->nullable();
      $table->string('delivery4')->nullable();
      $table->string('delivery5')->nullable();
      $table->string('delivery6')->nullable();
      $table->string('delivery7')->nullable();
      $table->string('area')->nullable();
      $table->string('settlement_type')->nullable();
      $table->string('is_branch')->nullable();
      $table->string('prevent_entry_new_streets_user')->nullable();
      $table->string('conglomerates')->nullable();
      $table->string('city_id')->nullable();
      $table->string('settlement_type_description_ru')->nullable();
      $table->string('settlement_type_description_uk')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('novaposhta_cities');
  }
}
