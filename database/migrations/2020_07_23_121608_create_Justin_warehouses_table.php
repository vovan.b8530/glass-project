<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJustinWarehousesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('justin_warehouses', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->unsignedBigInteger('city_id');
      $table->foreign('city_id')->references('id')->on('justin_cities');

      $table->string('descr')->nullable();
      $table->string('code')->nullable();
      $table->string('citySCOATOU')->nullable();
      $table->string('address')->nullable();
      $table->string('lat')->nullable();
      $table->string('lng')->nullable();
      $table->string('departNumber')->nullable();
      $table->string('houseNumber')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('justin_warehouses');
  }
}
