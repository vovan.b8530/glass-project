<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('products', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('alias');
      $table->string('title');
      $table->text('description');
      $table->string('product_hash')->nullable();
      $table->float('price')->default(0.00);
      $table->integer('stock_quantity');

      $table->boolean('is_main_page')->default(0);

      $table->unsignedBigInteger('category_id')->nullable();
      $table->foreign('category_id')->references('id')->on('categories');

      $table->unsignedInteger('display_order')->nullable();
      $table->string('seo_title')->nullable();
      $table->text('seo_description')->nullable();

      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('products');
  }
}
