<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJustinCitiesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('justin_cities', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->string('uuid')->nullable();
      $table->string('code')->nullable();
      $table->string('descr')->nullable();
      $table->string('SCOATOU')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('justin_cities');
  }
}
