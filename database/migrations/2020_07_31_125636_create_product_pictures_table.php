<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPicturesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('product_pictures', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->unsignedBigInteger('product_id');
      $table->foreign('product_id')->references('id')->on('products');

      $table->tinyInteger('is_main')->default(0);
      $table->string('picture_file_name');
      $table->string('picture_thumb_file_name')->nullable();

      $table->unsignedInteger('display_order')->default(0);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('product_pictures');
  }
}
