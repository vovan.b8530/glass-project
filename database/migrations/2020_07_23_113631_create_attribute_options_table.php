<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributeOptionsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('attribute_options', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('alias');

      $table->unsignedBigInteger('attribute_id')->nullable();
      $table->foreign('attribute_id')->references('id')->on('attributes');

      $table->string('title');
      $table->string('picture_file_name')->nullable();
      $table->unsignedInteger('display_order')->nullable();

      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('attribute_options');
  }
}
