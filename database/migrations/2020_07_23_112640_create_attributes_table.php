<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('attributes', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('alias');
      $table->string('title');
      $table->unsignedInteger('display_order')->nullable();
      $table->boolean('is_color')->default(false);

      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('attributes');
  }
}
