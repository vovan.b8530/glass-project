<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubCategoriesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('sub_categories', function (Blueprint $table) {
      $table->bigIncrements('id');

      $table->string('alias');
      $table->string('title');

      $table->unsignedBigInteger('category_id')->nullable();
      $table->foreign('category_id')->references('id')->on('categories');

      $table->string('attribute_option_url');
      $table->unsignedInteger('display_order')->default(0);
      $table->string('seo_title')->nullable();
      $table->text('seo_description')->nullable();

      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('sub_categories');
  }
}
