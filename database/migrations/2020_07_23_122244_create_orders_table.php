<?php

use App\Enums\DeliveryTypes;
use App\Enums\OrderStatuses;
use App\Enums\PaymentTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('orders', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('uuid')->nullable();
      $table->string('cookie_hash')->nullable();


      $table->enum('status', OrderStatuses::getValues())->default(OrderStatuses::NEW);
      $table->enum('payment_type', PaymentTypes::getValues())->nullable();

      $table->string('name')->nullable();
      $table->string('phone_number')->nullable();
      $table->string('email')->nullable();

      $table->enum('delivery_type', DeliveryTypes::getValues())->nullable();

      $table->unsignedBigInteger('novaposhta_city_id')->nullable();
      $table->foreign('novaposhta_city_id')->references('id')->on('novaposhta_cities');

      $table->unsignedBigInteger('novaposhta_warehouse_id')->nullable();
      $table->foreign('novaposhta_warehouse_id')->references('id')->on('novaposhta_warehouses');

      $table->unsignedBigInteger('justin_city_id')->nullable();
      $table->foreign('justin_city_id')->references('id')->on('justin_cities');

      $table->unsignedBigInteger('justin_warehouse_id')->nullable();
      $table->foreign('justin_warehouse_id')->references('id')->on('justin_warehouses');

      $table->float('total_amount')->default(0.00);

      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('orders');
  }
}
