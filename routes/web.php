<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
#### TEST ####

Route::group(['prefix' => '/test'], function () {
  Route::get('/getCities', 'TestController@getCities')->name('test.cities');
});


#### SITE ####


#### PAYMENT ####

Route::group(['prefix' => 'payments'], function () {
  Route::group(['prefix' => 'checkout'], function () {
    Route::get('liqpay', 'PaymentController@payViaLiqPay')->name('pay.checkout.liqpay');
  });

  Route::group(['prefix' => 'callback'], function () {
    Route::get('liqpay', 'PaymentController@callbackTransactionViaLiqPay')->name('pay.callback.liqpay');
  });

  Route::group(['prefix' => 'listener'], function () {
    Route::match(['GET', 'POST'], 'liqpay', 'PaymentController@listenerTransactionViaLiqPay')->name('pay.listener.liqpay');
  });
});

#### PAYMENT ####


Route::get('/', 'MainController@index')->name('site.home');

#### PRODUCTS ####

Route::group(['prefix' => 'products'], function () {
  Route::get('/', 'ProductController@index')->name('site.products.index');
  Route::get('/{product:alias}', 'ProductController@show')->name('site.products.show');
  Route::post('/order', 'ProductController@order')->name('site.products.order');
});

#### PRODUCTS ####

#### CART ####

Route::group(['prefix' => 'cart'], function () {
  #### PAGES ####

  Route::get('/', 'CartController@index')->name('cart.index');
  Route::get('/confirm', 'CartController@confirm')->name('cart.confirm');

  #### PAGES ####

  #### ITEMS ####

  Route::put('/edit-item/{orderProduct}', 'CartController@editItem')->name('cart.editItem');
  Route::delete('/delete-item/{orderProduct}', 'CartController@deleteItem')->name('cart.deleteItem');

  #### ITEMS ####

  Route::post('/checkout', 'CartController@checkout')->name('cart.checkout');

  Route::get('/success', 'CartController@success')
    ->name('cart.success')
    ->middleware(\App\Http\Middleware\FlashSuccessOrderMiddleware::class);
});

#### CART ####

#### STATIC ####

Route::get('/about', 'StaticController@about')->name('about');
Route::get('/contacts', 'StaticController@contacts')->name('contacts');
Route::get('/faq', 'StaticController@faq')->name('faq');
Route::get('/delivery', 'StaticController@delivery')->name('delivery');
Route::get('/warranty', 'StaticController@warranty')->name('warranty');
Route::get('/back-product', 'StaticController@backProduct')->name('backProduct');

#### STATIC ####

#### SITE ####


#### ADMIN PANEL ####

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


Route::get('/admin', 'HomeController@index')->name('home');

Route::group(['prefix' => '/admin', 'middleware' => 'auth', 'namespace' => 'Admin'], function () {

  #### CATEGORY ####

  Route::resource('categories', 'CategoryController');
  Route::group(['prefix' => '/categories'], function () {
    Route::post('/reorder', 'CategoryController@reorder');
  });

  #### CATEGORY ####

  #### SUBCATEGORY ####

  Route::resource('sub-categories', 'SubCategoryController');
  Route::group(['prefix' => '/sub-categories'], function () {
    Route::post('/reorder', 'SubCategoryController@reorder');
  });

  #### SUBCATEGORY ####

  #### ATTRIBUTES ####

  Route::resource('attributes', 'AttributeController');
  Route::group(['prefix' => '/attributes'], function () {
    Route::post('/reorder', 'AttributeController@reorder');
  });

  #### ATTRIBUTES ####

  #### ATTRIBUTE OPTIONS ####

  Route::resource('attribute-options', 'AttributeOptionController');
  Route::group(['prefix' => '/attribute-options'], function () {
    Route::post('/reorder', 'AttributeOptionController@reorder');
  });

  #### ATTRIBUTE OPTIONS ####

  #### PRODUCTS ####

  Route::resource('products', 'ProductController');
  Route::group(['prefix' => '/products'], function () {
    Route::post('/reorder', 'ProductController@reorder');
    Route::post('/reorder-picture', 'ProductController@reorderPictures');
  });

  #### PRODUCTS ####

  #### ORDERS ####

  Route::group(['prefix' => 'orders'], function () {
    Route::get('/', 'OrderController@index')->name('orders.index');
    Route::post('/{order}/accept', 'OrderController@accept')->name('order.accept');
    Route::post('/{order}/confirmed', 'OrderController@confirmed')->name('order.confirmed');
    Route::post('/{order}/canceled', 'OrderController@canceled')->name('order.canceled');
  });

  #### ORDERS ####

  #### SEO ####

  Route::resource('seo', 'SeoController')->except([
    'show'
  ]);

  #### SEO ####

});

#### ADMIN PANEL ####