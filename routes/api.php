<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {

  #### DELIVERY ####

  Route::group(['prefix' => 'delivery', 'namespace' => 'Delivery'], function () {
    #### JUSTIN ####

    Route::group(['prefix' => 'justin', 'namespace' => 'Justin'], function () {
      #### CITIES ####

      Route::group(['prefix' => 'cities'], function () {
        Route::get('/', 'CityController@index');
      });

      #### CITIES ####

      #### WAREHOUSES ####

      Route::group(['prefix' => 'warehouses'], function () {
        Route::get('/', 'WarehouseController@index');
      });

      #### WAREHOUSES ####
    });

    #### JUSTIN ####

    #### NOVA POSHTA ####

    Route::group(['prefix' => 'novaposhta', 'namespace' => 'NovaPoshta'], function () {

      #### CITIES ####

      Route::group(['prefix' => 'cities'], function () {
        Route::get('/', 'CityController@index');
      });

      #### CITIES ####

      #### WAREHOUSES ####

      Route::group(['prefix' => 'warehouses'], function () {
        Route::get('/', 'WarehouseController@index');
      });

      #### WAREHOUSES ####
    });

    #### NOVA POSHTA ####
  });

  #### DELIVERY ####
});