<?php

namespace App\Criteria\Builder;

use App\Criteria\Base\FullTextSearchCriteria;
use App\Criteria\Base\SortByStrategyCriteria;

/**
 * Criteria builder for view seo items.
 *
 * @package App\Criteria
 */
class SeoCriteriaBuilder extends BaseCriteriaBuilder {
  /**
   * List of criteria.
   *
   * @return array
   */
  public function getListCriteria(): array {
    return [
      new SortByStrategyCriteria(
        $this->request->get('orderByColumn', 'created_at'),
        $this->request->get('orderByMethod', 'desc')
      ),

      'q' => new FullTextSearchCriteria($this->request->get('q')),
    ];
  }
}