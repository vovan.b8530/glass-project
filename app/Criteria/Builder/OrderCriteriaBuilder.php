<?php

namespace App\Criteria\Builder;

use App\Criteria\Base\SelectAll;
use App\Criteria\Base\SortByStrategyCriteria;
use App\Criteria\Order\WhereStatus;
use App\Criteria\Order\WithDefaultRelationship;

class OrderCriteriaBuilder extends BaseCriteriaBuilder {
  /**
   * @return array
   */
  public function getListCriteria(): array {
    return [
      new SelectAll(),

      new WithDefaultRelationship(),
      'status' => new WhereStatus((int)$this->request->get('status')),

      new SortByStrategyCriteria('created_at', 'desc'),
    ];
  }
}