<?php

namespace App\Criteria\Builder\Justin;

use App\Criteria\Builder\CriteriaBuilder;
use App\Criteria\Justin\WithDefaultRelationship;

class CityCriteriaBuilder extends CriteriaBuilder {
  /**
   * @return array
   */
  public function getListCriteria(): array {
    return $this->unionParentsAndChildrenCriteria(parent::getListCriteria(), [
      new WithDefaultRelationship(),
    ]);
  }
}