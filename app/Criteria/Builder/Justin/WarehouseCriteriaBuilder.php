<?php

namespace App\Criteria\Builder\Justin;

use App\Criteria\Builder\CriteriaBuilder;
use App\Criteria\Justin\Warehouse\WhereCity;

class WarehouseCriteriaBuilder extends CriteriaBuilder {
  /**
   * @return array
   */
  public function getListCriteria(): array {
    return $this->unionParentsAndChildrenCriteria(parent::getListCriteria(), [
      'cityId' => new WhereCity((int)$this->request->get('cityId')),
    ]);
  }
}