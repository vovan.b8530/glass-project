<?php

namespace App\Criteria\Builder;


use App\Criteria\Base\FullTextSearchCriteria;
use App\Criteria\Base\SelectAll;
use App\Criteria\Base\SoftDeleted\OnlyTrashed;
use App\Criteria\Base\SoftDeleted\WithoutTrashed;
use App\Criteria\Base\SortByStrategyCriteria;
use App\Criteria\Product\WhereAttributeOptions;
use App\Criteria\Product\WhereCategory;
use App\Criteria\Product\WithDefaultRelationship;
use Illuminate\Support\Arr;

class ProductCriteriaBuilder extends CriteriaBuilder {
  /**
   * @return array
   */
  public function getListCriteria(): array {
    return [
      new SelectAll(),
      new WithDefaultRelationship(),
      'categoryId' => new WhereCategory((int)$this->request->get('categoryId')),
      'attributeOptionsIds' => new WhereAttributeOptions(Arr::wrap($this->request->get('attributeOptionsIds', []))),

      'q' => new FullTextSearchCriteria($this->request->get('q')),
      ($this->request->has('onlyTrashed') ? new OnlyTrashed() : new WithoutTrashed()),
      ($this->request->has('price') ? new SortByStrategyCriteria('price', $this->request->get('price')) : new SortByStrategyCriteria('display_order', 'asc')),
      new SortByStrategyCriteria('category_id', 'asc'),
    ];
  }
}