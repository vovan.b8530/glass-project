<?php

namespace App\Criteria\Builder\NovaPoshta;

use App\Criteria\Builder\CriteriaBuilder;
use App\Criteria\NovaPoshta\WithDefaultRelationship;

class CityCriteriaBuilder extends CriteriaBuilder {
  /**
   * @return array
   */
  public function getListCriteria(): array {
    return $this->unionParentsAndChildrenCriteria(parent::getListCriteria(), [
      new WithDefaultRelationship(),
    ]);
  }
}