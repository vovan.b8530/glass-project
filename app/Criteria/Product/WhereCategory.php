<?php

namespace App\Criteria\Product;

use App\Criteria\ICriteria;
use App\Models\Scopes\CriteriaScopes;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;

class WhereCategory implements ICriteria {
  /**
   * @var int|null
   */
  public $categoryId;

  /**
   * WhereExerciseCategory constructor.
   * @param int $categoryId
   */
  public function __construct(int $categoryId) {
    $this->categoryId = $categoryId;
  }

  /**
   * @param CriteriaScopes|EloquentBuilder|QueryBuilder $builder
   * @return CriteriaScopes|EloquentBuilder|QueryBuilder|mixed
   */
  public function apply($builder) {
    return $builder->when(!empty($this->categoryId), function ($builder) {
      /* @var CriteriaScopes|EloquentBuilder|QueryBuilder $builder */
      return $builder->where($builder->withAlias('category_id'), $this->categoryId);
    });
  }
}