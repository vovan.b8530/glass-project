<?php

namespace App\Criteria\Product;

use App\Criteria\ICriteria;
use App\Models\AttributeOption;
use App\Models\ProductAttributeOption;
use App\Models\Scopes\CriteriaScopes;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;

class WhereAttributeOptions implements ICriteria {
  /**
   * @var array
   */
  public $attributeOptionsIds;

  /**
   * WhereAttributeOptions constructor.
   *
   * @param array $attributeOptionsIds
   */
  public function __construct(array $attributeOptionsIds) {
    $this->attributeOptionsIds = $attributeOptionsIds;
  }

  /**
   * @param CriteriaScopes|EloquentBuilder|QueryBuilder $builder
   * @return CriteriaScopes|EloquentBuilder|QueryBuilder|mixed
   */
  public function apply($builder) {
    return $builder->when(!empty($this->attributeOptionsIds), function ($builder) {
      /* @var CriteriaScopes|EloquentBuilder|QueryBuilder $builder */

      $productAttributeOptionTbName = ProductAttributeOption::getTableName();
      $attributeOptionTbName = AttributeOption::getTableName();

      return $builder
        ->join($productAttributeOptionTbName, "{$productAttributeOptionTbName}.product_id", '=', $builder->withAlias('id'))
        ->join($attributeOptionTbName, "{$productAttributeOptionTbName}.attribute_option_id", '=', "{$attributeOptionTbName}.id")
        ->whereIn("{$attributeOptionTbName}.id", $this->attributeOptionsIds)
        ->groupBy($builder->withAlias('id'));
    });
  }
}
