<?php

namespace App\Criteria\Order;

use App\Criteria\ICriteria;
use App\Models\Scopes\CriteriaScopes;
use App\Models\Scopes\OrderScopes;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;

class WhereStatus implements ICriteria {
  /**
   * @var int
   */
  public $status;

  /**
   * WhereStatus constructor.
   * @param int $status
   */
  public function __construct(int $status) {
    $this->status = $status;
  }

  /**
   * @param CriteriaScopes|EloquentBuilder|QueryBuilder|OrderScopes $builder
   * @return CriteriaScopes|EloquentBuilder|QueryBuilder|OrderScopes
   */
  public function apply($builder) {
    return $builder->when(!empty($this->status), function ($builder) {
      /* @var CriteriaScopes|EloquentBuilder|QueryBuilder $builder */
      return $builder->where($builder->withAlias('status'), $this->status);
    });
  }
}