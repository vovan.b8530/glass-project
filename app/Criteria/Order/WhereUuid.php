<?php

namespace App\Criteria\Order;

use App\Criteria\ICriteria;
use App\Models\Scopes\CriteriaScopes;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;

class WhereUuid implements ICriteria {
  /**
   * @var string
   */
  protected $uuid;

  /**
   * WhereUuid constructor.
   *
   * @param string $uuid
   */
  public function __construct(string $uuid) {
    $this->uuid = $uuid;
  }

  /**
   * @return string
   */
  public function getUuid(): string {
    return $this->uuid;
  }

  /**
   * @param CriteriaScopes|EloquentBuilder|QueryBuilder $builder
   * @return CriteriaScopes|EloquentBuilder|QueryBuilder
   */
  public function apply($builder) {
    return $builder->where($builder->withAlias('uuid'), $this->uuid);
  }
}