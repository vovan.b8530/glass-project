<?php

namespace App\Payments\Exceptions;

use Illuminate\Http\Exceptions\HttpResponseException;

class PaymentException extends HttpResponseException {

}