<?php

namespace App\Payments;

use App\DTO\OrderDto;
use App\Models\Order;
use App\Payments\DTO\PaymentDto;
use App\Payments\Exceptions\PaymentException;
use App\Services\Actions\CartServiceAction;

class LiqPayPayment implements IPayment {
  /**
   * @var CartServiceAction
   */
  protected $cartService;

  /**
   * LiqPayPayment constructor.
   *
   * @param CartServiceAction $cartService
   */
  public function __construct(CartServiceAction $cartService) {
    $this->cartService = $cartService;
  }

  /**
   * @param Order $order
   * @param PaymentDto|OrderDto $paymentDto
   *
   * @return bool
   *
   * @throws PaymentException
   */
  public function pay(Order $order, PaymentDto $paymentDto): bool {
    $this->cartService->preCheckout($order, $paymentDto);

    throw new PaymentException(
      response()->json([
        'status' => 'redirect',
        'redirect_uri' => route('pay.checkout.liqpay'),
      ])
    );
  }
}