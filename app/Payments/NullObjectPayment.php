<?php

namespace App\Payments;

use App\Models\Order;
use App\Payments\DTO\PaymentDto;
use App\Payments\Exceptions\PaymentException;

class NullObjectPayment implements IPayment {
  /**
   * @param Order $order
   * @param PaymentDto $paymentDto
   *
   * @return bool
   *
   * @throws PaymentException
   */
  public function pay(Order $order, PaymentDto $paymentDto): bool {
    return true;
  }
}