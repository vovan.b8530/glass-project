<?php

namespace App\Factory;

use App\Enums\PaymentTypes;
use App\Payments\IPayment;
use App\Payments\LiqPayPayment;
use App\Payments\NullObjectPayment;

class PaymentFactory {
  /**
   * @param int|null $paymentType
   * @return IPayment
   */
  public static function createPayment(?int $paymentType): IPayment {
    switch ($paymentType) {
      case PaymentTypes::CARD:
        {
          return app(LiqPayPayment::class);
        }
        break;
    }

    return new NullObjectPayment();
  }
}