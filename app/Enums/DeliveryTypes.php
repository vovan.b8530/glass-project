<?php


namespace App\Enums;


class DeliveryTypes extends BaseEnum {
  const NOVA_POSHTA = 1;
  const JUSTIN = 2;

  public static $LABELS = [
    self::NOVA_POSHTA => 'Новая почта',
    self::JUSTIN => 'Justin',
  ];
}