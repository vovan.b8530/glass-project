<?php


namespace App\Enums;


class OrderStatuses extends BaseEnum {
  const NEW = 1;
  const ACCEPT = 2;
  const CONFIRMED = 3;
  const CANCELED = 4;

  public static $LABELS = [
    self::NEW => 'new',
    self::ACCEPT => 'accept',
    self::CONFIRMED => 'confirmed',
    self::CANCELED => 'canceled',
  ];

  public static $LABELS_RU = [
    self::NEW => 'Новый',
    self::ACCEPT => 'Подтвержденный',
    self::CONFIRMED => 'Закрытый',
    self::CANCELED => 'Отмененный',
  ];
}