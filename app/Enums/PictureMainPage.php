<?php


namespace App\Enums;


class PictureMainPage extends BaseEnum {
  const NOT_MAIN_PAGE = 0;
  const MAIN_PAGE = 1;

  public static $LABELS = [
    self::NOT_MAIN_PAGE => 'НЕ на главной',
    self::MAIN_PAGE => 'На главную',
  ];
}