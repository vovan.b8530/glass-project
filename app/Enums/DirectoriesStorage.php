<?php

namespace App\Enums;

class DirectoriesStorage extends BaseEnum {
  const PRODUCT_PICTURE_PATH = 'products';
  const ATTRIBUTE_OPTION_PICTURE_PATH = 'attribute-options';
}