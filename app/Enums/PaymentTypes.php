<?php


namespace App\Enums;


class PaymentTypes extends BaseEnum {
  const CASH = 1;
  const CARD = 2;

  public static $LABELS = [
    self::CASH => 'Наличные',
    self::CARD => 'Карта',
  ];
}