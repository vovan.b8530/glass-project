<?php

namespace App\Enums\Payments\LiqPay;

use App\Enums\BaseEnum;

class BaseOptions extends BaseEnum {
  const VERSION = 3;
  const CURRENCY = 'UAH';
}