<?php

namespace App\Enums\Payments\LiqPay;

use Illuminate\Support\Arr;

class TransactionStatuses extends TransactionSuccessStatuses {
  const ERROR = 'error';
  const FAILURE = 'failure';
  const REVERSED = 'reversed';
  const SUBSCRIBED = 'subscribed';
  const SUCCESS = 'success';
  const UNSUBSCRIBED = 'unsubscribed';
  const DS3_VERIFY = '3ds_verify';
  const CAPTCHA_VERIFY = 'captcha_verify';
  const CVV_VERIFY = 'cvv_verify';
  const IVR_VERIFY = 'ivr_verify';
  const OTP_VERIFY = 'otp_verify';
  const PASSWORD_VERIFY = 'password_verify';
  const PHONE_VERIFY = 'phone_verify';
  const PIN_VERIFY = 'pin_verify';
  const RECEIVER_VERIFY = 'receiver_verify';
  const SENDER_VERIFY = 'sender_verify';
  const SENDERAPP_VERIFY = 'senderapp_verify';
  const WAIT_QR = 'wait_qr';
  const WAIT_SENDER = 'wait_sender';
  const CASH_WAIT = 'cash_wait';
  const HOLD_WAIT = 'hold_wait';
  const INVOICE_WAIT = 'invoice_wait';
  const PREPARED = 'prepared';
  const PROCESSING = 'processing';
  const WAIT_ACCEPT = 'wait_accept';
  const WAIT_CARD = 'wait_card';
  const WAIT_COMPENSATION = 'wait_compensation';
  const WAIT_LC = 'wait_lc';
  const WAIT_RESERVE = 'wait_reserve';
  const WAIT_SECURE = 'wait_secure';


  /**
   * @param string|null $status
   * @return array|string|null
   */
  public static function labels(?string $status = null) {
    $statuses = [
      static::ERROR => 'Неуспешный платеж. Некорректно заполнены данные.',
      static::FAILURE => 'Неуспешный платеж.',
      static::REVERSED => 'Платеж возвращен.',
      static::SUBSCRIBED => '	Подписка успешно оформлена.',
      static::SUCCESS => 'Успешный платеж.',
      static::UNSUBSCRIBED => 'Подписка успешно деактивирована.',
      static::DS3_VERIFY => 'Требуется 3DS верификация.Для завершения платежа, требуется выполнить 3ds_verify.',
      static::CAPTCHA_VERIFY => 'Ожидается подтверждение captcha.',
      static::CVV_VERIFY => 'Требуется ввод CVV карты отправителя.Для завершения платежа, требуется выполнить cvv_verify.',
      static::IVR_VERIFY => '	Ожидается подтверждение звонком ivr.',
      static::OTP_VERIFY => 'Требуется OTP подтверждение клиента. OTP пароль отправлен на номер телефона Клиента.Для завершения платежа, требуется выполнить otp_verify.',
      static::PASSWORD_VERIFY => 'Ожидается подтверждение пароля приложения Приват24.',
      static::PHONE_VERIFY => 'Ожидается ввод телефона клиентом.Для завершения платежа, требуется выполнить phone_verify',
      static::PIN_VERIFY => 'Ожидается подтверждение pin-code.',
      static::RECEIVER_VERIFY => 'Требуется ввод данных получателя.Для завершения платежа, требуется выполнить receiver_verify.',
      static::SENDER_VERIFY => 'Требуется ввод данных отправителя.Для завершения платежа, требуется выполнить sender_verify.',
      static::SENDERAPP_VERIFY => 'Ожидается подтверждение в приложении SENDER.',
      static::WAIT_QR => 'Ожидается сканирование QR-кода клиентом.',
      static::WAIT_SENDER => 'Ожидается подтверждение оплаты клиентом в приложении Privat24/SENDER.',
      static::CASH_WAIT => 'Ожидается оплата наличными в ТСО.',
      static::HOLD_WAIT => 'Сумма успешно заблокирована на счету отправителя.',
      static::INVOICE_WAIT => 'Инвойс создан успешно, ожидается оплата.',
      static::PREPARED => 'Платеж создан, ожидается его завершение отправителем.',
      static::PROCESSING => 'Платеж обрабатывается.',
      static::WAIT_ACCEPT => 'Деньги с клиента списаны, но магазин еще не прошел проверку. Если магазин не пройдет активацию в течение 180 дней, платежи будут автоматически отменены.',
      static::WAIT_CARD => 'Не установлен способ возмещения у получателя.',
      static::WAIT_COMPENSATION => 'Платеж успешный, будет зачислен в ежесуточной проводке.',
      static::WAIT_LC => 'Аккредитив. Деньги с клиента списаны, ожидается подтверждение доставки товара.',
      static::WAIT_RESERVE => '	Средства по платежу зарезервированы для проведения возврата по ранее поданной заявке.',
      static::WAIT_SECURE => '	Платеж на проверке.',
    ];

    if ($status)
      return Arr::get($statuses, $status);

    return $statuses;
  }
}