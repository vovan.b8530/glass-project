<?php

namespace App\Enums\Payments\LiqPay;

use App\Enums\BaseEnum;

class TransactionSuccessStatuses extends BaseEnum {
  const SANDBOX = 'sandbox';
  const SUCCESS = 'success';
  const WAIT_ACCEPT = 'wait_accept';
  const WAIT_LC = 'wait_lc';
}