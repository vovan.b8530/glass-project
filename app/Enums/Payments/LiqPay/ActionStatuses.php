<?php

namespace App\Enums\Payments\LiqPay;

use App\Enums\BaseEnum;

class ActionStatuses extends BaseEnum {
  const ACTION_PAY = 'pay';
  const ACTION_STATUS = 'status';
}