<?php

namespace App\Enums;

class OrderingDirection extends BaseEnum {
  const UP = 'up';
  const DOWN = 'down';
}