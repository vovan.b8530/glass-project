<?php

namespace App\Http\Requests;

use App\DTO\OrderDto;
use App\Enums\DeliveryTypes;
use App\Enums\PaymentTypes;
use App\Models\JustinCity;
use App\Models\JustinWarehouse;
use App\Models\NovaPoshtaCity;
use App\Models\NovaPoshtaWarehouse;
use Illuminate\Validation\Rule;

class CheckoutRequest extends JsonFormRequest {
  /**
   * @param bool $isTrue
   * @return \Closure
   */
  protected function requiredIfDeliveryTypeIsNovaPoshta(bool $isTrue = true): \Closure {
    return function () use ($isTrue) {
      return ($this->get('delivery_type') == DeliveryTypes::NOVA_POSHTA) === $isTrue;
    };
  }

  /**
   * @param bool $isTrue
   * @return \Closure
   */
  protected function requiredIfDeliveryTypeIsJustin(bool $isTrue = true): \Closure {
    return function () use ($isTrue) {
      return ($this->get('delivery_type') == DeliveryTypes::JUSTIN) === $isTrue;
    };
  }

  /**
   * @return \Closure
   */
  protected function requiredIfIsNotQuickly(): \Closure {
    return function () {
      return !$this->has('_quickly');
    };
  }

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * @return OrderDto
   */
  public function createdDto(): OrderDto {
    return OrderDto::createFromArray($this->all());
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [

      'name' => [
        'required',
        'max:255',
        'regex:/^[А-Яа-яЁё ]{1,}$/iu',
      ],

      'phone_number' => [
        'required',
        'string',
        'max:255',
      ],

      'email' => [
        Rule::requiredIf($this->requiredIfIsNotQuickly()),
        'nullable',
        'string',
        'max:255',
      ],

      'payment_type' => [
        Rule::requiredIf($this->requiredIfIsNotQuickly()),
        'nullable',
        'integer',
        Rule::in(PaymentTypes::getValues()),
      ],

      'order_id' => [
        'nullable',
        'string',
      ],

      'delivery_type' => [
        Rule::requiredIf($this->requiredIfIsNotQuickly()),
        'nullable',
        'integer',
        Rule::in(DeliveryTypes::getValues())
      ],

      'novaposhta_city_id' => [
        Rule::requiredIf($this->requiredIfDeliveryTypeIsNovaPoshta()),

        'nullable',
        'integer',
        Rule::exists(NovaPoshtaCity::getTableName(), 'id')
      ],

      'novaposhta_warehouse_id' => [
        Rule::requiredIf($this->requiredIfDeliveryTypeIsNovaPoshta()),

        'nullable',
        'integer',
        Rule::exists(NovaPoshtaWarehouse::getTableName(), 'id'),
      ],

      'justin_city_id' => [
        Rule::requiredIf($this->requiredIfDeliveryTypeIsJustin()),

        'nullable',
        'integer',
        Rule::exists(JustinCity::getTableName(), 'id')
      ],

      'justin_warehouse_id' => [
        Rule::requiredIf($this->requiredIfDeliveryTypeIsJustin()),

        'nullable',
        'integer',
        Rule::exists(JustinWarehouse::getTableName(), 'id')
      ]
    ];
  }

  public function attributes() {
    return [
      'payment_type' => 'Тип оплаты',
      'delivery_type' => 'Тип доставки',
      'name' => 'Имя Фамилия',
      'phone_number' => 'Телефон',
      'email' => 'Почта',
      'novaposhta_city_id' => 'Город Новая почта',
      'novaposhta_warehouse_id' => 'Отделение Новая почта',
      'justin_city_id' => 'Город Justin',
      'justin_warehouse_id' => 'Отделение Justin',
    ];
  }
}
