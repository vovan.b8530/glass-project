<?php

namespace App\Http\Requests;

use App\DTO\ProductDto;
use App\Enums\PictureMainPage;
use App\Models\Category;
use App\Models\ProductPicture;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'title' => ['required', 'string', 'max:255'],
      'description' => ['nullable', 'string'],
      'product_hash' => ['nullable', 'string'],
      'price' => ['numeric', 'nullable'],
      'is_main_page' => ['nullable', 'integer', Rule::in(PictureMainPage::getValues())],
      'category_id' => [
        'integer',
        'required',
        Rule::exists((new Category())->getTable(), 'id'),
      ],
      'seo_title' => ['nullable', 'string', 'max:255'],
      'seo_description' => ['nullable', 'string'],
      'files' => ['array', 'nullable'],
      'files.*' => ['image', 'max:5000', 'nullable'],
      'mainFile' => ['image', 'max:5000', 'nullable'],
      'pictures_id' => ['array', 'nullable'],
      'pictures_id.*' => ['integer', 'nullable', Rule::exists((new ProductPicture())->getTable(), 'id')],
      'attrs' => ['array', 'nullable', 'required_with:attrOptions'],
      'attrs.*' => ['integer', 'nullable'],
      'attrOptions' => ['array', 'nullable', 'required_with:attrs'],
      'productIds' => ['array', 'nullable'],
      'attrOptions.*' => ['integer', 'nullable'],
    ];
  }

  /**
   * @return ProductDto
   */
  public function createDto(): ProductDto {
    return ProductDto::createFromArray($this->all());
  }

  /**
   * @return array|string[]
   */
  public function messages() {
    return [
      'title.required' => 'Название продукта обязательно!',
    ];
  }
}
