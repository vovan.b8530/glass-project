<?php

namespace App\Http\Requests;

use App\DTO\SubCategoryDto;
use Illuminate\Foundation\Http\FormRequest;

class SubCategoryRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'title' => ['required', 'string', 'max:255'],
      'category_id' => ['required', 'integer'],
      'attribute_option_url' => ['required', 'url'],
      'seo_title' => ['string', 'nullable', 'max:255'],
      'seo_description' => ['string', 'nullable', 'max:255'],
    ];
  }

  /**
   * @return SubCategoryDto
   */
  public function createDto(): SubCategoryDto {
    return SubCategoryDto::createFromArray($this->all());
  }

  /**
   * @return array|string[]
   */
  public function messages() {
    return [
      'title.required' => 'Название подкатегории обязательно!',
      'category_id.required' => 'Родительская категория обязательна!',
    ];
  }
}
