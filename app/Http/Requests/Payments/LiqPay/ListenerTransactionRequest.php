<?php

namespace App\Http\Requests\Payments\LiqPay;

use App\Http\Requests\JsonFormRequest;
use App\Rules\Payments\LiqPay\DataVerificationRule;
use App\Rules\Payments\LiqPay\SignatureVerificationRule;
use App\Support\LiqPay\Helpers\LiqPayDataParser;

class ListenerTransactionRequest extends JsonFormRequest {
  use LiqPayDataParser;

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'signature' => [
        'required',
        'string',
        new SignatureVerificationRule($this->get('data')),
      ],

      'data' => [
        'required',
        'string',

        new DataVerificationRule(),
      ],
    ];
  }

  /**
   * @return array
   */
  public function getData(): array {
    return $this->parseData($this->get('data'));
  }
}
