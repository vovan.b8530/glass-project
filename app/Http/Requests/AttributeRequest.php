<?php

namespace App\Http\Requests;

use App\DTO\AttributeDto;
use Illuminate\Foundation\Http\FormRequest;

class AttributeRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'title' => ['required', 'string', 'max:255'],
      'is_color' => ['nullable', 'boolean'],
    ];
  }

  /**
   * @return AttributeDto
   */
  public function createDto(): AttributeDto {
    return AttributeDto::createFromArray($this->all());
  }

  /**
   * @return array|string[]
   */
  public function messages() {
    return [
      'title.required' => 'Название атрибута обязательно!',
    ];
  }
}
