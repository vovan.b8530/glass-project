<?php

namespace App\Http\Requests;

use App\DTO\AttributeOptionDto;
use App\Models\Attribute;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AttributeOptionRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'title' => ['required', 'string', 'max:255'],
      'attribute_id' => [
        'integer',
        'required',
        Rule::exists((new Attribute())->getTable(), 'id'),
      ],
      'file' => ['file', 'nullable'],
      'deletePicture' => ['integer', 'nullable']
    ];
  }

  /**
   * @return AttributeOptionDto
   */
  public function createDto(): AttributeOptionDto {
    return AttributeOptionDto::createFromArray(array_merge($this->all(), [
      'file' => $this->file('file'),
    ]));
  }

  /**
   * @return array|string[]
   */
  public function messages() {
    return [
      'title.required' => 'Название атрибут опции обязательно!',
    ];
  }
}
