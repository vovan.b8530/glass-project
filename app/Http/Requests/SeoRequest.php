<?php

namespace App\Http\Requests;

use App\DTO\SeoDto;
use App\Models\Seo;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class SeoRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'url' => [
        'required',
        'string',
        'max:4000',
        'url',
        Rule
          ::unique((new Seo())->getTable(), 'url')
          ->ignore(Arr::get($this->route('seo'), 'id')),
      ],
      'title' => ['required', 'string', 'max:255'],
      'description' => ['required', 'string'],
    ];
  }

  /**
   * Prepare the data for validation.
   *
   * @return void
   */
  protected function prepareForValidation()
  {
    $this->merge([
      'url' => rtrim( $this->url, '/'),
    ]);
  }

  /**
   * @return SeoDto
   */
  public function createDto(): SeoDto {
    return SeoDto::createFromArray($this->all());
  }

  /**
   * @return array|string[]
   */
  public function messages() {
    return [
      'url.required' => 'Url перенаправления обязательно!',
      'title.required' => 'Название  обязательно!',
      'description.required' => 'Описание обязательно!',
    ];
  }
}
