<?php

namespace App\Http\Requests;

use App\DTO\CategoryDto;
use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return [
      'title' => ['required', 'string', 'max:255'],
      'seo_title' => ['string', 'nullable', 'max:255'],
      'seo_description' => ['string', 'nullable', 'max:255'],
    ];
  }

  /**
   * @return CategoryDto
   */
  public function createDto(): CategoryDto {
    return CategoryDto::createFromArray($this->all());
  }

  /**
   * @return array|string[]
   */
  public function messages() {
    return [
      'title.required' => 'Название категории обязательно!',
    ];
  }
}
