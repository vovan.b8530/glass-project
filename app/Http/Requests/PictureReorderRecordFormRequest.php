<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Validation\Rule;

class PictureReorderRecordFormRequest extends ReorderRecordFormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    return array_merge(parent::rules(), [
      'currentProductId' => [
        'required',
        'integer',
        Rule::exists((new Product())->getTable(), 'id'),
        'same:desiredProductId'
      ],
      'desiredProductId' => [
        'required',
        'integer',
        Rule::exists((new Product())->getTable(), 'id'),
        'same:currentProductId'
      ]
    ]);
  }

}
