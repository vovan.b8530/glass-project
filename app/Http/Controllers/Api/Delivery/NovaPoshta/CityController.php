<?php

namespace App\Http\Controllers\Api\Delivery\NovaPoshta;

use App\Criteria\Builder\NovaPoshta\CityCriteriaBuilder;
use App\Http\Controllers\Controller;
use App\Http\Resources\NovaPoshta\NovaPoshtaCityResource;
use App\Models\NovaPoshtaCity;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CityController extends Controller {
  /**
   * @param CityCriteriaBuilder $criteriaBuilder
   * @return AnonymousResourceCollection
   */
  public function index(CityCriteriaBuilder $criteriaBuilder) {
    return NovaPoshtaCityResource::collection(
      NovaPoshtaCity::filterToPaginate(
        $criteriaBuilder, $criteriaBuilder->max(), $criteriaBuilder->page()));
  }
}