<?php

namespace App\Http\Controllers\Api\Delivery\NovaPoshta;

use App\Criteria\Builder\NovaPoshta\WarehouseCriteriaBuilder;
use App\Http\Controllers\Controller;
use App\Http\Resources\NovaPoshta\NovaPoshtaWarehouseResource;
use App\Models\NovaPoshtaWarehouse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class WarehouseController extends Controller {
  /**
   * @param WarehouseCriteriaBuilder $criteriaBuilder
   * @return AnonymousResourceCollection
   */
  public function index(WarehouseCriteriaBuilder $criteriaBuilder) {
    return NovaPoshtaWarehouseResource::collection(
      NovaPoshtaWarehouse::filterToPaginate(
        $criteriaBuilder, $criteriaBuilder->max(), $criteriaBuilder->page()
      )
    );
  }
}