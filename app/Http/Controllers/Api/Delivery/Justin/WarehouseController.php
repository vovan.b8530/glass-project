<?php

namespace App\Http\Controllers\Api\Delivery\Justin;

use App\Criteria\Builder\Justin\WarehouseCriteriaBuilder;
use App\Http\Controllers\Controller;
use App\Http\Resources\Justin\JustinWarehouseResource;
use App\Models\JustinWarehouse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class WarehouseController extends Controller {
  /**
   * @param WarehouseCriteriaBuilder $criteriaBuilder
   * @return AnonymousResourceCollection
   */
  public function index(WarehouseCriteriaBuilder $criteriaBuilder) {
    return JustinWarehouseResource::collection(
      JustinWarehouse::filterToPaginate(
        $criteriaBuilder, $criteriaBuilder->max(), $criteriaBuilder->page()
      )
    );
  }
}