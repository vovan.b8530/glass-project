<?php

namespace App\Http\Controllers\Api\Delivery\Justin;

use App\Criteria\Builder\Justin\CityCriteriaBuilder;
use App\Http\Controllers\Controller;
use App\Http\Resources\Justin\JustinCityResource;
use App\Models\JustinCity;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CityController extends Controller {
  /**
   * @param CityCriteriaBuilder $criteriaBuilder
   * @return AnonymousResourceCollection
   */
  public function index(CityCriteriaBuilder $criteriaBuilder) {
    return JustinCityResource::collection(
      JustinCity::filterToPaginate(
        $criteriaBuilder, $criteriaBuilder->max(), $criteriaBuilder->page()));
  }
}