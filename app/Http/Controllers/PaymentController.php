<?php

namespace App\Http\Controllers;

use App\Criteria\Builder\CartCriteriaBuilder;
use App\Enums\Payments\LiqPay\TransactionStatuses;
use App\Http\Requests\Payments\LiqPay\ListenerTransactionRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Services\Actions\CartServiceAction;
use App\Services\Payment\LiqPayService;

class PaymentController {
  /**
   * @var LiqPayService
   */
  protected $liqpayPayer;

  /**
   * @var CartServiceAction
   */
  protected $cartService;

  /**
   * PaymentController constructor.
   *
   * @param LiqPayService $liqpayPayer
   * @param CartServiceAction $cartService
   */
  public function __construct(LiqPayService $liqpayPayer, CartServiceAction $cartService) {
    $this->liqpayPayer = $liqpayPayer;
    $this->cartService = $cartService;
  }

  /**
   * @param CartCriteriaBuilder $criteriaBuilder
   *
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function payViaLiqPay(CartCriteriaBuilder $criteriaBuilder) {
    /* @var Order $order */
    $order = Order::findOneOrFail($criteriaBuilder);

    $transactionData = $this->liqpayPayer->buildTransactionData($order);

    return view('payment.liqpay-checkout', ['transactionData' => $transactionData]);
  }

  /**
   * @param CartCriteriaBuilder $criteriaBuilder
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function callbackTransactionViaLiqPay(CartCriteriaBuilder $criteriaBuilder) {
    /* @var Order $order */
    $order = Order::findOneOrFail($criteriaBuilder);

    $transactionStatus = $this->liqpayPayer->checkStatus($order);

    if (!$transactionStatus->isCorrect($order)) {
      return view('cart.fail', [
        'order' => OrderResource::rawData($order, null, false),
        'message' => TransactionStatuses::labels($transactionStatus->getStatus()),
      ]);
    }

    $order = $this->cartService->checkoutViaPay($order);

    return view('cart.success', [
      'order' => OrderResource::rawData($order, null, false),
    ]);
  }

  /**
   * @param ListenerTransactionRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function listenerTransactionViaLiqPay(ListenerTransactionRequest $request) {
    $order = $request->retrieveOrder($request->getData());

    if ($order) {
      $transactionStatus = $this->liqpayPayer->checkStatus($order);

      if ($transactionStatus->isCorrect($order)) {
        $this->cartService->checkoutViaPay($order);

        return response()->json(['status' => 'success']);
      }

      return response()->json(['status' => $transactionStatus->getStatus()], 422);
    }

    return response()->json(['status' => 'failed'], 422);
  }
}