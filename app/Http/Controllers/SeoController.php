<?php

namespace App\Http\Controllers;

use App\Criteria\Builder\CriteriaCollection;
use App\Criteria\Seo\WhereRedirectUri;
use App\Models\Seo;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Cache;

/**
 * Controller for detecting and set seo meta tags for pages.
 *
 * @package App\Http\Controllers
 */
class SeoController extends BaseController {
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests, SEOTools;

  /**
   * @var Seo|null
   */
  protected $seoEntity;

  /**
   * SeoController constructor.
   * Search seo item by absolute url and set meta tags.
   *
   * @param Request $request
   * @return void
   */
  public function __construct(Request $request) {
    $fullUrl = $request->fullUrl();

    /* @var Seo $seo */
    $this->seoEntity = Cache::remember("seoPage:{$fullUrl}", 1440, function () use ($fullUrl) {
      return Seo::findOne(new CriteriaCollection([
        new WhereRedirectUri($fullUrl),
      ]));
    });

    if ($this->seoEntity) {
      $this->seo()->setTitle($this->seoEntity->title);
      $this->seo()->setDescription($this->seoEntity->description);
      $this->seo()->opengraph()->setTitle($this->seoEntity->title);
      $this->seo()->opengraph()->setDescription($this->seoEntity->description);
    }else{
      $this->seo()->setTitle(config('app.name'));
    }
  }

  /**
   * @return bool
   */
  public function hasSeoEntity(): bool {
    return !empty($this->seoEntity);
  }

  /**
   * @param Model $entity
   */
  public function setByEntity(Model $entity) {
    $this->seo()->setTitle($entity->seo_title ?: $entity->title);
    $this->seo()->setDescription($entity->seo_description ?: $entity->description);
    $this->seo()->opengraph()->setTitle($entity->seo_title ?: $entity->title);
    $this->seo()->opengraph()->setDescription($entity->seo_description ?: $entity->description);
  }
}
