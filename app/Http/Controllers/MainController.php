<?php

namespace App\Http\Controllers;

use App\Criteria\Builder\ProductCriteriaBuilder;
use App\Criteria\Product\WithDefaultRelationship;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class MainController extends SeoController {
  /**
   * MainController constructor.
   * @param Request $request
   */
  public function __construct(Request $request) {
    parent::__construct($request);
  }

  /**
   * @param ProductCriteriaBuilder $criteriaBuilder
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function index(ProductCriteriaBuilder $criteriaBuilder) {
    $products = Product::query()->where('is_main_page', '=', 1)->latest()->limit(3)->get();
    $products->load(WithDefaultRelationship::relations());
    return view('main.index', [
      'products' => ProductResource::rawList($products, $criteriaBuilder->getRequest(), false),
    ]);
  }
}
