<?php

namespace App\Http\Controllers;

use App\Criteria\Builder\ProductCriteriaBuilder;
use App\Criteria\Product\WithDefaultRelationship;
use App\Helpers\Auth\AuthenticatedUser;
use App\Http\Requests\ProductOrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Http\Resources\ProductResource;
use App\Services\Actions\CartServiceAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ProductController extends SeoController {
  /**
   * @var CartServiceAction
   */
  protected $cartService;

  /**
   * ProductController constructor.
   *
   * @param CartServiceAction $cartService
   * @param Request $request
   */
  public function __construct(CartServiceAction $cartService, Request $request) {
    $this->cartService = $cartService;

    parent::__construct($request);
  }

  /**
   * @param ProductCriteriaBuilder $criteriaBuilder
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function index(ProductCriteriaBuilder $criteriaBuilder) {
    $paginator = Product::filterToPaginate($criteriaBuilder, $criteriaBuilder->max(), $criteriaBuilder->page());

    $breadCategory = request()->has('categoryId')
      ? Category::whereId(request()->get('categoryId'))->first()
      : '';
    return view('products.index', [
      'paginator' => $paginator->appends($criteriaBuilder->getRequest()->all()),
      'products' => ProductResource::rawPaginator($paginator, $criteriaBuilder->getRequest()),
      'breadCategory' => $breadCategory,
    ]);
  }

  /**
   * @param Product $product
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function show(Product $product) {
    $product->load(WithDefaultRelationship::relations());

    if(!$this->hasSeoEntity()) $this->setByEntity($product);

    return view('products.show', [
      'product' => ProductResource::rawData($product, null, false),
    ]);
  }

  /**
   * @param ProductOrderRequest $request
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function order(ProductOrderRequest $request) {
    $order = Order::findOrReturnNewByCookieHash(AuthenticatedUser::getOrGenerateHashInSession($request));

    $order = $this->cartService->putProduct($order, $request->getProduct());

    return response()->json([
      'status' => 'success',
      'order' => new OrderResource($order),
      'redirectTo' => route('cart.index'),
    ]);
  }
}
