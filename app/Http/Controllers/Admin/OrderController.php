<?php

namespace App\Http\Controllers\Admin;

use App\Criteria\Builder\OrderCriteriaBuilder;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Services\Actions\OrderServiceAction;
use App\Services\OrderService;


class OrderController extends Controller {
  /**
   * @var OrderServiceAction
   */
  protected $service;

  /**
   * OrderController constructor.
   *
   * @param OrderServiceAction $service
   */
  public function __construct(OrderServiceAction $service) {
    $this->service = $service;
  }


  /**
   * @param OrderCriteriaBuilder $criteriaBuilder
   * @return mixed
   */
  public function index(OrderCriteriaBuilder $criteriaBuilder) {
    $paginator = Order::filterToPaginate($criteriaBuilder, $criteriaBuilder->max(), $criteriaBuilder->page());
    return view('admin.orders.index', [
      'paginator' => $paginator->appends($criteriaBuilder->getRequest()->all()),
      'orders' => OrderResource::rawPaginator($paginator, $criteriaBuilder->getRequest()),
    ]);
  }

  /**
   * @param Order $order
   * @return \Illuminate\Http\JsonResponse
   */
  public function accept(Order $order) {
    $service = new OrderService($order);
    $service
      ->toAccept()
      ->commitChanges();

    return response()
      ->json(['message' => 'Заказ изменен.', 'class' => 'success']);
  }


  /**
   * @param Order $order
   * @return \Illuminate\Http\JsonResponse
   */
  public function confirmed(Order $order) {
    $service = new OrderService($order);
    $service
      ->confirm()
      ->commitChanges();

    return response()
      ->json(['message' => 'Заказ изменен.', 'class' => 'success']);
  }


  /**
   * @param Order $order
   * @return \Illuminate\Http\JsonResponse
   */
  public function canceled(Order $order) {
    $service = new OrderService($order);
    $service
      ->toCancel()
      ->commitChanges();

    return response()
      ->json(['message' => 'Заказ изменен.', 'class' => 'success']);
  }
}