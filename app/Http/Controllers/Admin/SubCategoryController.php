<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReorderRecordFormRequest;
use App\Http\Requests\SubCategoryRequest;
use App\Models\Category;
use App\Models\SubCategory;
use App\Services\Actions\SubCategoryServiceAction;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class SubCategoryController extends Controller {
  /**
   * @var SubCategoryServiceAction
   */
  protected $service;

  /**
   * SubCategoryController constructor.
   *
   * @param SubCategoryServiceAction $service
   */
  public function __construct(SubCategoryServiceAction $service) {
    $this->service = $service;
  }

  /**
   * @return View
   */
  public function index(): View {
    $subCategories = SubCategory::query()
      ->with('category')
      ->orderBy('category_id', 'asc')
      ->orderBy('display_order', 'asc')
      ->get();

    return view('admin.sub_categories.index', [
      'subCategories' => $subCategories,
    ]);
  }

  /**
   * @return View
   */
  public function create(): View {
    $categories = Category::query()->get();
    return view('admin.sub_categories.edit', [
      'subCategory' => new SubCategory(),
      'categories' => $categories,
    ]);
  }

  /**
   * @param SubCategoryRequest $request
   * @return RedirectResponse
   */
  public function store(SubCategoryRequest $request) {
    $this->service->createSubCategory($request->createDto());

    return redirect()
      ->route('sub-categories.index')
      ->with(['message' => 'Подкатегория сохранена.', 'class' => 'success']);
  }

  /**
   * @param SubCategory $subCategory
   * @return View
   */
  public function edit(SubCategory $subCategory): View {
    $categories = Category::query()->get();
    return view('admin.sub_categories.edit', [
      'subCategory' => $subCategory,
      'categories' => $categories,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param SubCategory $subCategory
   * @param SubCategoryRequest $request
   * @return RedirectResponse
   */
  public function update(SubCategory $subCategory, SubCategoryRequest $request) {
    $this->service->updateSubCategory($subCategory, $request->createDto());

    return redirect()->route('sub-categories.index')->with([
      'message' => 'Подкатегория изменена.',
      'class' => 'success'
    ]);
  }

  /**
   * @param SubCategory $subCategory
   * @return RedirectResponse
   * @throws \Exception
   *
   */
  public function destroy(SubCategory $subCategory) {
    $subCategory->delete();
    return redirect()->route('sub-categories.index')->with([
      'class' => 'success',
      'message' => 'Подкатегория успешно удалена.',
    ]);
  }

  /**
   * @param ReorderRecordFormRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function reorder(ReorderRecordFormRequest $request) {
    if ($this->service->reorder((int)$request->json('currentId'), (int)$request->json('desiredId'), SubCategory::class)) {
      return response()->json(['status' => 'reorder']);
    }

    return response()->json(['error' => 'Подкатегория не найдена'], 404);
  }
}
