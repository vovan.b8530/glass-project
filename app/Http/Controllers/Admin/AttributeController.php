<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AttributeRequest;
use App\Http\Requests\ReorderRecordFormRequest;
use App\Models\Attribute;
use App\Services\Actions\AttributeServiceAction;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class AttributeController extends Controller {
  /**
   * @var AttributeServiceAction
   */
  protected $service;

  /**
   * AttributeController constructor.
   *
   * @param AttributeServiceAction $service
   */
  public function __construct(AttributeServiceAction $service) {
    $this->service = $service;
  }

  /**
   * @return View
   */
  public function index(): View {
    $attributes = Attribute::query()
      ->orderBy('display_order', 'asc')
      ->get();

    return view('admin.attributes.index', [
      'attributes' => $attributes,
    ]);
  }

  /**
   * @return View
   */
  public function create(): View {
    return view('admin.attributes.edit', [
      'attribute' => new Attribute(),
    ]);
  }

  /**
   * @param AttributeRequest $request
   * @return RedirectResponse
   */
  public function store(AttributeRequest $request) {
    $this->service->createAttribute($request->createDto());
    return redirect()
      ->route('attributes.index')
      ->with(['message' => 'Атрибут сохранен.', 'class' => 'success']);
  }

  /**
   * @param Attribute $attribute
   * @return View
   */
  public function edit(Attribute $attribute): View {
    return view('admin.attributes.edit', [
      'attribute' => $attribute,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Attribute $attribute
   * @param AttributeRequest $request
   * @return RedirectResponse
   */
  public function update(Attribute $attribute, AttributeRequest $request) {
    $this->service->updateAttribute($attribute, $request->createDto());

    return redirect()->route('attributes.index')->with([
      'message' => 'Атрибут изменен.',
      'class' => 'success'
    ]);
  }

  /**
   * @param Attribute $attribute
   * @return RedirectResponse
   * @throws \Exception
   *
   */
  public function destroy(Attribute $attribute) {
    $attribute->delete();
    return redirect()->route('attributes.index')->with([
      'class' => 'success',
      'message' => 'Атрибут успешно удален.',
    ]);
  }

  /**
   * @param ReorderRecordFormRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function reorder(ReorderRecordFormRequest $request) {
    if ($this->service->reorder((int)$request->json('currentId'), (int)$request->json('desiredId'), Attribute::class)) {
      return response()->json(['status' => 'reorder']);
    }

    return response()->json(['error' => 'Атрибут не найден'], 404);
  }
}
