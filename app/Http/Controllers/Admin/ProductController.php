<?php

namespace App\Http\Controllers\Admin;

use App\Criteria\Builder\ProductCriteriaBuilder;
use App\Criteria\Product\WithDefaultRelationship;
use App\Enums\PictureMainPage;
use App\Http\Controllers\Controller;
use App\Http\Requests\PictureReorderRecordFormRequest;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ReorderRecordFormRequest;
use App\Http\Resources\ProductResource;
use App\Models\Attribute;
use App\Models\AttributeOption;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductPicture;
use App\Services\Actions\ProductServiceAction;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Artisan;
use Illuminate\View\View;

class ProductController extends Controller {
  /**
   * @var ProductServiceAction
   */
  protected $service;

  /**
   * ProductController constructor.
   *
   * @param ProductServiceAction $service
   */
  public function __construct(ProductServiceAction $service) {
    $this->service = $service;
  }

  /**
   * @param ProductCriteriaBuilder $criteriaBuilder
   * @return View
   */
  public function index(ProductCriteriaBuilder $criteriaBuilder): View {
    $paginator = Product::filterToPaginate($criteriaBuilder, $criteriaBuilder->max(), $criteriaBuilder->page());

    return view('admin.products.index', [
      'paginator' => $paginator->appends($criteriaBuilder->getRequest()->all()),
      'products' => ProductResource::rawPaginator($paginator, $criteriaBuilder->getRequest()),
    ]);
  }

  /**
   * @return View
   */
  public function create(): View {
    $categories = Category::query()->get();
    $products = Product::query()->get();
    $picturesMainPage = PictureMainPage::$LABELS;
    $attributes = Attribute::query()->with('attributeOptions')->get();
    $attributeOptions = AttributeOption::query()->get();

    return view('admin.products.edit', [
      'product' => new Product(),
      'categories' => $categories,
      'picturesMainPage' => $picturesMainPage,
      'attributes' => $attributes,
      'productAttributeOptions' => $attributeOptions,
      'products' => $products,
    ]);
  }

  /**
   * @param ProductRequest $request
   * @return RedirectResponse
   */
  public function store(ProductRequest $request) {
    $this->service->createProduct($request->createDto());

    Artisan::call('export:products');

    return redirect()
      ->route('products.index')
      ->with(['message' => 'Продукт сохранен.', 'class' => 'success']);
  }

  /**
   * @param Product $product
   * @return View
   */
  public function edit(Product $product): View {
    $product->load(WithDefaultRelationship::relations());
    $products = Product::query()->get()->whereNotIn('id', $product->id);
    $picturesMainPage = PictureMainPage::$LABELS;
    $categories = Category::query()->get();
    $attributes = Attribute::query()->with('attributeOptions')->get();

    return view('admin.products.edit', [
      'product' => $product,
      'categories' => $categories,
      'picturesMainPage' => $picturesMainPage,
      'products' => $products,
      'attributes' => $attributes,
      'productAttributeOptions' => $product->attributeOptions->groupBy('attribute_id'),
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Product $product
   * @param ProductRequest $request
   * @return RedirectResponse
   */
  public function update(Product $product, ProductRequest $request) {

    $this->service->updateProduct($product, $request->createDto());

    Artisan::call('export:products');

    return redirect()->route('products.index')->with([
      'message' => 'Продукт изменен.',
      'class' => 'success'
    ]);
  }

  /**
   * @param Product $product
   * @return RedirectResponse
   * @throws \Exception
   *
   */
  public function destroy(Product $product) {
    $product->delete();

    Artisan::call('export:products');

    return redirect()->route('products.index')->with([
      'class' => 'success',
      'message' => 'Продукт успешно удален.',
    ]);
  }

  /**
   * @param ReorderRecordFormRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function reorder(ReorderRecordFormRequest $request) {
    if ($this->service->reorder((int)$request->json('currentId'), (int)$request->json('desiredId'), Product::class)) {
      return response()->json(['status' => 'reorder']);
    }

    return response()->json(['error' => 'Продукт не найден'], 404);
  }

  /**
   * Move the specified agency to desired position.
   *
   * @param PictureReorderRecordFormRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function reorderPictures(PictureReorderRecordFormRequest $request) {
    if ($this->service->reorder((int)$request->json('currentId'), (int)$request->json('desiredId'), ProductPicture::class)) {
      return response()->json(['status' => 'reorder']);
    }

    return response()->json(['error' => 'Картинка продукта не найдена'], 404);
  }
}
