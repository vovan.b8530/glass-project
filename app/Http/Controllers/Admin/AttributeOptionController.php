<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AttributeOptionRequest;
use App\Http\Requests\ReorderRecordFormRequest;
use App\Models\Attribute;
use App\Models\AttributeOption;
use App\Services\Actions\AttributeOptionServiceAction;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class AttributeOptionController extends Controller {
  /**
   * @var AttributeOptionServiceAction
   */
  protected $service;

  /**
   * AttributeOptionController constructor.
   *
   * @param AttributeOptionServiceAction $service
   */
  public function __construct(AttributeOptionServiceAction $service) {
    $this->service = $service;
  }

  /**
   * @return View
   */
  public function index(): View {
    $attributeOptions = AttributeOption::query()
      ->orderBy('attribute_id', 'asc')
      ->orderBy('display_order', 'asc')
      ->get();

    return view('admin.attribute_options.index', [
      'attributeOptions' => $attributeOptions,
    ]);
  }

  /**
   * @return View
   */
  public function create(): View {
    $attributes = Attribute::query()->get();
    return view('admin.attribute_options.edit', [
      'attributeOption' => new AttributeOption(),
      'attributes' => $attributes,
    ]);
  }

  /**
   * @param AttributeOptionRequest $request
   * @return RedirectResponse
   */
  public function store(AttributeOptionRequest $request) {
    $this->service->createAttributeOption($request->createDto());

    return redirect()
      ->route('attribute-options.index')
      ->with(['message' => 'Атрибут опция сохранена.', 'class' => 'success']);
  }

  /**
   * @param AttributeOption $attributeOption
   * @return View
   */
  public function edit(AttributeOption $attributeOption): View {
    $attributes = Attribute::query()->get();
    return view('admin.attribute_options.edit', [
      'attributeOption' => $attributeOption,
      'attributes' => $attributes,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param AttributeOption $attributeOption
   * @param AttributeOptionRequest $request
   * @return RedirectResponse
   */
  public function update(AttributeOption $attributeOption, AttributeOptionRequest $request) {
    $this->service->updateAttributeOption($attributeOption, $request->createDto());

    return redirect()->route('attribute-options.index')->with([
      'message' => 'Атрибут опции изменен.',
      'class' => 'success'
    ]);
  }

  /**
   * @param AttributeOption $attributeOption
   * @return RedirectResponse
   * @throws \Exception
   *
   */
  public function destroy(AttributeOption $attributeOption) {
    $attributeOption->delete();
    return redirect()->route('attribute-options.index')->with([
      'class' => 'success',
      'message' => 'Атрибут опции успешно удален.',
    ]);
  }

  /**
   * @param ReorderRecordFormRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function reorder(ReorderRecordFormRequest $request) {
    if ($this->service->reorder((int)$request->json('currentId'), (int)$request->json('desiredId'), AttributeOption::class)) {
      return response()->json(['status' => 'reorder']);
    }

    return response()->json(['error' => 'Атрибут опции не найден'], 404);
  }
}
