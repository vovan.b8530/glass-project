<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReorderRecordFormRequest;
use App\Services\Actions\CategoryServiceAction;
use Illuminate\Http\RedirectResponse;
use App\Models\Category;
use Illuminate\View\View;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller {
  /**
   * @var CategoryServiceAction
   */
  protected $service;

  /**
   * CategoryController constructor.
   *
   * @param CategoryServiceAction $service
   */
  public function __construct(CategoryServiceAction $service) {
    $this->service = $service;
  }

  /**
   * @return View
   */
  public function index(): View {
    $categories = Category::query()
      ->orderBy('display_order', 'asc')
      ->get();

    return view('admin.categories.index', [
      'categories' => $categories,
    ]);
  }

  /**
   * @return View
   */
  public function create(): View {
    return view('admin.categories.edit', [
      'category' => new Category(),
    ]);
  }

  /**
   * @param CategoryRequest $request
   * @return RedirectResponse
   */
  public function store(CategoryRequest $request) {
    $this->service->createCategory($request->createDto());
    return redirect()
      ->route('categories.index')
      ->with(['message' => 'Категория сохранена.', 'class' => 'success']);
  }

  /**
   * @param Category $category
   * @return View
   */
  public function edit(Category $category): View {
    return view('admin.categories.edit', [
      'category' => $category,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Category $category
   * @param CategoryRequest $request
   * @return RedirectResponse
   */
  public function update(Category $category, CategoryRequest $request) {
    $this->service->updateCategory($category, $request->createDto());

    return redirect()->route('categories.index')->with([
      'message' => 'Категория изменена.',
      'class' => 'success'
    ]);
  }

  /**
   * @param Category $category
   * @return RedirectResponse
   * @throws \Exception
   *
   */
  public function destroy(Category $category) {
    $category->delete();
    return redirect()->route('categories.index')->with([
      'class' => 'success',
      'message' => 'Категория успешно удалена.',
    ]);
  }

  /**
   * @param ReorderRecordFormRequest $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function reorder(ReorderRecordFormRequest $request) {
    if ($this->service->reorder((int)$request->json('currentId'), (int)$request->json('desiredId'), Category::class)) {
      return response()->json(['status' => 'reorder']);
    }

    return response()->json(['error' => 'Категория не найдена'], 404);
  }
}
