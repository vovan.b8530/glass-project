<?php

namespace App\Http\Resources;

use App\Http\Resources\Helpers\LazyEagerRelationsLoading;
use App\Models\Seo;

class SeoResource extends JsonResource {
  use LazyEagerRelationsLoading;

  /**
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    /* @var Seo|self $this */
    return [
      'id' => $this->id,

      'url' => $this->url,

      'title' => $this->title,
      'description' => $this->description,

      'route' => route('seo.edit', ['seo' => $this]),
      'routeDelete' => route('seo.destroy', ['seo' => $this]),
    ];
  }

  /**
   * @return array
   */
  public function relations(): array {
    //
  }
}