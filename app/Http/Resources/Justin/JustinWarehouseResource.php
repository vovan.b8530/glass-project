<?php

namespace App\Http\Resources\Justin;

use App\Http\Resources\JsonResource;
use App\Models\JustinWarehouse;

class JustinWarehouseResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    /* @var JustinWarehouse|self $this */
    return [
      'id' => $this->id,
      'text' => $this->descr . ', ' . $this->address,
    ];
  }
}
