<?php

namespace App\Http\Resources\Justin;

use App\Http\Resources\JsonResource;
use App\Models\JustinCity;

class JustinCityResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    /* @var JustinCity|self $this */
    return [
      'id' => $this->id,
      'text' => $this->descr,
    ];
  }
}
