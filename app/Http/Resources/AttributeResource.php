<?php

namespace App\Http\Resources;

use App\Models\Attribute;

class AttributeResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    /* @var Attribute|self $this */
    return [
      'id' => $this->id,

      'alias' => $this->alias,

      'title' => $this->title,

      'display_order' => $this->display_order

    ];
  }
}
