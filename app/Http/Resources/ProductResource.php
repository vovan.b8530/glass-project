<?php

namespace App\Http\Resources;

use App\Http\Resources\Helpers\LazyEagerRelationsLoading;
use App\Http\Resources\Product\PictureResource;
use App\Models\Product;

class ProductResource extends JsonResource {
  use LazyEagerRelationsLoading;

  /**
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    /* @var Product|self $this */
    return array_merge(
      [
        'id' => $this->id,
        'title' => $this->title,
        'description' => $this->description,
        'product_hash' => $this->product_hash,
        'price' => $this->price,
        'stock_quantity' => $this->stock_quantity,
        'is_main_page' => $this->is_main_page,
        'category_id' => $this->category_id,
        'display_order' => $this->display_order,
        'seo_title' => $this->seo_title,
        'seo_description' => $this->seo_description,
        'pictures' => PictureResource::collection($this->whenLoaded('pictures')),

        'category' => new CategoryResource($this->whenLoaded('category')),
        'attributeOptions' => AttributeOptionResource::collection($this->whenLoaded('attributeOptions')),
        'routeShow' => route('site.products.show', ['product' => $this]),
        'routeEdit' => route('products.edit', ['product' => $this]),
        'routeDelete' => route('products.destroy', ['product' => $this]),
        'getTable' => $this->getTable(),

        'colorAttributeOption' => $this->colorAttributeOption(),

        'routeAddToCart' => route('site.products.order', ['product' => $this]),
      ], $this->loadRelations($request)
    );
  }

  /**
   * @return array
   */
  public function relations(): array {
    $retrieveBackgroundPicture = function (string $attribute, string $defaultAttribute = null) {
      return function () use ($attribute, $defaultAttribute) {
        /* @var Product|self $this */
        $picture = $this->pictures->firstMainPicture();

        if (!$picture) return null;

        $path = $picture->assetAbsolute($picture->getAttributeValue($attribute));

        if (!$path && ($defaultFilePath = $picture->getAttributeValue($defaultAttribute)))
          $path = $picture->assetAbsolute($defaultFilePath);

        return $path;
      };
    };

    return [
      'backgroundPicture' => [
        'dependencyRelation' => 'pictures',
        'load' => $retrieveBackgroundPicture('picture_file_name'),
      ],
      'backgroundThumbnail' => [
        'dependencyRelation' => 'pictures',
        'load' => $retrieveBackgroundPicture('picture_thumb_file_name', 'picture_file_name'),
      ],

      'productGroup' => function () {
        /* @var Product|self $this */
        if ($attributeOption = $this->colorAttributeOption() || $this->productGroup->count()>1)
        return self::collection(
          $this->productGroup->filter(function ($productGroup) {
            /* @var Product|self $this */
            return $productGroup->id;
          })
        );
        return null;
      },

      'colorAttributeOption' => [
        'dependencyRelation' => 'attributeOptions',
        'load' => function () {
          /* @var Product|self $this */
          if ($attributeOption = $this->colorAttributeOption())
            return new AttributeOptionResource($this->colorAttributeOption());

          return null;
        }
      ],
    ];
  }
}