<?php

namespace App\Http\Resources;

use App\Models\AttributeOption;

class AttributeOptionResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    /* @var AttributeOption|self $this */
    return
      [
        'id' => $this->id,

        'alias' => $this->alias,
        'attribute_id' => $this->attribute_id,

        'attribute' => new AttributeResource($this->whenLoaded('attribute')),

        'title' => $this->title,
        'picture_file_name' => $this->picture_file_name,

        'display_order' => $this->display_order,
        'backgroundPicture' => isset($this->picture_file_name) ? $this->assetAbsolute($this->picture_file_name) : ''

      ];
  }
}
