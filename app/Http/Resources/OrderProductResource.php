<?php

namespace App\Http\Resources;

use App\Models\OrderProduct;

class OrderProductResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    /* @var OrderProduct|self $this */
    return [
      'id' => $this->id,
      'order_id' => $this->order_id,
      'product_id' => $this->product_id,
      'quantity' => $this->quantity,
      'price' => $this->price,

      'amount' => $this->calculateAmount($this->price),

      'product' => new ProductResource($this->whenLoaded('product')),
    ];
  }
}
