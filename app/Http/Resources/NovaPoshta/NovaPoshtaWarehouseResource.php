<?php

namespace App\Http\Resources\NovaPoshta;

use App\Http\Resources\JsonResource;
use App\Models\NovaPoshtaWarehouse;

class NovaPoshtaWarehouseResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    /* @var NovaPoshtaWarehouse|self $this */
    return [
      'id' => $this->id,
      'text' => $this->description_ru,
      'description_ru' => $this->description_ru,
      'description_uk' => $this->description_uk,
    ];
  }
}
