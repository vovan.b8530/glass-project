<?php

namespace App\Http\Resources;

use App\Enums\DeliveryTypes;
use App\Enums\OrderStatuses;
use App\Enums\PaymentTypes;
use App\Http\Resources\Justin\JustinCityResource;
use App\Http\Resources\Justin\JustinWarehouseResource;
use App\Http\Resources\NovaPoshta\NovaPoshtaCityResource;
use App\Http\Resources\NovaPoshta\NovaPoshtaWarehouseResource;
use App\Models\Order;
use Illuminate\Support\Arr;

class OrderResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    /* @var Order|self $this */
    return [
      'id' => $this->id,
      'uuid' => $this->uuid,
      'status' => $this->status,
      'status_name' =>
        $this->status ? Arr::get(
          OrderStatuses::$LABELS,
          $this->status,
          OrderStatuses::$LABELS[OrderStatuses::NEW]
        ) : '',

      'isNew' => $this->status === OrderStatuses::NEW,
      'isAccept' => $this->status === OrderStatuses::ACCEPT,
      'isConfirmed' => $this->status === OrderStatuses::CONFIRMED,
      'isCanceled' => $this->status === OrderStatuses::CANCELED,

      'payment_type' => $this->payment_type,
      'payment_name' =>
        $this->payment_type ? Arr::get(
          PaymentTypes::$LABELS,
          $this->payment_type,
          PaymentTypes::$LABELS[PaymentTypes::CASH]
        ) : '',

      'name' => $this->name,
      'email' => $this->email,
      'phone_number' => $this->phone_number,

      'delivery_type' => $this->delivery_type,
      'delivery_name' =>
        $this->delivery_type ? Arr::get(
          DeliveryTypes::$LABELS,
          $this->delivery_type,
          DeliveryTypes::$LABELS[DeliveryTypes::NOVA_POSHTA]
        ) : '',


      'novaposhta_city_id' => $this->novaposhta_city_id,
      'novaposhta_warehouse_id' => $this->novaposhta_warehouse_id,

      'justin_city_id' => $this->justin_city_id,
      'justin_warehouse_id' => $this->justin_warehouse_id,

      'created_at' => $this->getFormattedDateAttribute('created_at')->format('d.m.Y  H:i:s'),
      'updated_at' => $this->getFormattedDateAttribute('updated_at')->format('d.m.Y  H:i:s'),

      'novaposhtaCity' => new NovaPoshtaCityResource($this->whenLoaded('novaPoshtaCity')),
      'novaposhtaWarehouse' => new NovaPoshtaWarehouseResource($this->whenLoaded('novaPoshtaWarehouse')),

      'justinCity' => new JustinCityResource($this->whenLoaded('justinCity')),
      'justinWarehouse' => new JustinWarehouseResource($this->whenLoaded('justinWarehouse')),

      'total_amount' => $this->total_amount,

      'orderProducts' => OrderProductResource::collection($this->whenLoaded('orderProducts')),

      'routeAccept' => route('order.accept', ['order' => $this]),
      'routeConfirmed' => route('order.confirmed', ['order' => $this]),
      'routeCanceled' => route('order.canceled', ['order' => $this]),
    ];
  }
}
