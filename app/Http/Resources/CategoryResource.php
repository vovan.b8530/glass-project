<?php

namespace App\Http\Resources;

use App\Models\Category;

class CategoryResource extends JsonResource {

  /**
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray($request) {
    /* @var Category|self $this */
    return [
      'id' => $this->id,
      'title' => $this->title,
    ];
  }

}