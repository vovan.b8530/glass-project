<?php

namespace App\Console\Commands;

use App\NovaPoshta\Models\Address;
use App\NovaPoshta\Models\QueryBuilder\GetCitiesQueryBuilder;
use App\NovaPoshta\Models\QueryBuilder\GetWarehousesQueryBuilder;
use App\NovaPoshta\Seeder\BaseSeeder;
use Illuminate\Console\Command;

class DownloadCitiesWarehouseNovaPoshta extends Command {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'download:citiesWarehouseNovaPoshta';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Download cities and warehouse from NovaPoshta.';

  /**
   * @var BaseSeeder
   */
  protected $seeder;

  /**
   * @var Address
   */
  protected $address;

  /**
   * DownloadCitiesWarehouseNovaPoshta constructor.
   * @param BaseSeeder $seeder
   * @param Address $address
   */
  public function __construct(BaseSeeder $seeder, Address $address) {
    parent::__construct();
    $this->seeder = $seeder;
    $this->address = $address;
  }

  /**
   * @throws \App\NovaPoshta\Exceptions\NovaPoshtaException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function handle() {
    $this->info("Start download cities and warehouse NovaPoshta...\n");

    $cityPage = 1;
    $warehousePage = 1;
    while (($collection = $this->address->fetchCities(new GetCitiesQueryBuilder($cityPage)))->isNotEmpty()) {
      $cities = $this->seeder
        ->citySeeder($collection)
        ->run();

      $this->info("City page: {$cityPage} / Cities count: " . $cities->count());

      ++$cityPage;

      foreach ($cities as $city) {
        while (($collection = $this->address->fetchWarehouses(new GetWarehousesQueryBuilder($warehousePage, 500, '', $city->ref)))->isNotEmpty()) {
          $newWarehousesCollection = $this->seeder
            ->warehouseSeeder($collection, $city)
            ->run();

          $this->info("Warehouse page: {$warehousePage} / Warehouses count: " . $newWarehousesCollection->count());

          ++$warehousePage;
        }

        $warehousePage = 1;
      }
    }

    $this->info("End download cities and warehouse NovaPoshta!!!\n");
  }
}
