<?php

namespace App\Console\Commands;

use App\Justin\Collection\CityCollection;
use App\Justin\Collection\WarehouseCollection;
use App\Justin\Entity\Warehouse;
use App\Justin\Seeder\BaseSeeder;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use JustinLaravel\JustinLaravel;

class DownloadCitiesWarehouseJustin extends Command {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'download:citiesWarehouseJustin';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Download cities and warehouse from Justin.';

  /**
   * @var BaseSeeder
   */
  protected $seeder;


  /**
   * DownloadCitiesWarehouseNovaPoshta constructor.
   * @param BaseSeeder $seeder
   */
  public function __construct(BaseSeeder $seeder) {
    parent::__construct();
    $this->seeder = $seeder;
  }

  /**
   *
   */
  public function handle() {
    $this->info("Start download cities and warehouse Justin...\n");

    $justin = new JustinLaravel();
    $justinCities = $justin->listCities()->getData();

    $collection = (new CityCollection(Arr::wrap(Arr::pluck($justinCities, 'fields'))))->init();
    $cities = $this->seeder
      ->citySeeder($collection)
      ->run();

    $this->info("Cities count: " . $cities->count());

    $justinWarehouses = $justin->listDepartmentsLang()->getData();
    $warehouses = (
    new WarehouseCollection(
      Arr::wrap(Arr::pluck($justinWarehouses, 'fields'))
    )
    )->init();

    $this->info("Loaded warehouses count: " . $warehouses->count());

    foreach ($cities as $city) {
      $warehousesOfCity = $warehouses->filter(function ($warehouse) use ($city) {
        /* @var Warehouse $warehouse */
        return $warehouse->citySCOATOU == $city->SCOATOU;
      });

      if ($warehousesOfCity->isNotEmpty()) {
        $newWarehousesCollection = $this->seeder
          ->warehouseSeeder($warehousesOfCity, $city)
          ->run();

        $this->info("Saved warehouses count: " . $newWarehousesCollection->count());
      }
    }

    $this->info("End download cities and warehouse Justin!!!\n");
  }
}
