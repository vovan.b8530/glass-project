<?php

namespace App\Console\Commands;

use App\Models\Seo;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use App\Models\Product;

class ExportProductXML extends Command {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'export:products';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'export products';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle() {
    $this->info("Start export product to xml for google feed...\n");

    $seo = Seo::where('url', '=', env('APP_URL'))->first();
    $data = '<?xml version="1.0"?>
              <rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
                <channel>
                  <title>' . $seo['title'] . '</title>
                  <link>' . request()->root() . '</link>
                  <description>' . $seo['description'] . '</description>';

    foreach (Product::with('pictures')->cursor() as $product) {
      $data .= '<item>';
      $data .= '<g:id>' . $product->id . '</g:id>';
      $data .= '<g:title>' . html_entity_decode($product->title) . '</g:title>';
      $data .= '<g:description>' . str_replace(array("\r\n", "\r", "\n"), ' ', strip_tags(nl2br($product->description))) . '</g:description>';
      $data .= '<g:link>' . route('site.products.show', ['product' => $product->alias]) . '</g:link>';
      if ($product->mainPicture) {
        $data .= '<g:image_link>' . asset('storage/products/' . $product->mainPicture->picture_file_name) . '</g:image_link>';
      }
      foreach ($product->pictures as $productPicture) {
        $data .= '<g:additional_image_link>' . asset('storage/products/' . $productPicture->picture_file_name) . '</g:additional_image_link>';
      }
      $data .= '<g:availability>in stock</g:availability>';
      $data .= '<g:price>' . $product->price . '</g:price>';
      $data .= '</item>';
    }
    $data .= '</channel>
              </rss>';

    File::put(storage_path('app/public/products.xml'), $data);

    $this->info("End export product to xml for google feed!!!\n");
  }
}
