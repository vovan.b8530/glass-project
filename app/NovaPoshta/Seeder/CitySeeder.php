<?php

namespace App\NovaPoshta\Seeder;

use App\Models\NovaPoshtaCity;
use App\NovaPoshta\Collection\CityCollection as CityEntityCollection;
use App\NovaPoshta\Entity\City as CityEntity;
use App\Services\NovaPoshtaCityService;
use Illuminate\Database\Eloquent\Collection;

class CitySeeder {
  /**
   * @var CityEntityCollection
   */
  protected $cities;

  /**
   * CitySeeder constructor.
   * @param CityEntityCollection $cities
   */
  public function __construct(CityEntityCollection $cities) {
    $this->cities = $cities->keyBy(function ($city) {
      /* @var CityEntity $city */
      return $city->getRef();
    });
  }

  /**
   * @return Collection
   */
  protected function fetchExistModels(): Collection {
    /* @var Collection $cities */
    $cities = NovaPoshtaCity::query()
      ->whereIn('ref', $this->cities->keys())
      ->get()
      ->keyBy('ref');

    return $cities;
  }

  /**
   * @param CityEntity $city
   * @return NovaPoshtaCity
   */
  protected function createNewCity(CityEntity $city): NovaPoshtaCity {
    return $this->saveCity(new NovaPoshtaCity(), $city);
  }

  /**
   * @param NovaPoshtaCity $model
   * @param CityEntity $city
   * @return NovaPoshtaCity
   */
  protected function saveCity(NovaPoshtaCity $model, CityEntity $city): NovaPoshtaCity {
    $service = new NovaPoshtaCityService($model);
    $service->changeAttributes($city);
    $service->commitChanges();

    return $service->getNovaPoshtaCity();
  }

  /**
   * @param CityEntity $entity
   * @param CityModelCollection $collection
   *
   * @return CityModelCollection
   */
  protected function seed(CityEntity $entity, CityModelCollection $collection): CityModelCollection {
    $newCity = $this->createNewCity($entity);
    $collection->push($newCity);

    return $collection;
  }

  /**
   * @return NovaPoshtaCity[]|Collection
   * @throws \App\NovaPoshta\Exceptions\NovaPoshtaException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function run(): Collection {
    $cityModels = $this->fetchExistModels();

    $cityModels = $cityModels->map(function ($city) {
      /* @var NovaPoshtaCity $city */
      $entity = $this->cities->get($city->ref);

      if ($entity)
        $city = $this->saveCity($city, $entity);

      return $city;
    });

    $this->cities
      ->diffKeys($cityModels->all())
      ->each(function ($city) use ($cityModels) {
        /* @var CityEntity $city */
        $cityModels->push($this->createNewCity($city));
      });

    return $cityModels;
  }
}
