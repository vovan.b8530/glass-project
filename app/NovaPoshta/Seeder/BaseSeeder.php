<?php

namespace App\NovaPoshta\Seeder;

use App\Models\NovaPoshtaCity;
use App\NovaPoshta\Collection\CityCollection;
use App\NovaPoshta\Collection\WarehouseCollection;

class BaseSeeder {
  /**
   * @param CityCollection $cityCollection
   * @return CitySeeder
   */
  public function citySeeder(CityCollection $cityCollection): CitySeeder {
    return new CitySeeder($cityCollection);
  }

  /**
   * @param WarehouseCollection $warehouseCollection
   * @param NovaPoshtaCity $city
   *
   * @return WarehouseSeeder
   */
  public function warehouseSeeder(WarehouseCollection $warehouseCollection, NovaPoshtaCity $city): WarehouseSeeder {
    return new WarehouseSeeder($warehouseCollection, $city);
  }
}
