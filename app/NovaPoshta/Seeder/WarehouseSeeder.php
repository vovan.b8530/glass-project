<?php

namespace App\NovaPoshta\Seeder;

use App\Models\NovaPoshtaCity;
use App\Models\NovaPoshtaWarehouse;
use App\NovaPoshta\Collection\WarehouseCollection as WarehouseEntityCollection;
use App\NovaPoshta\Entity\Warehouse as WarehouseEntity;
use App\NovaPoshta\Collection\WarehouseCollection;
use App\Services\NovaPoshtaWarehouseService;
use Illuminate\Database\Eloquent\Collection;

class WarehouseSeeder {
  /**
   * @var WarehouseEntityCollection
   */
  protected $warehouses;

  /**
   * @var NovaPoshtaCity
   */
  protected $city;

  /**
   * WarehouseSeeder constructor.
   *
   * @param WarehouseCollection $entities
   * @param NovaPoshtaCity $city
   */
  public function __construct(WarehouseEntityCollection $entities, NovaPoshtaCity $city) {
    $this->warehouses = $entities->keyBy(function ($entity) {
      /* @var WarehouseEntity $entity */
      return $entity->getRef();
    });

    $this->city = $city;
  }

  /**
   * @return Collection
   */
  protected function fetchExistModels(): Collection {
    /* @var Collection $warehouses */
    $warehouses = NovaPoshtaWarehouse::query()
      ->whereIn('ref', $this->warehouses->keys())
      ->get()
      ->keyBy('ref');

    return $warehouses;
  }

  /**
   * @param WarehouseEntity $warehouse
   *
   * @return NovaPoshtaWarehouse
   */
  protected function createNewWarehouse(WarehouseEntity $warehouse): NovaPoshtaWarehouse {
    return $this->saveWarehouse(new NovaPoshtaWarehouse(), $warehouse);
  }

  /**
   * @param NovaPoshtaWarehouse $model
   * @param WarehouseEntity $entity
   *
   * @return NovaPoshtaWarehouse
   */
  protected function saveWarehouse(NovaPoshtaWarehouse $model, WarehouseEntity $entity): NovaPoshtaWarehouse {
    $service = new NovaPoshtaWarehouseService($model);

    $service
      ->changeAttributes($entity)
      ->attachCity($this->city);

    $service->commitChanges();

    return $service->getNovaPoshtaWarehouse();
  }

  /**
   * @return Collection
   */
  public function run(): Collection {
    $warehouseModels = $this->fetchExistModels();

    $warehouseModels = $warehouseModels->map(function ($warehouse) {
      /* @var NovaPoshtaWarehouse $warehouse */
      $entity = $this->warehouses->get($warehouse->ref);

      if ($entity)
        $warehouse = $this->saveWarehouse($warehouse, $entity);

      return $warehouse;
    });

    $this->warehouses
      ->diffKeys($warehouseModels->all())
      ->each(function ($warehouse) use ($warehouseModels) {
        /* @var WarehouseEntity $warehouse */
        $warehouseModels->push($this->createNewWarehouse($warehouse));
      });

    return $warehouseModels;
  }
}
