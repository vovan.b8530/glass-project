<?php

namespace App\NovaPoshta\Models;


use App\Support\Guzzle\BaseHttpClient;

class Transport {
  /**
   * @var string
   */
  protected $apiKey;

  /**
   * @var BaseHttpClient
   */
  protected $httpClient;

  /**
   * Transport constructor.
   *
   * @param string $apiKey
   * @param BaseHttpClient $httpClient
   */
  public function __construct(string $apiKey, BaseHttpClient $httpClient) {
    $this->apiKey = $apiKey;
    $this->httpClient = $httpClient;
  }

  /**
   * @return string
   */
  public function getApiKey(): string {
    return $this->apiKey;
  }

  /**
   * @return BaseHttpClient
   */
  public function getHttpClient(): BaseHttpClient {
    return $this->httpClient;
  }
}