<?php

namespace App\DTO;

use App\Helpers\ArrayHelper;
use Illuminate\Contracts\Support\Arrayable;

class SeoDto implements Arrayable {
  /**
   * @var string
   */
  protected $url;

  /**
   * @var string
   */
  protected $title;

  /**
   * @var string
   */
  protected $description;

  /**
   * SeoDto constructor.
   *
   * @param string $url
   * @param string $title
   * @param string $description
   */
  public function __construct(string $url,
                              string $title,
                              string $description) {
    $this->url = $url;
    $this->title = $title;
    $this->description = $description;

  }

  /**
   * @return string
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      'url' => rtrim($this->url, '/'),
      'title' => $this->title,
      'description' => $this->description,
    ];
  }

  /**
   * @param array $attributes
   * @return SeoDto
   */
  public static function createFromArray(array $attributes): self {
    return new self(
      ArrayHelper::getNotEmptyValue($attributes, 'url'),
      ArrayHelper::getNotEmptyValue($attributes, 'title'),
      ArrayHelper::getNotEmptyValue($attributes, 'description')
    );
  }
}