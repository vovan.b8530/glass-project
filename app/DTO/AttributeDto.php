<?php

namespace App\DTO;

use App\Helpers\ArrayHelper;

class AttributeDto {
  /**
   * @var string
   */
  public $title;

  /**
   * @var string
   */
  public $alias;
  /**
   * @var boolean
   */
  public $isColor;

  /**
   * AttributeDto constructor.
   *
   * @param string $title
   * @param string $alias
   * @param bool $isColor
   */
  public function __construct(string $title,
                              string $alias,
                              bool $isColor = false) {
    $this->title = $title;
    $this->alias = $alias;
    $this->isColor = $isColor;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return bool
   */
  public function getIsColor() {
    return $this->isColor;
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      'title' => $this->title,
      'alias' => $this->alias,
      'is_color' => $this->isColor,
    ];
  }

  /**
   * @param array $attributes
   * @return AttributeDto
   */
  public static function createFromArray(array $attributes): self {
    return new self(
      (string)ArrayHelper::getNotEmptyValue($attributes, 'title', ''),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'alias', ''),
      (bool)ArrayHelper::getNotEmptyValue($attributes, 'is_color') ?? false
    );
  }
}
