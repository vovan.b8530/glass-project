<?php

namespace App\DTO;

use App\Helpers\ArrayHelper;

class SubCategoryDto {
  /**
   * @var string
   */
  public $alias;

  /**
   * @var string
   */
  public $title;

  /**
   * @var int
   */
  public $categoryId;

  /**
   * @var string
   */
  public $attributeOptionUrl;

  /**
   * @var string
   */
  public $seoTitle;

  /**
   * @var string
   */
  public $seoDescription;

  /**
   * CategoryDto constructor.
   *
   * @param string $alias
   * @param string $title
   * @param int $categoryId
   * @param string $attributeOptionUrl
   * @param string $seoTitle
   * @param string $seoDescription
   */
  public function __construct(string $alias,
                              string $title,
                              int $categoryId,
                              string $attributeOptionUrl,
                              string $seoTitle,
                              string $seoDescription) {
    $this->alias = $alias;
    $this->title = $title;
    $this->categoryId = $categoryId;
    $this->attributeOptionUrl = $attributeOptionUrl;
    $this->seoTitle = $seoTitle;
    $this->seoDescription = $seoDescription;
  }

  /**
   * @return string
   */
  public function getAlias() {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return int
   */
  public function getCategoryId() {
    return $this->categoryId;
  }

  /**
   * @return string
   */
  public function getAttributeOptionUrl() {
    return $this->attributeOptionUrl;
  }

  /**
   * @return string
   */
  public function getSeoTitle() {
    return $this->seoTitle;
  }

  /**
   * @return string
   */
  public function getSeoDescription() {
    return $this->seoDescription;
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      'alias' => $this->alias,
      'title' => $this->title,
      'category_id' => $this->categoryId,
      'attribute_option_url' => $this->attributeOptionUrl,
      'seo_title' => $this->seoTitle,
      'seo_description' => $this->seoDescription,
    ];
  }

  /**
   * @param array $attributes
   * @return SubCategoryDto
   */
  public static function createFromArray(array $attributes): self {
    return new self(
      (string)ArrayHelper::getNotEmptyValue($attributes, 'alias', ''),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'title', ''),
      (int)ArrayHelper::getNotEmptyValue($attributes, 'category_id') ?: null,
      (string)ArrayHelper::getNotEmptyValue($attributes, 'attribute_option_url', ''),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'seo_title', ''),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'seo_description', '')
    );
  }
}
