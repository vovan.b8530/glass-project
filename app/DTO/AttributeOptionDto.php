<?php

namespace App\DTO;

use App\Helpers\ArrayHelper;
use Illuminate\Http\UploadedFile;

class AttributeOptionDto {

  /**
   * @var string
   */
  public $alias;

  /**
   * @var int
   */
  public $attributeId;

  /**
   * @var string
   */
  public $title;

  /**
   * @var UploadedFile|null
   */
  protected $uploadedFile;

  /**
   * @var integer|null
   */
  protected $deletePicture;

  /**
   * AttributeOptionDto constructor.
   *
   * @param string $alias
   * @param int $attributeId
   * @param string $title
   * @param UploadedFile|null $uploadedFile
   * @param integer|null $deletePicture
   */
  public function __construct(string $alias,
                              int $attributeId,
                              string $title,
                              UploadedFile $uploadedFile = null,
                              int $deletePicture = null) {
    $this->alias = $alias;
    $this->attributeId = $attributeId;
    $this->title = $title;
    $this->uploadedFile = $uploadedFile;
    $this->deletePicture = $deletePicture;
  }

  /**
   * @return string
   */
  public function getAlias() {
    return $this->alias;
  }

  /**
   * @return string
   */
  public function getAttributeId() {
    return $this->attributeId;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return UploadedFile|null
   */
  public function getUploadedFile(): ?UploadedFile {
    return $this->uploadedFile;
  }

  /**
   * @return bool
   */
  public function hasFile(): bool {
    return !empty($this->uploadedFile);
  }


  /**
   * @return int|null
   */
  public function getDeletePicture(): ?int {
    return $this->deletePicture;
  }

  /**
   * @return bool
   */
  public function hasDeletePicture(): bool {
    return !empty($this->deletePicture);
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      'alias' => $this->alias,
      'attribute_id' => $this->attributeId,
      'title' => $this->title,
    ];
  }

  /**
   * @param array $attributes
   * @return AttributeOptionDto
   */
  public static function createFromArray(array $attributes): self {
    return new self(
      (string)ArrayHelper::getNotEmptyValue($attributes, 'alias', ''),
      (int)ArrayHelper::getNotEmptyValue($attributes, 'attribute_id') ?: null,
      (string)ArrayHelper::getNotEmptyValue($attributes, 'title', ''),
      ArrayHelper::getNotEmptyValue($attributes, 'file', '') ?: null,
      (int)ArrayHelper::getNotEmptyValue($attributes, 'deletePicture', 0) ?: null
    );
  }
}
