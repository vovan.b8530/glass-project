<?php

namespace App\DTO;

use App\Helpers\ArrayHelper;
use App\Models\Order;
use App\Payments\DTO\PaymentDto;
use Illuminate\Contracts\Support\Arrayable;

class OrderDto extends PaymentDto implements Arrayable {
  /**
   * @var string|null
   */
  public $uuid;

  /**
   * @var string|null
   */
  public $cookieHash;

  /**
   * @var integer|null
   */
  public $paymentType;

  /**
   * @var string
   */
  public $name;

  /**
   * @var string
   */
  public $phoneNumber;

  /**
   * @var string|null
   */
  public $email;

  /**
   * @var integer|null
   */
  public $deliveryType;


  /**
   * @var integer|null
   */
  public $novaposhtaCityId;

  /**
   * @var integer|null
   */
  public $novaposhtaWarehouseId;

  /**
   * @var integer|null
   */
  public $justinCityId;

  /**
   * @var integer|null
   */
  public $justinWarehouseId;


  /**
   * OrderDto constructor.
   *
   * @param string|null $uuid
   * @param string $name
   * @param string $phoneNumber
   * @param string $email
   * @param int $paymentType
   * @param int|null $deliveryType
   * @param int|null $novaposhtaCityId
   * @param int|null $novaposhtaWarehouseId
   * @param int|null $justinCityId
   * @param int|null $justinWarehouseId
   * @param string|null $cookieHash
   */
  public function __construct(string $name,
                              string $phoneNumber,
                              string $email,
                              int $paymentType,
                              ?string $uuid = null,
                              ?int $deliveryType = null,
                              ?int $novaposhtaCityId = null,
                              ?int $novaposhtaWarehouseId = null,
                              ?int $justinCityId = null,
                              ?int $justinWarehouseId = null,
                              ?string $cookieHash = null) {
    $this->uuid = $uuid;
    $this->name = $name;
    $this->phoneNumber = $phoneNumber;
    $this->email = $email;
    $this->paymentType = $paymentType;
    $this->deliveryType = $deliveryType;
    $this->justinCityId = $justinCityId;
    $this->novaposhtaCityId = $novaposhtaCityId;
    $this->novaposhtaWarehouseId = $novaposhtaWarehouseId;
    $this->justinWarehouseId = $justinWarehouseId;
    $this->cookieHash = $cookieHash;
  }

  /**
   * @return int|null
   */
  public function getPaymentType(): ?int {
    return $this->paymentType;
  }

  /**
   * @return int
   */
  public function getDeliveryType(): int {
    return $this->deliveryType;
  }

  /**
   * @return int|null
   */
  public function getNovaposhtaCityId(): ?int {
    return $this->novaposhtaCityId;
  }

  /**
   * @return int|null
   */
  public function getNovaposhtaWarehouseId(): ?int {
    return $this->novaposhtaWarehouseId;
  }

  /**
   * @return int|null
   */
  public function getJustinCityId(): ?int {
    return $this->justinCityId;
  }

  /**
   * @return int|null
   */
  public function getJustinWarehouseId(): ?int {
    return $this->justinWarehouseId;
  }

  /**
   * @return null|string
   */
  public function getCookieHash(): ?string {
    return $this->cookieHash;
  }

  /**
   * @return self
   */
  public function createPaymentDto(): self {
    return $this;
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      'uuid' => $this->uuid,
      'payment_type' => $this->paymentType,
      'name' => $this->name,
      'phone_number' => $this->phoneNumber,
      'email' => $this->email,
      'delivery_type' => $this->deliveryType,
      'novaposhta_city_id' => $this->novaposhtaCityId,
      'novaposhta_warehouse_id' => $this->novaposhtaWarehouseId,
      'justin_city_id' => $this->justinCityId,
      'justin_warehouse_id' => $this->justinWarehouseId,
    ];
  }

  /**
   * @param array $attributes
   * @return OrderDto
   */
  public static function createFromArray(array $attributes): self {
    return new self(
      (string)ArrayHelper::getNotEmptyValue($attributes, 'name', ''),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'phone_number', ''),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'email', ''),
      (int)ArrayHelper::getNotEmptyValue($attributes, 'payment_type', 1),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'uuid', ''),
      ((int)ArrayHelper::getNotEmptyValue($attributes, 'delivery_type') ?: null),
      ((int)ArrayHelper::getNotEmptyValue($attributes, 'novaposhta_city_id') ?: null),
      ((int)ArrayHelper::getNotEmptyValue($attributes, 'novaposhta_warehouse_id') ?: null),
      ((int)ArrayHelper::getNotEmptyValue($attributes, 'justin_city_id') ?: null),
      ((int)ArrayHelper::getNotEmptyValue($attributes, 'justin_warehouse_id') ?: null)
    );
  }

  /**
   * @param Order $order
   * @return OrderDto
   */
  public static function createFromOrder(Order $order): self {
    return static::createFromArray($order->toArray());
  }
}