<?php

namespace App\DTO;

use App\Helpers\ArrayHelper;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;

class ProductDto implements Arrayable {

  /**
   * @var string
   */
  public $title;

  /**
   * @var string
   */
  public $description;


  /**
   * @var string
   */
  public $productHash;

  /**
   * @var float
   */
  public $price;

  /**
   * @var integer
   */
  public $stockQuantity;

  /**
   * @var int
   */
  public $isMainPage;

  /**
   * @var int
   */
  public $categoryId;

  /**
   * @var string
   */
  public $seoTitle;

  /**
   * @var string
   */
  public $seoDescription;

  /**
   * @var array
   */
  public $files;

  /**
   * @var UploadedFile|null
   */
  protected $mainFile;

  /**
   * @var array
   */
  public $picturesIdToDelete;

  /**
   * @var array
   */
  public $attrOptions;

  /**
   * @var array|null
   */
  public $productIds;

  /**
   * ProductDto constructor.
   *
   * @param string $title
   * @param string $description
   * @param string $productHash
   * @param float $price
   * @param integer $stockQuantity
   * @param int $isMainPage
   * @param int $categoryId
   * @param string $seoTitle
   * @param string $seoDescription
   * @param array $files
   * @param UploadedFile|null $mainFile
   * @param array $picturesIdToDelete
   * @param array $attrOptions
   * @param array $productIds
   */
  public function __construct(string $title,
                              string $description,
                              float $price,
                              int $stockQuantity,
                              int $categoryId,
                              string $seoTitle,
                              string $seoDescription,
                              int $isMainPage,
                              string $productHash,
                              array $files = [],
                              UploadedFile $mainFile = null,
                              array $picturesIdToDelete = [],
                              array $attrOptions = [],
                              array $productIds = []) {
    $this->title = $title;
    $this->description = $description;
    $this->productHash = $productHash;
    $this->price = $price;
    $this->stockQuantity = $stockQuantity;
    $this->isMainPage = $isMainPage;
    $this->categoryId = $categoryId;
    $this->seoTitle = $seoTitle;
    $this->seoDescription = $seoDescription;
    $this->files = $files;
    $this->mainFile = $mainFile;
    $this->picturesIdToDelete = $picturesIdToDelete;
    $this->attrOptions = $attrOptions;
    $this->productIds = $productIds;
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return float
   */
  public function getPrice(): float {
    return $this->price;
  }

  /**
   * @return int
   */
  public function getStockQuantity(): int {
    return $this->stockQuantity;
  }

  /**
   * @return UploadedFile|null
   */
  public function getMainFile() {
    return $this->mainFile;
  }

  /**
   * @return bool
   */
  public function hasFiles(): bool {
    return !empty($this->files);
  }

  /**
   * @return bool
   */
  public function hasMainFile(): bool {
    return !empty($this->mainFile);
  }

  /**
   * @return bool
   */
  public function hasToDeleteFiles(): bool {
    return !empty($this->picturesIdToDelete);
  }

  /**
   * @return array
   */
  public function getPicturesIdToDelete() {
    return $this->picturesIdToDelete;
  }

  /**
   * @return mixed
   */
  public function getFiles() {
    return $this->files;
  }

  /**
   * @return array
   */
  public function getAttrOptions() {
    return $this->attrOptions;
  }

  /**
   * @return bool
   */
  public function hasAttrOptions(): bool {
    return !empty($this->attrOptions);
  }

  /**
   * @return array
   */
  public function getProductIds() {
    return $this->productIds;
  }

  /**
   * @return bool
   */
  public function hasProductIds(): bool {
    return !empty($this->productIds);
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      'title' => $this->title,
      'description' => $this->description,
      'product_hash' => $this->productHash,
      'price' => $this->price,
      'stock_quantity' => $this->stockQuantity,
      'is_main_page' => $this->isMainPage,
      'category_id' => $this->categoryId,
      'seo_title' => $this->seoTitle,
      'seo_description' => $this->seoDescription,
    ];
  }

  /**
   * @param array $attributes
   * @return ProductDto
   */
  public static function createFromArray(array $attributes): self {
    return new self(
      (string)ArrayHelper::getNotEmptyValue($attributes, 'title', ''),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'description', ''),
      (float)ArrayHelper::getNotEmptyValue($attributes, 'price', 0.00),
      (int)ArrayHelper::getNotEmptyValue($attributes, 'stock_quantity', 0),
      (int)ArrayHelper::getNotEmptyValue($attributes, 'category_id') ?: null,
      (string)ArrayHelper::getNotEmptyValue($attributes, 'seo_title', ''),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'seo_description', ''),
      (int)ArrayHelper::getNotEmptyValue($attributes, 'is_main_page', 0),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'product_hash'),
      Arr::wrap(ArrayHelper::getNotEmptyValue($attributes, 'files', [])),
      ArrayHelper::getNotEmptyValue($attributes, 'mainFile') ?: null,
      ArrayHelper::getNotEmptyValue($attributes, 'pictures_id', []),
      ArrayHelper::getNotEmptyValue($attributes, 'attrOptions', []),
      ArrayHelper::getNotEmptyValue($attributes, 'productIds', [])
    );
  }
}
