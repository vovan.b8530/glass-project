<?php

namespace App\DTO;

use App\Helpers\ArrayHelper;

class CategoryDto {
  /**
   * @var string
   */
  public $title;

  /**
   * @var string
   */
  public $alias;

  /**
   * @var string
   */
  public $seoTitle;

  /**
   * @var string
   */
  public $seoDescription;

  /**
   * CategoryDto constructor.
   *
   * @param string $title
   * @param string $alias
   * @param string $seoTitle
   * @param string $seoDescription
   */
  public function __construct(string $title,
                              string $alias,
                              string $seoTitle,
                              string $seoDescription) {
    $this->title = $title;
    $this->alias = $alias;
    $this->seoTitle = $seoTitle;
    $this->seoDescription = $seoDescription;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getSeoTitle() {
    return $this->seoTitle;
  }

  /**
   * @return string
   */
  public function getSeoDescription() {
    return $this->seoDescription;
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      'title' => $this->title,
      'alias' => $this->alias,
      'seo_title' => $this->seoTitle,
      'seo_description' => $this->seoDescription,
    ];
  }

  /**
   * @param array $attributes
   * @return CategoryDto
   */
  public static function createFromArray(array $attributes): self {
    return new self(
      (string)ArrayHelper::getNotEmptyValue($attributes, 'title', ''),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'alias', ''),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'seo_title', ''),
      (string)ArrayHelper::getNotEmptyValue($attributes, 'seo_description', '')
    );
  }
}
