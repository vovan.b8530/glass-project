<?php

namespace App\Services;

use App\Models\NovaPoshtaCity;
use App\Models\NovaPoshtaWarehouse;
use App\NovaPoshta\Entity\Warehouse as WarehouseEntity;
use App\Services\Helpers\PromiseActionsTrait;

class NovaPoshtaWarehouseService {
  use PromiseActionsTrait;

  /**
   * @var NovaPoshtaWarehouse
   */
  private $warehouse;

  /**
   * NovaPoshtaWarehouseService constructor.
   * @param NovaPoshtaWarehouse $warehouse
   */
  public function __construct(NovaPoshtaWarehouse $warehouse) {
    $this->warehouse = $warehouse;
  }

  /**
   * @return NovaPoshtaWarehouse
   */
  public function getNovaPoshtaWarehouse() {
    return $this->warehouse;
  }

  /**
   * Check is warehouse created in DB or no.
   *
   * @return bool
   */
  public function isNewNovaPoshtaWarehouse(): bool {
    return !$this->warehouse->exists;
  }

  /**
   * @param WarehouseEntity $entity
   * @return NovaPoshtaWarehouseService
   */
  public function changeAttributes(WarehouseEntity $entity): self {
    $this->warehouse->fill($entity->toArray());
    return $this;
  }

  /**
   * @param NovaPoshtaCity $city
   * @return $this
   */
  public function attachCity(NovaPoshtaCity $city): self {
    $this->warehouse->novaPoshtaCity()->associate($city);
    return $this;
  }

  /**
   * @return NovaPoshtaWarehouseService
   */
  public function commitChanges(): self {
    $this->warehouse->save();
    $this->releasePromiseActions();

    return $this;
  }
}
