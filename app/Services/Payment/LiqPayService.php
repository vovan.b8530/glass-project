<?php

namespace App\Services\Payment;

use App\Enums\Payments\LiqPay\ActionStatuses;
use App\Enums\Payments\LiqPay\BaseOptions;
use App\Models\Order;
use App\Services\Payment\Entity\LiqPay\TransactionData;
use App\Services\Payment\Entity\LiqPay\TransactionStatus;
use App\Support\LiqPay\LiqPay;

class LiqPayService {
  /**
   * @var LiqPay
   */
  protected $sdk;

  /**
   * LiqPayPayer constructor.
   *
   * @param LiqPay $sdk
   */
  public function __construct(LiqPay $sdk) {
    $this->sdk = $sdk;
  }

  /**
   * @param Order $order
   * @param array $params
   *
   * @return TransactionData
   */
  public function buildTransactionData(Order $order, array $params = []): TransactionData {
    return TransactionData::createFromArray(
      $this->sdk->cnb_form_raw(
        array_merge(
          [
            'version' => BaseOptions::VERSION,
            'action' => ActionStatuses::ACTION_PAY,
            'amount' => $order->total_amount, // сумма заказа
            'currency' => BaseOptions::CURRENCY,
            'description' => 'Оплата заказа',
            'order_id' => $order->uuid,
            'result_url' => route('pay.callback.liqpay'),
            'server_url' => route('pay.listener.liqpay'),
          ],

          $params
        )
      )
    );
  }

  /**
   * @param Order $order
   * @return TransactionStatus
   */
  public function checkStatus(Order $order): TransactionStatus {
    $result = $this->sdk->api('request', [
      'version' => BaseOptions::VERSION,
      'action' => ActionStatuses::ACTION_STATUS,
      'order_id' => $order->uuid,
    ]);

    return TransactionStatus::createFromArray((array)$result);
  }
}