<?php

namespace App\Services\Payment\Entity\LiqPay;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Arr;

class TransactionData implements Arrayable {
  /**
   * Checkout LiqPay URL.
   *
   * @var string
   */
  protected $url;

  /**
   * Order data.
   *
   * @var string
   */
  protected $data;

  /**
   * Security key.
   *
   * @var string
   */
  protected $signature;

  /**
   * TransactionData constructor.
   *
   * @param string $url
   * @param string $data
   * @param string $signature
   */
  public function __construct(string $url, string $data, string $signature) {
    $this->url = $url;
    $this->data = $data;
    $this->signature = $signature;
  }

  /**
   * @return string
   */
  public function getUrl(): string {
    return $this->url;
  }

  /**
   * @return string
   */
  public function getData(): string {
    return $this->data;
  }

  /**
   * @return string
   */
  public function getSignature(): string {
    return $this->signature;
  }

  /**
   * @return array
   */
  public function toFormData(): array {
    return Arr::except($this->toArray(), ['url']);
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      'url' => $this->url,
      'data' => $this->data,
      'signature' => $this->signature,
    ];
  }

  /**
   * @param array $attributes
   * @return static
   */
  public static function createFromArray(array $attributes): self {
    return new self(
      (string)Arr::get($attributes, 'url'),
      (string)Arr::get($attributes, 'data'),
      (string)Arr::get($attributes, 'signature')
    );
  }
}