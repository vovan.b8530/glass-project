<?php

namespace App\Services\Payment\Entity\LiqPay;

use App\Enums\Payments\LiqPay\TransactionSuccessStatuses;
use App\Models\Order;
use Illuminate\Support\Arr;

class TransactionStatus {
  /**
   * @var string
   */
  protected $result;

  /**
   * @var int
   */
  protected $paymentId;

  /**
   * @var string
   */
  protected $action;

  /**
   * @var string
   */
  protected $status;

  /**
   * @var int
   */
  protected $version;

  /**
   * @var string
   */
  protected $type;

  /**
   * @var string
   */
  protected $payType;

  /**
   * @var string
   */
  protected $publicKey;

  /**
   * @var int
   */
  protected $acqId;

  /**
   * @var string
   */
  protected $orderId;

  /**
   * @var string
   */
  protected $liqpayOrderId;

  /**
   * @var string
   */
  protected $description;

  /**
   * @var string
   */
  protected $senderCardMask2;

  /**
   * @var string
   */
  protected $senderCardBank;

  /**
   * @var string
   */
  protected $senderCardType;

  /**
   * @var int
   */
  protected $senderCardCountry;

  /**
   * @var string
   */
  protected $ip;

  /**
   * @var float
   */
  protected $amount;

  /**
   * @var string
   */
  protected $currency;

  /**
   * @var float
   */
  protected $senderCommission;

  /**
   * @var float
   */
  protected $receiverCommission;

  /**
   * @var float
   */
  protected $agentCommission;

  /**
   * @var float
   */
  protected $amountDebit;

  /**
   * @var float
   */
  protected $amountCredit;

  /**
   * @var float
   */
  protected $commissionDebit;

  /**
   * @var float
   */
  protected $commissionCredit;

  /**
   * @var string
   */
  protected $currencyDebit;

  /**
   * @var string
   */
  protected $currencyCredit;

  /**
   * @var float
   */
  protected $senderBonus;

  /**
   * @var float
   */
  protected $amountBonus;

  /**
   * @var string
   */
  protected $mpiEci;

  /**
   * @var bool
   */
  protected $is3ds;

  /**
   * @var string
   */
  protected $language;

  /**
   * @var int
   */
  protected $createDate;

  /**
   * @var int
   */
  protected $endDate;

  /**
   * @var int
   */
  protected $transactionId;

  /**
   * TransactionStatus constructor.
   *
   * @param string $result
   * @param int $paymentId
   * @param string $action
   * @param string $status
   * @param int $version
   * @param string $type
   * @param string $payType
   * @param string $publicKey
   * @param int $acqId
   * @param string $orderId
   * @param string $liqpayOrderId
   * @param string $description
   * @param string $senderCardMask2
   * @param string $senderCardBank
   * @param string $senderCardType
   * @param int $senderCardCountry
   * @param string $ip
   * @param float $amount
   * @param string $currency
   * @param float $senderCommission
   * @param float $receiverCommission
   * @param float $agentCommission
   * @param float $amountDebit
   * @param float $amountCredit
   * @param float $commissionDebit
   * @param float $commissionCredit
   * @param string $currencyDebit
   * @param string $currencyCredit
   * @param float $senderBonus
   * @param float $amountBonus
   * @param string $mpiEci
   * @param bool $is3ds
   * @param string $language
   * @param int $createDate
   * @param int $endDate
   * @param int $transactionId
   */
  public function __construct(string $result,
                              int $paymentId,
                              string $action,
                              string $status,
                              int $version,
                              string $type,
                              string $payType,
                              string $publicKey,
                              int $acqId,
                              string $orderId,
                              string $liqpayOrderId,
                              string $description,
                              string $senderCardMask2,
                              string $senderCardBank,
                              string $senderCardType,
                              int $senderCardCountry,
                              string $ip,
                              float $amount,
                              string $currency,
                              float $senderCommission,
                              float $receiverCommission,
                              float $agentCommission,
                              float $amountDebit,
                              float $amountCredit,
                              float $commissionDebit,
                              float $commissionCredit,
                              string $currencyDebit,
                              string $currencyCredit,
                              float $senderBonus,
                              float $amountBonus,
                              string $mpiEci,
                              bool $is3ds,
                              string $language,
                              int $createDate,
                              int $endDate,
                              int $transactionId) {
    $this->result = $result;
    $this->paymentId = $paymentId;
    $this->action = $action;
    $this->status = $status;
    $this->version = $version;
    $this->type = $type;
    $this->payType = $payType;
    $this->publicKey = $publicKey;
    $this->acqId = $acqId;
    $this->orderId = $orderId;
    $this->liqpayOrderId = $liqpayOrderId;
    $this->description = $description;
    $this->senderCardMask2 = $senderCardMask2;
    $this->senderCardBank = $senderCardBank;
    $this->senderCardType = $senderCardType;
    $this->senderCardCountry = $senderCardCountry;
    $this->ip = $ip;
    $this->amount = $amount;
    $this->currency = $currency;
    $this->senderCommission = $senderCommission;
    $this->receiverCommission = $receiverCommission;
    $this->agentCommission = $agentCommission;
    $this->amountDebit = $amountDebit;
    $this->amountCredit = $amountCredit;
    $this->commissionDebit = $commissionDebit;
    $this->commissionCredit = $commissionCredit;
    $this->currencyDebit = $currencyDebit;
    $this->currencyCredit = $currencyCredit;
    $this->senderBonus = $senderBonus;
    $this->amountBonus = $amountBonus;
    $this->mpiEci = $mpiEci;
    $this->is3ds = $is3ds;
    $this->language = $language;
    $this->createDate = $createDate;
    $this->endDate = $endDate;
    $this->transactionId = $transactionId;
  }

  /**
   * @return string
   */
  public function getResult(): string {
    return $this->result;
  }

  /**
   * @return int
   */
  public function getPaymentId(): int {
    return $this->paymentId;
  }

  /**
   * @return string
   */
  public function getAction(): string {
    return $this->action;
  }

  /**
   * @return string
   */
  public function getStatus(): string {
    return $this->status;
  }

  /**
   * @return int
   */
  public function getVersion(): int {
    return $this->version;
  }

  /**
   * @return string
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * @return string
   */
  public function getPayType(): string {
    return $this->payType;
  }

  /**
   * @return string
   */
  public function getPublicKey(): string {
    return $this->publicKey;
  }

  /**
   * @return int
   */
  public function getAcqId(): int {
    return $this->acqId;
  }

  /**
   * @return string
   */
  public function getOrderId(): string {
    return $this->orderId;
  }

  /**
   * @return string
   */
  public function getLiqpayOrderId(): string {
    return $this->liqpayOrderId;
  }

  /**
   * @return string
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * @return string
   */
  public function getSenderCardMask2(): string {
    return $this->senderCardMask2;
  }

  /**
   * @return string
   */
  public function getSenderCardBank(): string {
    return $this->senderCardBank;
  }

  /**
   * @return string
   */
  public function getSenderCardType(): string {
    return $this->senderCardType;
  }

  /**
   * @return int
   */
  public function getSenderCardCountry(): int {
    return $this->senderCardCountry;
  }

  /**
   * @return string
   */
  public function getIp(): string {
    return $this->ip;
  }

  /**
   * @return float
   */
  public function getAmount(): float {
    return $this->amount;
  }

  /**
   * @return string
   */
  public function getCurrency(): string {
    return $this->currency;
  }

  /**
   * @return float
   */
  public function getSenderCommission(): float {
    return $this->senderCommission;
  }

  /**
   * @return float
   */
  public function getReceiverCommission(): float {
    return $this->receiverCommission;
  }

  /**
   * @return float
   */
  public function getAgentCommission(): float {
    return $this->agentCommission;
  }

  /**
   * @return float
   */
  public function getAmountDebit(): float {
    return $this->amountDebit;
  }

  /**
   * @return float
   */
  public function getAmountCredit(): float {
    return $this->amountCredit;
  }

  /**
   * @return float
   */
  public function getCommissionDebit(): float {
    return $this->commissionDebit;
  }

  /**
   * @return float
   */
  public function getCommissionCredit(): float {
    return $this->commissionCredit;
  }

  /**
   * @return string
   */
  public function getCurrencyDebit(): string {
    return $this->currencyDebit;
  }

  /**
   * @return string
   */
  public function getCurrencyCredit(): string {
    return $this->currencyCredit;
  }

  /**
   * @return float
   */
  public function getSenderBonus(): float {
    return $this->senderBonus;
  }

  /**
   * @return float
   */
  public function getAmountBonus(): float {
    return $this->amountBonus;
  }

  /**
   * @return string
   */
  public function getMpiEci(): string {
    return $this->mpiEci;
  }

  /**
   * @return bool
   */
  public function isIs3ds(): bool {
    return $this->is3ds;
  }

  /**
   * @return string
   */
  public function getLanguage(): string {
    return $this->language;
  }

  /**
   * @return int
   */
  public function getCreateDate(): int {
    return $this->createDate;
  }

  /**
   * @return int
   */
  public function getEndDate(): int {
    return $this->endDate;
  }

  /**
   * @return int
   */
  public function getTransactionId(): int {
    return $this->transactionId;
  }

  /**
   * @return bool
   */
  public function isSuccess(): bool {
    return in_array($this->status, TransactionSuccessStatuses::getValues());
  }

  /**
   * @param Order $order
   * @return bool
   */
  public function isCorrect(Order $order): bool {
    return $this->isSuccess() && $this->orderId == $order->uuid;
  }

  /**
   * @param array $attributes
   * @return static
   */
  public static function createFromArray(array $attributes): self {
    return new self(
      Arr::get($attributes, 'result'),
      Arr::get($attributes, 'payment_id'),
      Arr::get($attributes, 'action'),
      Arr::get($attributes, 'status'),
      Arr::get($attributes, 'version'),
      Arr::get($attributes, 'type'),
      Arr::get($attributes, 'paytype'),
      Arr::get($attributes, 'public_key'),
      Arr::get($attributes, 'acq_id'),
      Arr::get($attributes, 'order_id'),
      Arr::get($attributes, 'liqpay_order_id'),
      Arr::get($attributes, 'description'),
      Arr::get($attributes, 'sender_card_mask2'),
      Arr::get($attributes, 'sender_card_bank'),
      Arr::get($attributes, 'sender_card_type'),
      Arr::get($attributes, 'sender_card_country'),
      Arr::get($attributes, 'ip'),
      Arr::get($attributes, 'amount'),
      Arr::get($attributes, 'currency'),
      Arr::get($attributes, 'sender_commission'),
      Arr::get($attributes, 'receiver_commission'),
      Arr::get($attributes, 'agent_commission'),
      Arr::get($attributes, 'amount_debit'),
      Arr::get($attributes, 'amount_credit'),
      Arr::get($attributes, 'commission_debit'),
      Arr::get($attributes, 'commission_credit'),
      Arr::get($attributes, 'currency_debit'),
      Arr::get($attributes, 'currency_credit'),
      Arr::get($attributes, 'sender_bonus'),
      Arr::get($attributes, 'amount_bonus'),
      Arr::get($attributes, 'mpi_eci'),
      Arr::get($attributes, 'is_3ds'),
      Arr::get($attributes, 'language'),
      Arr::get($attributes, 'create_date'),
      Arr::get($attributes, 'end_date'),
      Arr::get($attributes, 'transaction_id')
    );
  }
}