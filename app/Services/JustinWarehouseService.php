<?php

namespace App\Services;


use App\Justin\Entity\Warehouse as WarehouseEntity;
use App\Models\JustinCity;
use App\Models\JustinWarehouse;
use App\Services\Helpers\PromiseActionsTrait;

class JustinWarehouseService {
  use PromiseActionsTrait;

  /**
   * @var JustinWarehouse
   */
  private $warehouse;

  /**
   * JustinWarehouseService constructor.
   * @param JustinWarehouse $warehouse
   */
  public function __construct(JustinWarehouse $warehouse) {
    $this->warehouse = $warehouse;
  }

  /**
   * @return JustinWarehouse
   */
  public function getJustinWarehouse() {
    return $this->warehouse;
  }

  /**
   * Check is warehouse created in DB or no.
   *
   * @return bool
   */
  public function isNewJustinWarehouse(): bool {
    return !$this->warehouse->exists;
  }

  /**
   * @param WarehouseEntity $entity
   * @return JustinWarehouseService
   */
  public function changeAttributes(WarehouseEntity $entity): self {
    $this->warehouse->fill($entity->toArray());
    return $this;
  }

  /**
   * @param JustinCity $city
   * @return $this
   */
  public function attachCity(JustinCity $city): self {
    $this->warehouse->justinCity()->associate($city);
    return $this;
  }

  /**
   * @return JustinWarehouseService
   */
  public function commitChanges(): self {
    $this->warehouse->save();
    $this->releasePromiseActions();

    return $this;
  }
}
