<?php

namespace App\Services;

use App\Models\JustinCity;
use App\Services\Helpers\PromiseActionsTrait;
use App\Justin\Entity\City as CityEntity;

class JustinCityService {
  use PromiseActionsTrait;

  /**
   * @var JustinCity
   */
  private $city;

  /**
   * JustinCityService constructor.
   *
   * @param JustinCity $city
   */
  public function __construct(JustinCity $city) {
    $this->city = $city;
  }

  /**
   * @return JustinCity
   */
  public function getJustinCity() {
    return $this->city;
  }

  /**
   * Check is city created in DB or no.
   *
   * @return bool
   */
  public function isNewJustinCity(): bool {
    return !$this->city->exists;
  }

  /**
   * @param CityEntity $entity
   * @return JustinCityService
   */
  public function changeAttributes(CityEntity $entity): self {
    $this->city->fill($entity->toArray());
    return $this;
  }

  /**
   * @return JustinCityService
   */
  public function commitChanges(): self {
    $this->city->save();
    $this->releasePromiseActions();

    return $this;
  }
}
