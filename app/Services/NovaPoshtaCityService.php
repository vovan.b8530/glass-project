<?php

namespace App\Services;

use App\Models\NovaPoshtaCity;
use App\Services\Helpers\PromiseActionsTrait;
use App\NovaPoshta\Entity\City as CityEntity;

class NovaPoshtaCityService {
  use PromiseActionsTrait;

  /**
   * @var NovaPoshtaCity
   */
  private $city;

  /**
   * NovaPoshtaCityService constructor.
   *
   * @param NovaPoshtaCity $city
   */
  public function __construct(NovaPoshtaCity $city) {
    $this->city = $city;
  }

  /**
   * @return NovaPoshtaCity
   */
  public function getNovaPoshtaCity() {
    return $this->city;
  }

  /**
   * Check is city created in DB or no.
   *
   * @return bool
   */
  public function isNewNovaPoshtaCity(): bool {
    return !$this->city->exists;
  }

  /**
   * @param CityEntity $entity
   * @return NovaPoshtaCityService
   */
  public function changeAttributes(CityEntity $entity): self {
    $this->city->fill($entity->toArray());
    return $this;
  }

  /**
   * @return NovaPoshtaCityService
   */
  public function commitChanges(): self {
    $this->city->save();
    $this->releasePromiseActions();

    return $this;
  }
}
