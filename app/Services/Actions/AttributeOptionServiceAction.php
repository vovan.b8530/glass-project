<?php

namespace App\Services\Actions;

use App\DTO\AttributeOptionDto;
use App\Models\AttributeOption;
use App\Services\AttributeOptionService;
use App\Services\Helpers\ReorderAction;

class AttributeOptionServiceAction {
  use ReorderAction;

  /**
   * @param AttributeOptionDto $attributeOptionDto
   * @return AttributeOption
   */
  public function createAttributeOption(AttributeOptionDto $attributeOptionDto): AttributeOption {
    return $this->saveAttributeOption(new AttributeOption(), $attributeOptionDto);
  }

  /**
   * @param AttributeOption $attributeOption
   * @param AttributeOptionDto $dto
   * @return AttributeOption
   */
  public function updateAttributeOption(AttributeOption $attributeOption, AttributeOptionDto $dto): AttributeOption {
    return $this->saveAttributeOption($attributeOption, $dto);
  }

  /**
   * @param AttributeOption $attributeOption
   * @param AttributeOptionDto $attributeOptionDto
   * @return AttributeOption
   */
  public function saveAttributeOption(AttributeOption $attributeOption, AttributeOptionDto $attributeOptionDto): AttributeOption {
    $serviceItemService = new AttributeOptionService($attributeOption);
    if ($attributeOptionDto->hasFile())
      $serviceItemService->uploadNewFile($attributeOptionDto->getUploadedFile());

    if ($attributeOptionDto->hasDeletePicture())
      $serviceItemService->deletePicture();

    $serviceItemService
      ->changeAttributes($attributeOptionDto)
      ->commitChanges();

    return $serviceItemService->getAttributeOption();
  }
}