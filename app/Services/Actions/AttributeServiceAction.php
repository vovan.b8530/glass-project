<?php

namespace App\Services\Actions;


use App\DTO\AttributeDto;
use App\Models\Attribute;
use App\Services\AttributeService;
use App\Services\Helpers\ReorderAction;

class AttributeServiceAction {
  use ReorderAction;

  /**
   * @param AttributeDto $attributeDto
   * @return Attribute
   */
  public function createAttribute(AttributeDto $attributeDto): Attribute {
    return $this->saveAttribute(new Attribute(), $attributeDto);
  }

  /**
   * @param Attribute $attribute
   * @param AttributeDto $dto
   * @return Attribute
   */
  public function updateAttribute(Attribute $attribute, AttributeDto $dto): Attribute {
    return $this->saveAttribute($attribute, $dto);
  }

  /**
   * @param Attribute $attribute
   * @param AttributeDto $attributeDto
   * @return Attribute
   */
  public function saveAttribute(Attribute $attribute, AttributeDto $attributeDto): Attribute {
    $serviceItemService = new AttributeService($attribute);

    $serviceItemService
      ->changeAttributes($attributeDto)
      ->commitChanges();

    return $serviceItemService->getAttribute();
  }
}