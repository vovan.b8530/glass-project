<?php

namespace App\Services\Actions;

use App\DTO\OrderDto;
use App\Models\Order;
use App\Services\OrderService;

class OrderServiceAction {

  /**
   * @param Order $order
   * @param OrderDto $dto
   * @return Order
   */
  public function updateOrder(Order $order, OrderDto $dto): Order {
    return $this->saveOrder($order, $dto);
  }

  /**
   * @param Order $order
   * @param OrderDto $dto
   * @return Order
   */
  protected function saveOrder(Order $order, OrderDto $dto): Order {
    $service = new OrderService($order);

    $service
      ->changeAttributes($dto)
      ->commitChanges();

    return $service->getOrder();
  }
}