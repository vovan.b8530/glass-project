<?php

namespace App\Services\Actions;

use App\DTO\SubCategoryDto;
use App\Models\SubCategory;
use App\Services\Helpers\ReorderAction;
use App\Services\SubCategoryService;

class SubCategoryServiceAction {
  use ReorderAction;

  /**
   * @param SubCategoryDto $subCategoryDto
   * @return SubCategory
   */
  public function createSubCategory(SubCategoryDto $subCategoryDto): SubCategory {
    return $this->saveSubCategory(new SubCategory(), $subCategoryDto);
  }

  /**
   * @param SubCategory $subCategory
   * @param SubCategoryDto $dto
   * @return SubCategory
   */
  public function updateSubCategory(SubCategory $subCategory, SubCategoryDto $dto): SubCategory {
    return $this->saveSubCategory($subCategory, $dto);
  }

  /**
   * @param SubCategory $subCategory
   * @param SubCategoryDto $subCategoryDto
   * @return SubCategory
   */
  public function saveSubCategory(SubCategory $subCategory, SubCategoryDto $subCategoryDto): SubCategory {
    $serviceItemService = new SubCategoryService($subCategory);

    $serviceItemService
      ->changeAttributes($subCategoryDto)
      ->commitChanges();

    return $serviceItemService->getSubCategory();
  }
}