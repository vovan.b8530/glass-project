<?php

namespace App\Services;

use App\DTO\AttributeOptionDto;
use App\Helpers\FileHelper;
use App\Models\AttributeOption;
use App\Services\Helpers\PromiseActionsTrait;
use Illuminate\Http\UploadedFile;

class AttributeOptionService {
  use PromiseActionsTrait;

  /**
   * @var AttributeOption
   */
  private $attributeOption;

  /**
   * AttributeOptionItemService constructor.
   *
   * @param AttributeOption $attributeOption
   */
  public function __construct(AttributeOption $attributeOption) {
    $this->attributeOption = $attributeOption;
  }

  /**
   * @return AttributeOption
   */
  public function getAttributeOption() {
    return $this->attributeOption;
  }

  /**
   * @param AttributeOptionDto $attributeOptionDto
   * @return AttributeOptionService
   */
  public function changeAttributes(AttributeOptionDto $attributeOptionDto): self {
    $this->attributeOption->fill($attributeOptionDto->toArray());
    return $this;
  }

  /**
   * @param UploadedFile $file
   * @return AttributeOptionService
   */
  public function uploadNewFile(UploadedFile $file): self {
    $newFileName = FileHelper::generateNewName($file->clientExtension());
    if ($file->storeAs("public/" . $this->attributeOption->directoryStorage(), $newFileName))
      $this->resetFile($newFileName);

    return $this;
  }

  /**
   * @param string $fileName
   * @return AttributeOptionService
   */
  public function resetFile(string $fileName): self {
    if ($this->attributeOption->picture_file_name)
      $this->deletePicture();
    $this->attributeOption->picture_file_name = $fileName;

    return $this;
  }

  /**
   * @return AttributeOptionService
   */
  public function deletePicture(): self {
    if ($this->attributeOption->deleteFile($this->attributeOption->picture_file_name))
      $this->attributeOption->picture_file_name = null;

    return $this;
  }

  /**
   * @return AttributeOptionService
   */
  public function commitChanges(): self {
    $this->attributeOption->save();

    $this->releasePromiseActions();

    return $this;
  }

}