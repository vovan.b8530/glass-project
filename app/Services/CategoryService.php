<?php

namespace App\Services;

use App\DTO\CategoryDto;
use App\Models\Category;
use App\Services\Helpers\PromiseActionsTrait;

class CategoryService {
  use PromiseActionsTrait;

  /**
   * @var Category
   */
  private $category;

  /**
   * CategoryItemService constructor.
   *
   * @param Category $category
   */
  public function __construct(Category $category) {
    $this->category = $category;
  }

  /**
   * @return Category
   */
  public function getCategory() {
    return $this->category;
  }

  /**
   * @param CategoryDto $categoryDto
   * @return CategoryService
   */
  public function changeAttributes(CategoryDto $categoryDto): self {
    $this->category->fill($categoryDto->toArray());
    return $this;
  }

  /**
   * @return CategoryService
   */
  public function commitChanges(): self {
    $this->category->save();

    $this->releasePromiseActions();

    return $this;
  }
}