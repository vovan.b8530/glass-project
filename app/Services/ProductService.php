<?php

namespace App\Services;

use App\DTO\ProductDto;
use App\Models\Product;
use App\Models\ProductPicture;
use App\Services\Helpers\PromiseActionsTrait;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

class ProductService {
  use PromiseActionsTrait;

  /**
   * @var Product
   */
  private $product;

  /**
   * ProductService constructor.
   *
   * @param Product $product
   */
  public function __construct(Product $product) {
    $this->product = $product;
  }

  /**
   * @return Product
   */
  public function getProduct() {
    return $this->product;
  }

  /**
   * @param ProductDto $productDto
   * @return $this
   */
  public function changeAttributes(ProductDto $productDto): self {
    $this->product->fill($productDto->toArray());
    return $this;
  }

  /**
   * @param bool $isMain
   * @param UploadedFile[]|array $files
   * @return ProductService
   */
  public function uploadFiles(array $files, bool $isMain = false): self {
    $this->recordPromiseAction(function () use ($files, $isMain) {
      foreach ($files as $file) {
        $newPicture = new ProductPicture();
        /* @var UploadedFile $uploadedImage */
        $uploadedImage = $file;
        $pictureService = new PictureService($uploadedImage);
        $newFileName = $pictureService->storeToFolder($newPicture->directoryStorage());
        if (!empty($newFileName)) {
          $newPicture->picture_file_name = $newFileName;
          $newThumbFileName = "thumb_" . $newFileName;

          if ($pictureService->createThumbnail($newPicture->getFilePath($newFileName), $newPicture->getFilePath($newThumbFileName))) {
            $newPicture->picture_thumb_file_name = $newThumbFileName;
          }
          if ($isMain) {
            $newPicture->is_main = 1;
            /* @var ProductPicture $oldMainPicture */
            $oldMainPicture = $this->product->mainPicture()->first();
            if ($oldMainPicture) {
              $oldMainPicture->deleteFile($oldMainPicture->picture_file_name);
              $oldMainPicture->deleteFile($oldMainPicture->picture_thumb_file_name);
              $oldMainPicture->delete();
            }
            $this->product->mainPicture()->save($newPicture);
          } else {
            $this->product->pictures()->save($newPicture);
          }
        }
      }
    });
    return $this;
  }

  /**
   * @param array $picturesIdToDelete
   * @return ProductService
   */
  public function deleteFiles(array $picturesIdToDelete): self {
    $this->recordPromiseAction(function () use ($picturesIdToDelete) {
      /* @var Collection|ProductPicture[] $deletingPictures */
      $deletingPictures = $this->product->pictures()
        ->whereIn('id', $picturesIdToDelete)
        ->get();

      $deletingPictures->each(function ($picture) {
        /* @var ProductPicture $picture */
        $picture->deleteFile($picture->picture_file_name);
        $picture->deleteFile($picture->picture_thumb_file_name);
        $picture->delete();
      });
    });
    return $this;
  }

  /**
   * @param ProductPicture $picture
   * @return bool|null
   * @throws \Exception
   */
  public function deletePictureThumb(ProductPicture $picture) {
    $picture->deleteFile($picture->picture_file_name);
    $picture->deleteFile($picture->picture_thumb_file_name);
    return $picture->delete();
  }

  /**
   * @return ProductService
   */
  public function commitChanges(): self {
    $this->product->save();

    $this->releasePromiseActions();

    return $this;
  }

  /**
   * @param array $attrOptions
   * @return $this
   */
  public function attachAttrOptions($attrOptions) {
    $this->recordPromiseAction(function () use ($attrOptions) {
      $oldAttrOptions = $this->product->attributeOptions;
      if (!empty($oldAttrOption)) {
        $oldAttrOptions->each(function ($oldAttrOption) {
          return $oldAttrOption->delete();
        });
      }

      $this->product->attributeOptions()->sync($attrOptions);

    });

    return $this;
  }

  /**
   * @param string $title
   * @return string
   */
  public function generateProductHash(string $title) {
    return Hash::make($title . Carbon::now());
  }

  /**
   * @param string $title
   * @param array $productIds
   * @return $this
   */
  public function generateHashByProducts(string $title, array $productIds = []) {
    $hash = $this->generateProductHash($title);
    $this->product->product_hash = $hash;

    if ($productIds) {
      $products = Product::query()->whereIn('id', $productIds)->get();
      $products->each(function ($product) use ($hash) {
        $product->product_hash = $hash;
        $product->save();
      });
    }
    return $this;
  }

}