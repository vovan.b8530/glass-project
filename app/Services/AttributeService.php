<?php

namespace App\Services;


use App\DTO\AttributeDto;
use App\Models\Attribute;
use App\Services\Helpers\PromiseActionsTrait;

class AttributeService {
  use PromiseActionsTrait;

  /**
   * @var Attribute
   */
  private $attribute;

  /**
   * AttributeItemService constructor.
   *
   * @param Attribute $attribute
   */
  public function __construct(Attribute $attribute) {
    $this->attribute = $attribute;
  }

  /**
   * @return Attribute
   */
  public function getAttribute() {
    return $this->attribute;
  }

  /**
   * @param AttributeDto $attributeDto
   * @return AttributeService
   */
  public function changeAttributes(AttributeDto $attributeDto): self {
    $this->attribute->fill($attributeDto->toArray());
    return $this;
  }

  /**
   * @return AttributeService
   */
  public function commitChanges(): self {
    $this->attribute->save();

    $this->releasePromiseActions();

    return $this;
  }
}