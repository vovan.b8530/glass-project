<?php

namespace App\Services;


use App\DTO\SubCategoryDto;
use App\Models\SubCategory;
use App\Services\Helpers\PromiseActionsTrait;

class SubCategoryService {
  use PromiseActionsTrait;

  /**
   * @var SubCategory
   */
  private $subCategory;

  /**
   * SubCategoryItemService constructor.
   *
   * @param SubCategory $subCategory
   */
  public function __construct(SubCategory $subCategory) {
    $this->subCategory = $subCategory;
  }

  /**
   * @return SubCategory
   */
  public function getSubCategory() {
    return $this->subCategory;
  }

  /**
   * @return bool
   */
  public function isNewSubCategory(): bool {
    return !$this->subCategory->exists;
  }

  /**
   * @param SubCategoryDto $subCategoryDto
   * @return SubCategoryService
   */
  public function changeAttributes(SubCategoryDto $subCategoryDto): self {
    $this->subCategory->fill($subCategoryDto->toArray());
    return $this;
  }

  /**
   * @return SubCategoryService
   */
  public function commitChanges(): self {
    $this->subCategory->save();

    $this->releasePromiseActions();

    return $this;
  }
}