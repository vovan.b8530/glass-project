<?php

namespace App\Services;

use App\DTO\OrderDto;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Services\Helpers\PromiseActionsTrait;
use Illuminate\Support\Str;

class OrderService {
  use PromiseActionsTrait;

  /**
   * @var Order
   */
  private $order;

  /**
   * OrderService constructor.
   *
   * @param Order $order
   */
  public function __construct(Order $order) {
    $this->order = $order;

    if($this->isNewOrder())
      $this->recordPromiseMethod('generateUuid', [], 'before');
  }

  /**
   * @return $this
   */
  protected function generateUuid(): self {
    $this->order->uuid = (string) Str::uuid();
    return $this;
  }

  /**
   * @return Order
   */
  public function getOrder(): Order {
    return $this->order;
  }

  /**
   * @return bool
   */
  public function isNewOrder(): bool {
    return !$this->order->exists;
  }

  /**
   * @param OrderDto $orderDto
   * @return $this
   */
  public function changeAttributes(OrderDto $orderDto): self {
    $this->order->fill($orderDto->toArray());
    return $this;
  }

  /**
   * @param Product $product
   * @return OrderService
   */
  public function addProduct(Product $product): self {
    $this->recordPromiseAction(function () use ($product) {
      $newOrderProduct = new OrderProduct();

      $newOrderProduct->copyPriceFromProduct($product);

      $newOrderProduct->product()->associate($product);

      $this->order
        ->orderProducts()
        ->save($newOrderProduct);

      $this->order->unsetRelation('orderProducts');
    });

    $this->recordPromiseMethod('reloadRelations');
    $this->recordPromiseMethod('recalculateAmounts');

    return $this;
  }

  /**
   * @return $this
   */
  public function makeNew(): self {
    $this->order->toNewStatus();
    return $this;
  }

  /**
   * @return $this
   */
  public function toAccept(): self {
    $this->order
      ->clearCookieHash()
      ->toAcceptStatus();

    return $this;
  }

  /**
   * @return OrderService
   */
  public function confirm(): self {
    $this->order
      ->clearCookieHash()
      ->toConfirmStatus();

    return $this;
  }

  public function toCancel(): self {
    $this->order
      ->clearCookieHash()
      ->toCancelStatus();

    return $this;
  }

  /**
   * @return $this
   */
  public function commitChanges(): self {
    $this->releasePromiseActions('before');

    $this->order->save();

    $this->releasePromiseActions();

    return $this;
  }

  /**
   * @param bool $save
   * @return OrderService
   */
  public function recalculateAmounts(bool $save = true): self {
    $this->order->total_amount = $this->order->orderProducts->priceTotalAmount();
    if ($save)
      $this->order->save();

    return $this;
  }

  /**
   * @return OrderService
   */
  protected function reloadRelations(): self {
    $this->order->unsetRelation('orderProducts');
    $this->order->load(['orderProducts']);

    return $this;
  }
}
