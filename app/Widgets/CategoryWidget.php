<?php

namespace App\Widgets;

use App\Models\Category;
use \Arrilot\Widgets\AbstractWidget;

class CategoryWidget extends AbstractWidget {
  /**
   * Treat this method as a controller action.
   * Return view() or other content to display.
   */
  public function run() {
    $categories = Category::query()
      ->with(['subCategories' => function ($query) {
        $query->orderBy('sub_categories.display_order');
      }])
      ->orderBy('categories.display_order')
      ->get();

    return view('widgets.categories-sidebar', [
      'categories' => $categories,
    ]);
  }
}