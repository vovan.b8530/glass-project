<?php

namespace App\Widgets;

use App\Models\Attribute;
use \Arrilot\Widgets\AbstractWidget;

class AttributeWidget extends AbstractWidget {
  /**
   * Treat this method as a controller action.
   * Return view() or other content to display.
   */
  public function run() {
    $attributes = Attribute::query()
      ->with(['attributeOptions' => function ($query) {
        $query->orderBy('display_order');
      }])
      ->orderBy('display_order')
      ->get();

    return view('widgets.attributes-sidebar', [
      'attributes' => $attributes,
      'selected' => request()->get('attributeOptionsIds', []),
    ]);
  }
}