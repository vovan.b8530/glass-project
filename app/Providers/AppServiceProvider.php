<?php

namespace App\Providers;

use App\Models\Attribute;
use App\Models\AttributeOption;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductPicture;
use App\Models\SubCategory;
use App\NovaPoshta\Connector\HttpConnector;
use App\NovaPoshta\Models\BaseModel;
use App\NovaPoshta\Models\Transport;
use App\Observers\GenerateOrderIndexObserver;
use App\Support\Guzzle\BaseHttpClient;
use App\Support\Guzzle\HttpClient;
use App\Support\LiqPay\LiqPay;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
  /**
   * @throws \Illuminate\Contracts\Container\BindingResolutionException
   */
  public function register() {
    $this->app->instance(BaseHttpClient::class, new HttpClient(config('services.guzzle_http_client')));
    $this->app->instance(HttpConnector::class, new HttpConnector(config('services.novaposhta')));

    $this->app->instance(
      Transport::class,
      new Transport(config('services.novaposhta.api_key'), $this->app->make(HttpConnector::class))
    );

    $this->app->instance(BaseModel::class, new BaseModel($this->app->make(Transport::class)));

    $this->app->instance(LiqPay::class, new LiqPay(config('services.liqpay.public_key'), config('services.liqpay.private_key')));
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot() {
    $this->registerObservers();
  }

  public function registerObservers() {
    // generate order indexes
    Attribute::observe(GenerateOrderIndexObserver::class);
    Category::observe(GenerateOrderIndexObserver::class);
    SubCategory::observe(GenerateOrderIndexObserver::class);
    AttributeOption::observe(GenerateOrderIndexObserver::class);
    Product::observe(GenerateOrderIndexObserver::class);
    ProductPicture::observe(GenerateOrderIndexObserver::class);
  }
}
