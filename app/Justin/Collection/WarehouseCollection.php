<?php

namespace App\Justin\Collection;

use App\Justin\Entity\BaseEntity;
use App\Justin\Entity\Warehouse;

class WarehouseCollection extends BaseCollection {
  /**
   * @param array $item
   * @return BaseEntity
   */
  public function hydrate(array $item): BaseEntity {
    return Warehouse::createFromArray($item);
  }
}