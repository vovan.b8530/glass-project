<?php

namespace App\Justin\Collection;

use App\Justin\Entity\BaseEntity;
use App\Justin\Entity\City;

class CityCollection extends BaseCollection {
  /**
   * @param array $item
   * @return BaseEntity
   */
  public function hydrate(array $item): BaseEntity {
    return City::createFromArray($item);
  }
}