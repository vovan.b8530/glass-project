<?php

namespace App\Justin\Seeder;

use App\Models\JustinCity;
use App\Justin\Collection\CityCollection;
use App\Justin\Collection\WarehouseCollection;

class BaseSeeder {
  /**
   * @param CityCollection $cityCollection
   * @return CitySeeder
   */
  public function citySeeder(CityCollection $cityCollection): CitySeeder {
    return new CitySeeder($cityCollection);
  }

  /**
   * @param WarehouseCollection $warehouseCollection
   * @param JustinCity $city
   *
   * @return WarehouseSeeder
   */
  public function warehouseSeeder(WarehouseCollection $warehouseCollection, JustinCity $city): WarehouseSeeder {
    return new WarehouseSeeder($warehouseCollection, $city);
  }
}
