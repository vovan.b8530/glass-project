<?php

namespace App\Justin\Seeder;

use App\Models\JustinCity;
use App\Models\JustinWarehouse;
use App\Justin\Collection\WarehouseCollection as WarehouseEntityCollection;
use App\Justin\Entity\Warehouse as WarehouseEntity;
use App\Justin\Collection\WarehouseCollection;
use App\Services\JustinWarehouseService;
use Illuminate\Database\Eloquent\Collection;

class WarehouseSeeder {
  /**
   * @var WarehouseEntityCollection
   */
  protected $warehouses;

  /**
   * @var JustinCity
   */
  protected $city;

  /**
   * WarehouseSeeder constructor.
   *
   * @param WarehouseCollection $entities
   * @param JustinCity $city
   */
  public function __construct(WarehouseEntityCollection $entities, JustinCity $city) {
//	  dd($city);
    $this->warehouses = $entities->keyBy(function ($entity) {
      /* @var WarehouseEntity $entity */
      return $entity->getCode();
    });

    $this->city = $city;
  }

  /**
   * @return Collection
   */
  protected function fetchExistModels(): Collection {
    /* @var Collection $warehouses */
    $warehouses = JustinWarehouse::query()
      ->whereIn('code', $this->warehouses->keys())
      ->get()
      ->keyBy('code');

    return $warehouses;
  }

  /**
   * @param WarehouseEntity $warehouse
   *
   * @return JustinWarehouse
   */
  protected function createNewWarehouse(WarehouseEntity $warehouse): JustinWarehouse {
    return $this->saveWarehouse(new JustinWarehouse(), $warehouse);
  }

  /**
   * @param JustinWarehouse $model
   * @param WarehouseEntity $entity
   *
   * @return JustinWarehouse
   */
  protected function saveWarehouse(JustinWarehouse $model, WarehouseEntity $entity): JustinWarehouse {
    $service = new JustinWarehouseService($model);

    $service
      ->changeAttributes($entity)
      ->attachCity($this->city);

    $service->commitChanges();

    return $service->getJustinWarehouse();
  }

  /**
   * @return Collection
   */
  public function run(): Collection {
    $warehouseModels = $this->fetchExistModels();

    $warehouseModels = $warehouseModels->map(function ($warehouse) {
      /* @var JustinWarehouse $warehouse */
      $entity = $this->warehouses->get($warehouse->code);

      if ($entity)
        $warehouse = $this->saveWarehouse($warehouse, $entity);

      return $warehouse;
    });

    $this->warehouses
      ->diffKeys($warehouseModels->all())
      ->each(function ($warehouse) use ($warehouseModels) {
        /* @var WarehouseEntity $warehouse */
        $warehouseModels->push($this->createNewWarehouse($warehouse));
      });

    return $warehouseModels;
  }
}
