<?php

namespace App\Justin\Seeder;

use App\Models\JustinCity;
use App\Justin\Collection\CityCollection as CityEntityCollection;
use App\Justin\Entity\City as CityEntity;
use App\Services\JustinCityService;
use Illuminate\Database\Eloquent\Collection;

class CitySeeder {
  /**
   * @var CityEntityCollection
   */
  protected $cities;

  /**
   * CitySeeder constructor.
   * @param CityEntityCollection $cities
   */
  public function __construct(CityEntityCollection $cities) {
    $this->cities = $cities->keyBy(function ($city) {
      /* @var CityEntity $city */
      return $city->getCode();
    });
  }

  /**
   * @return Collection
   */
  protected function fetchExistModels(): Collection {
    /* @var Collection $cities */
    $cities = JustinCity::query()
      ->whereIn('code', $this->cities->keys())
      ->get()
      ->keyBy('code');

    return $cities;
  }

  /**
   * @param CityEntity $city
   * @return JustinCity
   */
  protected function createNewCity(CityEntity $city): JustinCity {
    return $this->saveCity(new JustinCity(), $city);
  }

  /**
   * @param JustinCity $model
   * @param CityEntity $city
   * @return JustinCity
   */
  protected function saveCity(JustinCity $model, CityEntity $city): JustinCity {
    $service = new JustinCityService($model);
    $service->changeAttributes($city);
    $service->commitChanges();

    return $service->getJustinCity();
  }

  /**
   * @param CityEntity $entity
   * @param CityModelCollection $collection
   *
   * @return CityModelCollection
   */
  protected function seed(CityEntity $entity, CityModelCollection $collection): CityModelCollection {
    $newCity = $this->createNewCity($entity);
    $collection->push($newCity);

    return $collection;
  }

  /**
   * @return Collection
   */
  public function run(): Collection {
    $cityModels = $this->fetchExistModels();

    $cityModels = $cityModels->map(function ($city) {
      /* @var JustinCity $city */
      $entity = $this->cities->get($city->code);

      if ($entity)
        $city = $this->saveCity($city, $entity);

      return $city;
    });

    $this->cities
      ->diffKeys($cityModels->all())
      ->each(function ($city) use ($cityModels) {
        /* @var CityEntity $city */
        $cityModels->push($this->createNewCity($city));
      });

    return $cityModels;
  }
}
