<?php

namespace App\Justin\Entity;

use Illuminate\Support\Arr;

class City extends BaseEntity {

  /**
   * Унікальний внутрішній ідентифікатор обласного району..
   *
   * @var string
   */
  protected $uuid;

  /**
   * Код населеного пункту.
   *
   * @var string
   */
  protected $code;

  /**
   * Назва населеного пункту вибраною в запиті мовою.
   *
   * @var string
   */
  public $descr;

  /**
   * Код КОАТУУ населеного пункту.
   *
   * @var string
   */
  protected $SCOATOU;

  /**
   * City constructor.
   *
   * @param string $uuid
   * @param string $code
   * @param string $descr
   * //   * @param array $objectOwner
   * //   * @param array $district
   * @param string $SCOATOU
   */
  public function __construct(string $uuid,
                              string $code,
                              string $descr,
                              string $SCOATOU) {
    $this->uuid = $uuid;
    $this->code = $code;
    $this->descr = $descr;
    $this->SCOATOU = $SCOATOU;
  }

  /**
   * @return string
   */
  public function getCode(): string {
    return $this->code;
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      "uuid" => $this->uuid,
      "code" => $this->code,
      "descr" => $this->descr,
      "SCOATOU" => $this->SCOATOU,
    ];
  }

  /**
   * @param array $attributes
   * @return BaseEntity
   */
  public static function createFromArray(array $attributes): BaseEntity {
    return new self(
      (string)Arr::get($attributes, 'uuid', ''),
      (string)Arr::get($attributes, 'code', ''),
      (string)Arr::get($attributes, 'descr', ''),
      (string)Arr::get($attributes, 'SCOATOU', '')
    );
  }
}
