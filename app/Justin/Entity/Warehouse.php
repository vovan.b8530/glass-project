<?php

namespace App\Justin\Entity;

use Illuminate\Support\Arr;

/**
 * Отделения.
 */
class Warehouse extends BaseEntity {
  /**
   * Назва відділення.
   *
   * @var string
   */
  protected $descr;

  /**
   * Код (номер) відділення.
   *
   * @var string
   */
  protected $code;

  /**
   * Код КОАТУУ населеного пункту.
   *
   * @var string
   */
  public $citySCOATOU;

  /**
   * Повний адрес відділення на вибраній в запиті мові.
   *
   * @var string
   */
  protected $address;

  /**
   * Широта відділення.
   *
   * @var string
   */
  protected $lat;

  /**
   * Довгота відділення.
   *
   * @var string
   */
  protected $lng;

  /**
   * Номер відділення.
   *
   * @var string
   */
  protected $departNumber;

  /**
   * Номер дома, в якому знаходиться відділення.
   *
   * @var string
   */
  protected $houseNumber;


  /**
   * Warehouse constructor.
   * @param string $descr
   * @param string $code
   * @param string $citySCOATOU
   * @param string $address
   * @param string $lat
   * @param string $lng
   * @param string $departNumber
   * @param string $houseNumber
   */
  public function __construct(string $descr,
                              string $code,
                              string $citySCOATOU,
                              string $address,
                              string $lat,
                              string $lng,
                              string $departNumber,
                              string $houseNumber) {
    $this->descr = $descr;
    $this->code = $code;
    $this->citySCOATOU = $citySCOATOU;
    $this->address = $address;
    $this->lat = $lat;
    $this->lng = $lng;
    $this->departNumber = $departNumber;
    $this->houseNumber = $houseNumber;

  }

  /**
   * @return string
   */
  public function getCode(): string {
    return $this->code;
  }

  /**
   * @return string
   */
  public function getCitySCOATOU(): string {
    return $this->citySCOATOU;
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      "descr" => $this->descr,
      "code" => $this->code,
      "citySCOATOU" => $this->citySCOATOU,
      "address" => $this->address,
      "lat" => $this->lat,
      "lng" => $this->lng,
      "departNumber" => $this->departNumber,
      "houseNumber" => $this->houseNumber
    ];
  }

  /**
   * @param array $attributes
   * @return BaseEntity
   */
  public static function createFromArray(array $attributes): BaseEntity {
    return new self(
      (string)Arr::get($attributes, 'descr', ''),
      (string)Arr::get($attributes, 'code', ''),
      (string)Arr::get($attributes, 'citySCOATOU', ''),
      (string)Arr::get($attributes, 'address', ''),
      (string)Arr::get($attributes, 'lat', ''),
      (string)Arr::get($attributes, 'lng', ''),
      (string)Arr::get($attributes, 'departNumber', ''),
      (string)Arr::get($attributes, 'houseNumber', '')
    );
  }
}
