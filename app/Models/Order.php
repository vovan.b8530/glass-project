<?php

namespace App\Models;

use App\Collection\OrderProductCollection;
use App\Models\Helpers\CriteriaActions;
use App\Models\Mixins\OrderMixin;
use App\Models\Scopes\OrderScopes;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Class Order
 * @package App\Models
 *
 * @property integer $id
 * @property string $uuid
 * @property string $cookie_hash
 * @property integer $status
 * @property integer $payment_type
 * @property integer $name
 * @property integer $phone_number
 * @property integer $email
 * @property integer $delivery_type
 * @property integer $novaposhta_city_id
 * @property integer $novaposhta_warehouse_id
 * @property integer $justin_city_id
 * @property integer $justin_warehouse_id
 * @property float $total_amount
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property OrderProduct[]|OrderProductCollection $orderProducts
 * @property Product[]|Collection $products
 * @property NovaPoshtaInternetDocument $novaPoshtaInternetDocument
 * @property NovaPoshtaCity $novaPoshtaCity
 * @property NovaPoshtaWarehouse $novaPoshtaWarehouse
 * @property JustinCity $justinCity
 * @property JustinWarehouse $justinWarehouse
 */
class Order extends Model {
  use SoftDeletes, CriteriaActions, OrderScopes, OrderMixin;

  /**
   * @var string
   */
  protected $table = 'orders';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'uuid',
    'cookie_hash',
    'status',
    'payment_type',
    'name',
    'phone_number',
    'email',
    'delivery_type',
    'novaposhta_city_id',
    'novaposhta_warehouse_id',
    'justin_city_id',
    'justin_warehouse_id',
    'total_amount',
  ];

  /**
   * The attribute provides a convenient method of converting attributes to common data types
   *
   * @var array
   */
  protected $casts = [
    'uuid' => 'string',
    'cookie_hash' => 'string',
    'status' => 'integer',
    'payment_type' => 'integer',
    'name' => 'string',
    'phone_number' => 'string',
    'email' => 'string',
    'delivery_type' => 'integer',
    'novaposhta_city_id' => 'integer',
    'novaposhta_warehouse_id' => 'integer',
    'justin_city_id' => 'integer',
    'justin_warehouse_id' => 'integer',
    'total_amount' => 'float',
  ];

  /**
   * Get order products for the order
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function orderProducts() {
    return $this->hasMany(OrderProduct::class, 'order_id', 'id');
  }

  /**
   * Get products for the order
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function products() {
    return $this->belongsToMany(
      Product::class,
      (new OrderProduct())->getTable(),
      'order_id',
      'product_id'
    )
      ->withTimestamps();
  }

  /**
   * Get a novaposhta city for the order
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function novaPoshtaCity() {
    return $this->belongsTo(NovaPoshtaCity::class, 'novaposhta_city_id', 'id');
  }

  /**
   * Get a novaposhta warehouse for the order
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function novaPoshtaWarehouse() {
    return $this->belongsTo(NovaPoshtaWarehouse::class, 'novaposhta_warehouse_id', 'id');
  }

  /**
   * Get a justin city for the order
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function justinCity() {
    return $this->belongsTo(JustinCity::class, 'justin_city_id', 'id');
  }

  /**
   * Get a justin warehouse for the order
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function justinWarehouse() {
    return $this->belongsTo(JustinWarehouse::class, 'justin_warehouse_id', 'id');
  }

  /**
   * @param $query
   * @param $value
   */
  protected function getFullTextSearchColumn($query, $value) {
    //
  }
}
