<?php

namespace App\Models;

use App\Collection\ProductPictureCollection;
use App\Enums\DirectoriesStorage;
use App\Models\Helpers\AssetFileAttribute;
use App\Models\Helpers\CriteriaActions;
use App\Models\Helpers\ReorderRecord;
use App\Models\Mixins\ProductPictureMixin;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class ProductPicture
 * @package App\Models
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $is_main
 * @property string $picture_file_name
 * @property string $picture_thumb_file_name
 * @property integer $display_order
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Product $product
 */
class ProductPicture extends Model {
  use CriteriaActions, AssetFileAttribute, ReorderRecord, ProductPictureMixin;

  /**
   * @var string
   */
  protected $table = 'product_pictures';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'product_id',
    'is_main',
    'picture_file_name',
    'picture_thumb_file_name',
    'display_order',
  ];

  /**
   * The attribute provides a convenient method of converting attributes to common data types
   *
   * @var array
   */
  protected $casts = [
    'product_id' => 'integer',
    'is_main' => 'bool',
    'picture_file_name' => 'string',
    'picture_thumb_file_name' => 'string',
    'display_order' => 'integer',
  ];

  /**
   * @param array $models
   * @return ProductPictureCollection|\Illuminate\Database\Eloquent\Collection
   */
  public function newCollection(array $models = []) {
    return new ProductPictureCollection($models);
  }

  /**
   * @return string
   */
  public function directoryStorage(): string {
    return DirectoriesStorage::PRODUCT_PICTURE_PATH;
  }

  /**
   * @param string $fileName
   * @return string
   */
  public function getFilePath(string $fileName) {
    return $this->directoryStorage() . '/' . $fileName;
  }

  /**
   * Get a product for the picture
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function product() {
    return $this->belongsTo(Product::class, 'product_id', 'id');
  }

  /**
   * @param Builder $query
   * @param string $value
   * @return bool
   */
  protected function getFullTextSearchColumn($query, $value) {
    return 'file_name';
  }
}
