<?php

namespace App\Models;

use App\Models\Helpers\CriteriaActions;
use App\Models\Mixins\SeoMixin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Seo
 * @package App\Models
 *
 * @property integer $id
 * @property string $url
 * @property string $title
 * @property string $description
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Seo extends Model {
  use CriteriaActions, SeoMixin;

  /**
   * @var string
   */
  protected $table = 'seo';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'url',
    'title',
    'description',
  ];

  /**
   * The attribute provides a convenient method of converting attributes to common data types
   *
   * @var array
   */
  protected $casts = [
    'url' => 'string',
    'title' => 'string',
    'description' => 'string',
  ];

}
