<?php

namespace App\Models;

use App\Models\Helpers\CriteriaActions;
use App\Models\Mixins\Justin\JustinWarehouseMixin;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class NovaPoshtaWarehouse
 * @package App\Models
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $descr
 * @property string $code
 * @property string $citySCOATOU
 * @property string $address
 * @property string $lat
 * @property string $lng
 * @property string $departNumber
 * @property string $houseNumber
 * @property string $description
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property Order[]|Collection $orders
 * @property JustinCity $justinCity
 */
class JustinWarehouse extends Model {
  use CriteriaActions, JustinWarehouseMixin;

  /**
   * @var string
   */
  protected $table = 'justin_warehouses';

  /**
   * The attributes that are mass assignable.
   *
   * @var array $fillable
   */
  protected $fillable = [
    'city_id',
    'descr',
    'code',
    'citySCOATOU',
    'address',
    'lat',
    'lng',
    'departNumber',
    'houseNumber',
  ];

  /**
   * @var array
   */
  protected $casts = [
    'city_id' => 'int',
    'descr' => 'string',
    'code' => 'string',
    'citySCOATOU' => 'string',
    'address' => 'string',
    'lat' => 'string',
    'lng' => 'string',
    'departNumber' => 'string',
    'houseNumber' => 'string',
  ];

  /**
   * Get orders for the justin warehouse
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function orders() {
    return $this->hasMany(Order::class, 'justin_warehouse_id', 'id');
  }

  /**
   * Get a justin city for the justin warehouse
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function justinCity() {
    return $this->belongsTo(JustinCity::class, 'city_id', 'id');
  }
}
