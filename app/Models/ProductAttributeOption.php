<?php

namespace App\Models;

use App\Models\Helpers\CriteriaActions;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class ProductAttributeOption
 * @package App\Models
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $attribute_option_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Product $product
 * @property AttributeOption $attributeOption
 */
class ProductAttributeOption extends Model {
  use CriteriaActions;

  /**
   * @var string
   */
  protected $table = 'product_attribute_option';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'product_id',
    'attribute_option_id',
  ];

  /**
   * The attribute provides a convenient method of converting attributes to common data types
   *
   * @var array
   */
  protected $casts = [
    'product_id' => 'integer',
    'attribute_option_id' => 'integer',
  ];

  /**
   * Get product for the product attribute option
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function product() {
    return $this->belongsTo(Product::class, 'product_id', 'id');
  }

  /**
   * Get attribute option for the product attribute option
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function attributeOption() {
    return $this->belongsTo(AttributeOption::class, 'attribute_option_id', 'id');
  }

  /**
   * @param Builder $query
   * @param string $value
   * @return string
   */
  protected function getFullTextSearchColumn($query, $value) {
    //
  }
}
