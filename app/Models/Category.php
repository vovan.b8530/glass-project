<?php

namespace App\Models;

use App\Models\Helpers\CriteriaActions;
use App\Models\Helpers\ReorderRecord;
use App\Models\Mixins\CategoryMixin;
use App\Models\Scopes\CategoryScopes;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property int $display_order
 * @property string $seo_title
 * @property string $seo_description
 *
 * @property Product[]|Collection $product
 * @property Category[]|Collection $subCategory
 */
class Category extends Model {
  use CriteriaActions, CategoryScopes, CategoryMixin, ReorderRecord, Sluggable, SoftDeletes;

  /**
   * @var string
   */
  protected $table = 'categories';

  /**
   * @var array
   */
  protected $fillable = [
    'alias',
    'title',
    'display_order',
    'seo_title',
    'seo_description',
  ];

  /**
   * @var array
   */
  protected $casts = [
    'alias' => 'string',
    'title' => 'string',
    'display_order' => 'int',
    'seo_title' => 'string',
    'seo_description' => 'string',
  ];

  /**
   * Generate an alias
   *
   * @return array|\string[][]
   */
  public function sluggable(): array {
    return [
      'alias' => [
        'source' => 'title',
      ],
    ];
  }

  /**
   * Get products for the category.
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function products() {
    return $this->hasMany(Product::class, 'category_id', 'id');
  }

  /**
   * Get sub categories for the category.
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function subCategories() {
    return $this->hasMany(SubCategory::class, 'category_id', 'id');
  }
}
