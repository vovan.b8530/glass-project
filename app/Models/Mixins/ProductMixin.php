<?php

namespace App\Models\Mixins;

use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;

trait ProductMixin {
  /**
   * @param Builder $query
   * @param mixed $value
   * @return \Closure
   */
  protected function getFullTextSearchColumn($query, $value) {
    return function ($query, $value) {
      /**
       * @var Builder $query
       * @var mixed $value
       */
      return $query->where(function ($query) use ($value) {
        /**
         * @var Builder $query
         * @var mixed $value
         */
        $query
          ->where('title', 'like', "%{$value}%")
          ->orWhere('description', 'like', "%{$value}%");
      });
    };
  }

  /**
   * @return bool
   */
  public function isNew(): bool {
    return !$this->exists;
  }

  /**
   * @return string|null
   */
  public function getBackgroundImagePath(): ?string {
    /* @var Product|self $this */
    return $this->mainPicture
      ? $this->mainPicture->getOriginalImagePath()
      : null;
  }
}