<?php

namespace App\Models\Mixins;

trait ProductPictureMixin {
  /**
   * @return bool
   */
  public function hasThumbImage(): bool {
    return !empty($this->picture_thumb_file_name);
  }

  /**
   * @return string
   */
  public function getOriginalImagePath(): string {
    return $this->assetAbsolute($this->picture_file_name);
  }

  /**
   * @return string|null
   */
  public function getThumbImagePath(): ?string {
    return $this->hasThumbImage()
      ? $this->assetAbsolute($this->picture_thumb_file_name)
      : null;
  }
}