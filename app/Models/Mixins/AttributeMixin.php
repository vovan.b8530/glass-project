<?php

namespace App\Models\Mixins;

use Illuminate\Database\Eloquent\Builder;

trait AttributeMixin {
  /**
   * @param Builder $query
   * @param string $value
   * @return string
   */
  protected function getFullTextSearchColumn($query, $value) {
    return 'alias';
  }

  /**
   * @return bool
   */
  public function isNew(): bool {
    return !$this->exists;
  }

  /**
   * @return bool
   */
  public function isColor(): bool {
    return $this->is_color;
  }
}