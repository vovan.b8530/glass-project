<?php

namespace App\Models\Mixins;

use App\Models\AttributeOption;
use Illuminate\Database\Eloquent\Builder;

trait AttributeOptionMixin {
  /**
   * @param Builder $query
   * @param mixed $value
   * @return \Closure
   */
  protected function getFullTextSearchColumn($query, $value) {
    return function ($query, $value) {
      /**
       * @var Builder $query
       * @var mixed $value
       */
      return $query->where(function ($query) use ($value) {
        /**
         * @var Builder $query
         * @var mixed $value
         */
        $query
          ->where('name', 'like', "%{$value}%")
          ->orWhere('description', 'like', "%{$value}%");
      });
    };
  }

  /**
   * @return bool
   */
  public function isNew(): bool {
    return !$this->exists;
  }

  /**
   * @return string|null
   */
  public function getImagePath(): ?string {
    /* @var AttributeOption|self $this */
    return $this->assetAbsolute($this->picture_file_name) ?: null;
  }

  /**
   * @return bool
   */
  public function isColorAttributeOption(): bool {
    return $this->attribute && $this->attribute->isColor();
  }
}