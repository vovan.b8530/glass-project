<?php

namespace App\Models\Mixins;

use Illuminate\Database\Eloquent\Builder;

trait SubCategoryMixin {
  /**
   * @param Builder $query
   * @param string $value
   * @return string
   */
  protected function getFullTextSearchColumn($query, $value) {
    return 'name';
  }

  /**
   * @return bool
   */
  public function isNew(): bool {
    return !$this->exists;
  }
}