<?php

namespace App\Models\Mixins;

use App\Enums\OrderStatuses;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait OrderMixin {
  /**
   * @param Builder $query
   * @param mixed $value
   * @return \Closure
   */
  protected function getFullTextSearchColumn($query, $value) {
    return function ($query, $value) {
      /**
       * @var Builder $query
       * @var mixed $value
       */
      return $query->where(function ($query) use ($value) {
        /**
         * @var Builder $query
         * @var mixed $value
         */
        $query
          ->where('delivery_general_info', 'like', "%{$value}%");
      });
    };
  }

  /**
   * @param string $hash
   * @return OrderMixin|Order
   */
  public static function findOrReturnNewByCookieHash(string $hash): self {
    /* @var Order $order */
    $order = static::whereCookieHash($hash)->first();
    return $order ?: new Order(['cookie_hash' => $hash]);
  }

  /**
   * Checks for exists items in order.
   */
  public function checkHasProducts(): void {
    /* @var Order|self $this */
    if ($this->orderProducts->isEmpty())
      throw new \LogicException('Cart is empty!');
  }

  /**
   * @param int $status
   * @return $this
   */
  public function changeStatus(int $status): self {
    $this->status = $status;
    return $this;
  }

  /**
   * @return $this
   */
  public function toNewStatus(): self {
    return $this->changeStatus(OrderStatuses::NEW);
  }

  /**
   * @return $this
   */
  public function toAcceptStatus(): self {
    return $this->changeStatus(OrderStatuses::ACCEPT);
  }

  /**
   * @return $this
   */
  public function toConfirmStatus(): self {
    return $this->changeStatus(OrderStatuses::CONFIRMED);
  }

  /**
   * @return $this
   */
  public function toCancelStatus(): self {
    return $this->changeStatus(OrderStatuses::CANCELED);
  }

  /**
   * @return $this
   */
  public function clearCookieHash(): self {
    $this->cookie_hash = null;
    return $this;
  }

  /**
   * @param string|null $param
   * @return Carbon
   */
  public function getFormattedDateAttribute(?string $param = null) {
    if ($param)
      return Carbon::parse($this->$param);
    return Carbon::parse($this->created_at);
  }
}