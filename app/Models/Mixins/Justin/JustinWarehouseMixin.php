<?php

namespace App\Models\Mixins\Justin;

use Illuminate\Database\Eloquent\Builder;

trait JustinWarehouseMixin {
  /**
   * @param Builder $query
   * @param mixed $value
   * @return \Closure
   */
  protected function getFullTextSearchColumn($query, $value) {
    return function ($query, $value) {
      /**
       * @var Builder $query
       * @var mixed $value
       */
      return $query->where(function ($query) use ($value) {
        /**
         * @var Builder $query
         * @var mixed $value
         */
        $query
          ->where('description', 'like', "%{$value}%");
      });
    };
  }
}