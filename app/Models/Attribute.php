<?php

namespace App\Models;

use App\Models\Helpers\CriteriaActions;
use App\Models\Helpers\ReorderRecord;
use App\Models\Mixins\AttributeMixin;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Collection;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Class Attribute
 * @package App\Models
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property integer $display_order
 * @property boolean $is_color
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property AttributeOption[]|Collection $attributeOptions
 */
class Attribute extends Model {
  use SoftDeletes, CriteriaActions, ReorderRecord, Sluggable, AttributeMixin;

  const IS_COLOR = 1;

  /**
   * @var string
   */
  protected $table = 'attributes';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'alias',
    'title',
    'display_order',
    'is_color',
  ];

  /**
   * The attribute provides a convenient method of converting attributes to common data types
   *
   * @var array
   */
  protected $casts = [
    'alias' => 'string',
    'title' => 'string',
    'display_order' => 'integer',
    'is_color' => 'boolean',
  ];

  /**
   * Generate an alias
   *
   * @return array|\string[][]
   */
  public function sluggable(): array {
    return [
      'alias' => [
        'source' => 'title',
      ],
    ];
  }

  /**
   * Get product attributes for the attribute
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function attributeOptions() {
    return $this->hasMany(AttributeOption::class, 'attribute_id', 'id');
  }
}