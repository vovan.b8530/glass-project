<?php

namespace App\Models;

use App\Enums\DirectoriesStorage;
use App\Models\Helpers\AssetFileAttribute;
use App\Models\Helpers\CriteriaActions;
use App\Models\Helpers\ReorderRecord;
use App\Models\Mixins\AttributeOptionMixin;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use \Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Class Attribute
 * @package App\Models
 *
 * @property integer $id
 * @property string $alias
 * @property integer $attribute_id
 * @property string $title
 * @property string $picture_file_name
 * @property integer $display_order
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Attribute $attribute
 * @property ProductAttributeOption[]|Collection $productAttributeOptions
 * @property Product[]|Collection $products
 */
class AttributeOption extends Model {
  use SoftDeletes, CriteriaActions, ReorderRecord, Sluggable, AssetFileAttribute, AttributeOptionMixin;

  /**
   * @var string
   */
  protected $table = 'attribute_options';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'alias',
    'attribute_id',
    'title',
    'picture_file_name',
    'display_order',
  ];

  /**
   * The attribute provides a convenient method of converting attributes to common data types
   *
   * @var array
   */
  protected $casts = [
    'alias' => 'string',
    'attribute_id' => 'integer',
    'title_' => 'string',
    'picture_file_name' => 'string',
    'display_order' => 'integer',
  ];

  /**
   * @return string
   */
  public function directoryStorage(): string {
    return DirectoriesStorage::ATTRIBUTE_OPTION_PICTURE_PATH;
  }

  /**
   * Generate an alias
   *
   * @return array|\string[][]
   */
  public function sluggable(): array {
    return [
      'alias' => [
        'source' => 'title',
      ],
    ];
  }

  /**
   * Get an attribute for the attribute option
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function attribute() {
    return $this->belongsTo(Attribute::class, 'attribute_id', 'id');
  }

  /**
   * Get product attribute options for the attribute option
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function productAttributeOptions() {
    return $this->hasMany(ProductAttributeOption::class, 'attribute_option_id', 'id');
  }

  /**
   * Get products for the attribute option
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function products() {
    return $this->belongsToMany(
      Product::class,
      (new ProductAttributeOption())->getTable(),
      'attribute_option_id',
      'product_id'
    )
      ->withTimestamps();
  }

  /**
   * @param Builder $query
   * @param string $value
   * @return string
   */
  protected function getFullTextSearchColumn($query, $value) {
    return 'alias';
  }

}