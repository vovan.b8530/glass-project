<?php

namespace App\Models;

use App\Models\Helpers\CriteriaActions;
use App\Models\Helpers\ReorderRecord;
use App\Models\Mixins\SubCategoryMixin;
use App\Models\Scopes\SubCategoryScopes;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SubCategory
 * @package App\Models
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property int $category_id
 * @property string $attribute_option_url
 * @property int $display_order
 * @property string $seo_title
 * @property string $seo_description
 *
 * @property Category[]|Collection $category
 */
class SubCategory extends Model {
  use CriteriaActions, SubCategoryScopes, SubCategoryMixin, ReorderRecord, Sluggable, SoftDeletes;

  /**
   * @var string
   */
  protected $table = 'sub_categories';

  /**
   * @var array
   */
  protected $fillable = [
    'alias',
    'title',
    'category_id',
    'attribute_option_url',
    'display_order',
    'seo_title',
    'seo_description',
  ];

  /**
   * @var array
   */
  protected $casts = [
    'alias' => 'string',
    'title' => 'string',
    'category_id',
    'attribute_option_url',
    'display_order' => 'int',
    'seo_title' => 'string',
    'seo_description' => 'string',
  ];

  /**
   * Generate an alias
   *
   * @return array|\string[][]
   */
  public function sluggable(): array {
    return [
      'alias' => [
        'source' => 'title',
      ],
    ];
  }

  /**
   * Get category for the subCategory.
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function category() {
    return $this->belongsTo(Category::class, 'category_id', 'id');
  }
}
