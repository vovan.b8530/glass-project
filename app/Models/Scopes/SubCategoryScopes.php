<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

/**
 * Trait SubCategoryScopes
 * @package App\Models\Scopes
 *
 * @method Builder|self orderByIdAsc()
 */
trait SubCategoryScopes {

  /**
   * @param self|Builder $query the eloquent builder.
   *
   * @return self|Builder|SubCategoryScopes
   */
  public function scopeOrderByIdAsc($query) {
    return $query->orderBy($query->withAlias('id'), 'asc');
  }
}