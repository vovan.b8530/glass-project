<?php

namespace App\Models\Scopes;

use App\Enums\OrderStatuses;
use Illuminate\Database\Eloquent\Builder;

/**
 * Trait OrderScopes
 * @package App\Models\Scopes
 *
 * @method static Builder|self whereCookieHash(string $hash)
 * @method static Builder|self onlyNew()
 * @method static Builder|self onlyAccept()
 * @method static Builder|self onlyConfirmed()
 * @method static Builder|self onlyCanceled()
 */
trait OrderScopes {
  /**
   * @param self|Builder $query the eloquent builder.
   * @param string $hash
   *
   * @return self|Builder
   */
  public function scopeWhereCookieHash($query, string $hash) {
    return $query->where($query->withAlias('cookie_hash'), $hash);
  }

  /**
   * @param self|Builder $query the eloquent builder.
   * @return self|Builder
   */
  public function scopeOnlyNew($query) {
    return $query->where($query->withAlias('status'), OrderStatuses::NEW);
  }

  /**
   * @param self|Builder $query the eloquent builder.
   * @return self|Builder
   */
  public function scopeOnlyAccept($query) {
    return $query->where($query->withAlias('status'), OrderStatuses::ACCEPT);
  }

  /**
   * @param self|Builder $query the eloquent builder.
   * @return self|Builder
   */
  public function scopeOnlyConfirmed($query) {
    return $query->where($query->withAlias('status'), OrderStatuses::CONFIRMED);
  }

  /**
   * @param self|Builder $query the eloquent builder.
   * @return self|Builder
   */
  public function scopeOnlyCanceled($query) {
    return $query->where($query->withAlias('status'), OrderStatuses::CANCELED);
  }
}