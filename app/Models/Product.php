<?php

namespace App\Models;

use App\Collection\ProductPictureCollection;
use App\Enums\DirectoriesStorage;
use App\Models\Helpers\AssetFileAttribute;
use App\Models\Helpers\CriteriaActions;
use App\Models\Helpers\ReorderRecord;
use App\Models\Mixins\ProductMixin;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Class Product
 * @package App\Models
 *
 * @property int $id
 * @property string $alias
 * @property string $title
 * @property string $description
 * @property string $product_hash
 * @property float $price
 * @property integer $stock_quantity
 * @property int $is_main_page
 * @property integer $category_id
 * @property integer $display_order
 * @property string $seo_title
 * @property string $seo_description
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Category $category
 * @property ProductPicture[]|ProductPictureCollection $pictures
 * @property AttributeOption[]|Collection $attributeOptions
 * @property ProductPicture $backgroundPicture
 * @property OrderProduct[]|Collection $orderProducts
 * @property Product[]|Collection $productGroup
 * @property Order[]|Collection $orders
 * @property ProductPicture $mainPicture
 */
class Product extends Model {
  use CriteriaActions, ProductMixin, SoftDeletes, Sluggable, ReorderRecord, AssetFileAttribute;

  /**
   * @var string
   */
  protected $table = 'products';

  /**
   * The attributes that are mass assignable.
   *
   * @var array $fillable
   */
  protected $fillable = [
    'alias',
    'title',
    'description',
    'product_hash',
    'price',
    'stock_quantity',
    'is_main_page',
    'category_id',
    'display_order',
    'seo_title',
    'seo_description'
  ];

  /**
   * @var array
   */
  protected $casts = [
    'alias' => 'string',
    'title' => 'string',
    'description' => 'string',
    'product_hash' => 'string',
    'price' => 'float',
    'stock_quantity' => 'int',
    'is_main_page' => 'int',
    'category_id' => 'int',
    'display_order' => 'int',
    'seo_title' => 'string',
    'seo_description' => 'string'
  ];

  /**
   * Generate an alias
   *
   * @return array|\string[][]
   */
  public function sluggable(): array {
    return [
      'alias' => [
        'source' => 'title',
      ],
    ];
  }

  /**
   * @return string
   */
  public function directoryStorage(): string {
    return DirectoriesStorage::PRODUCT_PICTURE_PATH;
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function category() {
    return $this->belongsTo(Category::class, 'category_id', 'id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function pictures() {
    return $this
      ->hasMany(ProductPicture::class, 'product_id', 'id')
      ->orderBy('display_order');
  }

  /**
   * Get main picture for the project.
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasOne
   */
  public function mainPicture() {
    return $this
      ->hasOne(ProductPicture::class, 'product_id', 'id')
      ->where('is_main', '=', 1);
  }

  /**
   * Get attribute options for the product
   *
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function attributeOptions() {
    return $this->belongsToMany(
      AttributeOption::class,
      (new ProductAttributeOption())->getTable(),
      'product_id',
      'attribute_option_id'
    )
      ->withTimestamps();
  }

  /**
   * @return mixed
   */
  public function colorAttributeOption() {
    return $this->attributeOptions
      ->loadMissing('attribute')
      ->filter(function ($value) {
        /* @var AttributeOption $value */
        return $value->isColorAttributeOption();
      })
      ->first();
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function productGroup() {
    return $this->hasMany(self::class, 'product_hash', 'product_hash');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function orderProducts() {
    return $this->hasMany(OrderProduct::class, 'product_id', 'id');
  }
}
