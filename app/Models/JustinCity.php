<?php

namespace App\Models;

use App\Models\Helpers\CriteriaActions;
use App\Models\Mixins\Justin\JustinCityMixin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;

/**
 * Class JustinCity
 * @package App\Models
 *
 * @property int $id
 * @property string $uuid
 * @property string $code
 * @property string $descr
 * @property string $SCOATOU
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property Order[]|Collection $orders
 * @property JustinWarehouse[]|Collection $justinWarehouses
 */
class JustinCity extends Model {
  use CriteriaActions, JustinCityMixin;

  /**
   * @var string
   */
  protected $table = 'justin_cities';

  /**
   * The attributes that are mass assignable.
   *
   * @var array $fillable
   */
  protected $fillable = [
    'uuid',
    'code',
    'descr',
    'SCOATOU',
  ];

  /**
   * @var array
   */
  protected $casts = [
    'uuid' => 'string',
    'code' => 'string',
    'descr' => 'string',
    'SCOATOU' => 'string',
  ];

  /**
   * Get orders for the novaposhta city
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function orders() {
    return $this->hasMany(Order::class, 'justin_city_id', 'id');
  }

  /**
   * Get novaposhta warehouse for the novaposhta city
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function justinWarehouses() {
    return $this->hasMany(JustinWarehouse::class, 'city_id', 'id');
  }
}
