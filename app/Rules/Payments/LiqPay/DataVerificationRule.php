<?php

namespace App\Rules\Payments\LiqPay;

use App\Support\LiqPay\Helpers\LiqPayDataParser;
use Illuminate\Contracts\Validation\Rule;

class DataVerificationRule implements Rule {
  use LiqPayDataParser;

  /**
   * Create a new rule instance.
   *
   * @return void
   */
  public function __construct() {
    //
  }

  /**
   * Determine if the validation rule passes.
   *
   * @param string $attribute
   * @param string $value
   *
   * @return bool
   */
  public function passes($attribute, $value) {
    try {
      $data = $this->parseData($value);
    } catch (\Exception $exception) {
      return false;
    }

    return $this->hasOrder($data) && $this->isTransactionSuccess($data);
  }

  /**
   * Get the validation error message.
   *
   * @return string
   */
  public function message() {
    return 'Data is not valid.';
  }
}
