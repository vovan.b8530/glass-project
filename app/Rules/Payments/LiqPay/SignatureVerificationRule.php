<?php

namespace App\Rules\Payments\LiqPay;

use App\Support\LiqPay\LiqPay;
use Illuminate\Contracts\Validation\Rule;

class SignatureVerificationRule implements Rule {
  /**
   * @var string
   */
  protected $data;

  /**
   * Create a new rule instance.
   *
   * @param string $data
   * @return void
   */
  public function __construct(string $data) {
    $this->data = $data;
  }

  /**
   * Determine if the validation rule passes.
   *
   * @param string $attribute
   * @param mixed $value
   * @return bool
   */
  public function passes($attribute, $value) {
    /* @var LiqPay $liqPay */
    $liqPay = app(LiqPay::class);

    return $liqPay->buildSignature($this->data) === $value;
  }

  /**
   * Get the validation error message.
   *
   * @return string
   */
  public function message() {
    return 'Signature is not verification.';
  }
}
