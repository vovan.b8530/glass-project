<?php

namespace App\Collection;

use App\Models\ProductPicture;
use Illuminate\Database\Eloquent\Collection;

class ProductPictureCollection extends Collection {
  /**
   * @return ProductPicture|null
   */
  public function firstMainPicture(): ?ProductPicture {
    return $this->where('is_main')->first();
  }
}