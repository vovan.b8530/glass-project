<?php

namespace App\Collection;

use App\Collection\Helpers\CommonHelper;
use App\Models\OrderProduct;
use Illuminate\Database\Eloquent\Collection;

class OrderProductCollection extends Collection {
  use CommonHelper;

  /**
   * Calculate total amount of list products in order with using price.
   *
   * @return float
   */
  public function priceTotalAmount(): float {
    return $this->totalAmount(function ($orderProduct) {
      /* @var OrderProduct $orderProduct */

      return $orderProduct->price;
    });
  }


  /**
   * Calculate total amount of list products in order.
   *
   * @param \Closure $getPrice
   * @return float
   */
  public function totalAmount(\Closure $getPrice): float {
    $this->ensureItemsConsisting(OrderProduct::class);
    return $this->sum(function ($orderProduct) use ($getPrice) {
      /* @var OrderProduct $orderProduct */
      return $orderProduct->calculateAmount($getPrice($orderProduct));
    });
  }
}