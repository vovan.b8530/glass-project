<?php

namespace App\Support\LiqPay;

class LiqPay extends \LiqPay {
  /**
   * @var string
   */
  protected $__publicKey;

  /**
   * @var string
   */
  protected $__privateKey;

  /**
   * LiqPay constructor.
   * @param $public_key
   * @param $private_key
   * @param null $api_url
   */
  public function __construct($public_key, $private_key, $api_url = null) {
    parent::__construct($public_key, $private_key, $api_url);

    $this->__publicKey = $public_key;
    $this->__privateKey = $private_key;
  }

  /**
   * @param string $data
   * @return string
   */
  public function buildSignature(string $data): string {
    return base64_encode(sha1($this->__privateKey . $data . $this->__privateKey, 1));
  }
}