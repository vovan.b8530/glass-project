<?php

namespace App\Support\LiqPay\Helpers;

use App\Criteria\Order\WhereUuid;
use App\Enums\Payments\LiqPay\TransactionSuccessStatuses;
use App\Models\Order;
use Illuminate\Support\Arr;

trait LiqPayDataParser {
  /**
   * @var Order|null
   */
  protected $order;

  /**
   * @param string $data
   * @return array
   */
  protected function parseData(string $data): array {
    return json_decode(base64_decode($data), true);
  }

  /**
   * @param array $data
   * @return string|null
   */
  public function retrieveOrderId(array $data): ?string {
    return Arr::get($data, 'order_id') ?: null;
  }

  /**
   * @param array $data
   * @return string|null
   */
  public function retrieveStatus(array $data): ?string {
    return Arr::get($data, 'status') ?: null;
  }

  /**
   * @param array $data
   * @return bool
   */
  public function hasOrder(array $data): bool {
    return !!$this->retrieveOrder($data);
  }

  /**
   * @param array $data
   * @return bool
   */
  public function isTransactionSuccess(array $data): bool {
    $status = $this->retrieveStatus($data);
    return in_array($status, TransactionSuccessStatuses::getValues());
  }

  /**
   * @param array $data
   * @return Order|null
   */
  public function retrieveOrder(array $data): ?Order {
    $orderId = $this->retrieveOrderId($data);
    if ($orderId && !$this->order)
      $this->order = Order::findOne(new CriteriaCollection([new WhereUuid($orderId)]));

    return $this->order;
  }
}