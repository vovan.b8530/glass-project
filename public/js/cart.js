/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/cart.js":
/*!******************************!*\
  !*** ./resources/js/cart.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _mixins_common_helper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mixins/common-helper */ "./resources/js/mixins/common-helper.js");
/* harmony import */ var _mixins_form_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mixins/form-helper */ "./resources/js/mixins/form-helper.js");
/* harmony import */ var _mixins_form_input_events_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mixins/form-input-events-helper */ "./resources/js/mixins/form-input-events-helper.js");




function init() {}

function order() {}

$('.btn-order').on('click', order);
$(document).ready(function () {
  init();
});
var cart = new Vue({
  el: '#bascketPage',
  mixins: [_mixins_common_helper__WEBPACK_IMPORTED_MODULE_0__["default"], _mixins_form_helper__WEBPACK_IMPORTED_MODULE_1__["default"], _mixins_form_input_events_helper__WEBPACK_IMPORTED_MODULE_2__["default"]],
  data: function data() {
    return {
      sourceAxiosRequest: null,
      editingProcess: false,
      deletingProcess: false,
      checkoutProcess: false,
      confirmItemsError: null,
      checkoutErrors: null,
      order: null,
      confirmData: null,
      PAYMENT_TYPES: window._PAYMENT_TYPES,
      DELIVERY_TYPES: window._DELIVERY_TYPES
    };
  },
  created: function created() {
    var _this = this;

    this.init();
    $(document).ready(function () {
      var deliveryNPRoute = '/api/delivery/novaposhta';
      var deliveryJustinRoute = '/api/delivery/justin';
      var $npCity = $('#npCity');
      var $npWarehouse = $('#npWarehouse');
      var $justinCity = $('#justinCity');
      var $justinWarehouse = $('#justinWarehouse');

      _this.viaSelect2($npCity, deliveryNPRoute + '/cities');

      _this.viaSelect2($npWarehouse, deliveryNPRoute + '/warehouses', function () {
        return {
          cityId: _this.confirmData.novaposhta_city_id
        };
      });

      _this.viaSelect2($justinCity, deliveryJustinRoute + '/cities');

      _this.viaSelect2($justinWarehouse, deliveryJustinRoute + '/warehouses', function () {
        return {
          cityId: _this.confirmData.justin_city_id
        };
      });

      $npCity.on('change', function (e) {
        _this.confirmData.novaposhta_city_id = +e.target.value;
      });
      $npWarehouse.on('change', function (e) {
        _this.confirmData.novaposhta_warehouse_id = +e.target.value;
      });
      $justinCity.on('change', function (e) {
        _this.confirmData.justin_city_id = +e.target.value;
      });
      $justinWarehouse.on('change', function (e) {
        _this.confirmData.justin_warehouse_id = +e.target.value;
      });
    });
  },
  mounted: function mounted() {
    $('[data-server-render="true"]').remove();
  },
  computed: {
    totalCount: function totalCount() {
      return _.sumBy(_.get(this.order, 'orderProducts', []), function (item) {
        return item.quantity;
      });
    },
    totalAmount: function totalAmount() {
      var _this2 = this;

      var sum = _.sumBy(_.get(this.order, 'orderProducts', []), function (item) {
        return _this2.calculateAmount(item.price, item.quantity);
      });

      return _.round(sum, 2);
    }
  },
  methods: {
    init: function init() {
      this.order = window._ORDER;
      this.confirmData = _.merge(this.newConfirmData(), _.clone(this.order));
    },
    viaSelect2: function viaSelect2($jElement, url, callableParams) {
      $jElement.select2({
        ajax: {
          url: url,
          data: function data(params) {
            var addParams = callableParams ? callableParams() : {};
            return _.merge({
              q: params.term,
              max: 100,
              page: params.page || 1
            }, addParams);
          },
          processResults: function processResults(data, params) {
            var currentPage = params.page || 1;

            var more = currentPage < _.get(data, 'meta.last_page', 0);

            return {
              results: _.get(data, 'data', []),
              pagination: {
                more: more
              }
            };
          }
        }
      });
    },
    possibleCountNumbers: function possibleCountNumbers(orderProduct) {
      var limit = _.get(orderProduct, 'product.stock_quantity', 0);

      limit = limit >= 5 ? 5 : limit;
      var counters = [];

      for (var i = 1; i <= limit; i++) {
        counters.push(i);
      }

      return counters;
    },
    newConfirmData: function newConfirmData() {
      return {
        payment_type: window._PAYMENT_TYPES.CASH_PAYMENT,
        delivery_type: window._DELIVERY_TYPES.NOVA_POSHTA,
        name: '',
        phone_number: '',
        email: '',
        novaposhta_city_id: null,
        novaposhta_warehouse_id: null,
        justin_city_id: null,
        justin_warehouse_id: null
      };
    },
    callbackCalcAmount: function callbackCalcAmount(attribute) {
      return function (item) {
        return _.get(item, attribute);
      };
    },
    calculateAmount: function calculateAmount(price, quantity) {
      return _.floor(price * quantity, 2);
    },
    recalculateAmounts: function recalculateAmounts(item, quantity) {
      item.amount = this.calculateAmount(_.get(item, 'price', 0), quantity);
    },
    sendRequestSavingItem: function sendRequestSavingItem(order, index) {
      if (this.sourceAxiosRequest) this.sourceAxiosRequest.cancel();
      this.editItem(order, index);
    },
    editItem: function editItem(item) {
      var _this3 = this;

      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      this.sourceAxiosRequest = axios.CancelToken.source();
      this.editingProcess = true;
      this.confirmItemsError = null;
      axios.put('cart/edit-item/' + _.get(item, 'id'), {
        quantity: _.get(item, 'quantity', 1)
      }).then(function (response) {
        _this3.sourceAxiosRequest = null;
      })["catch"](function (error) {
        return _.forEach(_.get(error, ['response', 'data'], []), function (error, key) {
          _this3.confirmItemsError = _this3.confirmItemsError || {};
          _this3.confirmItemsError['orders.' + index + '.' + key] = error;
        });
      }).then(function () {
        return _this3.editingProcess = false;
      });
    },
    deleteItem: function deleteItem(item) {
      var _this4 = this;

      this.deletingProcess = true;
      axios["delete"]('cart/delete-item/' + _.get(item, 'id')).then(function (response) {
        _this4.order.orderProducts.splice(_this4.order.orderProducts.indexOf(item), 1);
      })["catch"](function (error) {
        return _this4.deletingProcess = false;
      }).then(function () {
        return _this4.deletingProcess = false;
      });
    },
    checkout: function checkout() {
      var _this5 = this;

      var quickly = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      if (this.checkoutProcess) return false;
      this.checkoutProcess = true;
      this.checkoutErrors = null;
      axios.post('checkout', _.merge(this.confirmData, {
        _quickly: quickly
      })).then(function (response) {
        var data = _.get(response, ['data']);

        switch (data.status) {
          case 'decorated':
            {
              window.location.replace('success');
            }
            break;

          case 'redirect':
            {
              var redirectUri = _.get(data, 'redirect_uri');

              if (redirectUri) window.location.replace(redirectUri);
            }
            break;
        }

        throw new Error('Failed.');
      })["catch"](function (error) {
        return _this5.checkoutErrors = _.get(error, ['response', 'data'], []);
      }).then(function () {
        return _this5.checkoutProcess = false;
      });
    }
  }
});

/***/ }),

/***/ "./resources/js/mixins/common-helper.js":
/*!**********************************************!*\
  !*** ./resources/js/mixins/common-helper.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    equalsArrays: function equalsArrays(array1, array2) {
      // (_.isArray(val) && _.isArray(oldVal) ? !val.equals(oldVal) : val != oldVal)
      // if the other array is a falsy value, return
      if (!_.isArray(array1) && !_.isArray(array2)) return false; // compare lengths - can save a lot of time

      if (array1.length != array2.length) return false;

      for (var i = 0, l = array1.length; i < l; i++) {
        // Check if we have nested arrays
        if (array1[i] instanceof Array && array2[i] instanceof Array) {
          // recurse into the nested arrays
          if (!this.equalsArrays(array1[i], array2[i])) return false;
        } else if (array1[i] != array2[i]) {
          // Warning - two different object instances will never be equal: {x:20} != {x:20}
          return false;
        }
      }

      return true;
    },
    clone: function clone(obj) {
      var copy; // Handle the 3 simple types, and null or undefined

      if (null == obj || "object" != _typeof(obj)) return obj; // Handle Date

      if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
      } // Handle Array


      if (obj instanceof Array) {
        copy = [];

        for (var i = 0, len = obj.length; i < len; i++) {
          copy[i] = this.clone(obj[i]);
        }

        return copy;
      } // Handle Object


      if (obj instanceof Object) {
        copy = {};

        for (var attr in obj) {
          if (obj.hasOwnProperty(attr)) copy[attr] = this.clone(obj[attr]);
        }

        return copy;
      }

      throw new Error("Unable to copy obj! Its type isn't supported.");
    },
    delay: function delay() {
      var timer = 0;
      return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
      };
    },
    omitEmpty: function omitEmpty(object) {
      var params = {};

      for (var property in object) {
        var value = _.get(object, property);

        if (!(value === '' || _.isArray(value) && value.length === 0 || value === null || _.isString(value) && value.trim() === '')) {
          _.set(params, property, value);
        }
      }

      return params;
    },
    requiredAllProperties: function requiredAllProperties(object) {
      return _.keys(this.omitEmpty(object)).length === _.keys(object).length;
    },
    toFormatDate: function toFormatDate(date) {
      var originalFormat = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'DD/MM/YYYY';
      var makeFormat = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'YYYY-MM-DD';
      if (!_.isEmpty(date)) return moment(date, originalFormat).format(makeFormat);
      return date;
    },
    pluck: function pluck(array, key) {
      return array.map(function (o) {
        return o[key];
      });
    },
    generateHash: function generateHash() {
      var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 10;
      var hash = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < length; i++) {
        hash += possible.charAt(Math.floor(Math.random() * possible.length));
      }

      return hash;
    },
    toUrl: function toUrl(obj) {
      var str = [];

      for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
      }

      return str.join("&");
    },
    numberRoundOff: function numberRoundOff(number) {
      var precision = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
      return _.floor(number, precision);
    },
    changeWindowHistory: function changeWindowHistory(path) {
      var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
      var title = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
      var parameters = this.globalWindowHistoryState();
      var hasNotQuestionMark = !/\/.*?\?{1,}/g.test(path);
      var hasNotAmpersand = !/\&$/g.test(path);
      path = path + (parameters ? hasNotQuestionMark ? '?' : hasNotAmpersand ? '&' : '' : '') + parameters;
      window.history.replaceState(data, title, path);
    },
    globalWindowHistoryState: function globalWindowHistoryState() {
      return !_.isEmpty(window._FRAME_GET_PARAMETERS) ? this.toUrl(window._FRAME_GET_PARAMETERS) : '';
    },
    hasAny: function hasAny(object, params) {
      for (var i = 0; i < params.length; i++) {
        if (_.has(object, params[i])) return true;
      }

      return false;
    },
    readURL: function readURL(input, cb) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          cb(e, e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
      }
    },
    isMobile: function isMobile() {
      if (window.innerWidth <= 800 && window.innerHeight <= 600) {
        return true;
      } else {
        return false;
      }
    }
  }
});

/***/ }),

/***/ "./resources/js/mixins/form-helper.js":
/*!********************************************!*\
  !*** ./resources/js/mixins/form-helper.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    getErrorMessage: function getErrorMessage(errors, key) {
      var messages = _.get(errors, key);

      return _.isArray(messages) ? messages.join('. ') : messages;
    },
    hasError: function hasError(errors, key) {
      return _.hasIn(errors, key);
    }
  }
});

/***/ }),

/***/ "./resources/js/mixins/form-input-events-helper.js":
/*!*********************************************************!*\
  !*** ./resources/js/mixins/form-input-events-helper.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    isNumber: function isNumber(event) {
      event = event ? event : window.event;
      var charCode = event.which ? event.which : event.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        event.preventDefault();
      } else {
        return true;
      }
    },
    isEmptyResetDefaultValue: function isEmptyResetDefaultValue(event, defaultValue) {
      if (_.isEmpty(event.target.value)) {
        event.target.value = defaultValue;
      }

      return true;
    }
  }
});

/***/ }),

/***/ 6:
/*!************************************!*\
  !*** multi ./resources/js/cart.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/mac/www/glass.back/resources/js/cart.js */"./resources/js/cart.js");


/***/ })

/******/ });