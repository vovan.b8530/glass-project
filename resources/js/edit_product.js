import CommonHelper from './mixins/common-helper'
import FormHelper from './mixins/form-helper'

console.log(1)

const editProduct = new Vue({
  el: '#editProduct',
  mixins: [
    CommonHelper,
    FormHelper,
  ],

  data() {
    return {
      attrs: [],
      selectedAttrs: []
    };
  },

  created() {
    this.init();

    $(document).ready(() => {
      $(".chosen-select").chosen({
        no_results_text: "Oops, nothing found!"
      });
    });
  },

  methods: {
    init() {
      this.attrs = window._ATTRS;

      this.selectedAttrs = _.map(
        window._SELECTED_ATTRS,
        (options, attrId) => this.selectedAttrModel(+attrId, this.pluck(options, 'id'))
      );
    },

    selectedAttrModel(attrId = null, attrOptions = []) {
      return {
        attrId: attrId,
        attrOptions: attrOptions
      };
    },

    addNewUserAttr() {
      return this.selectedAttrs.push(this.selectedAttrModel());
    },

    filterAttrOptions(attributeId) {
      let attribute = _.find(this.attrs, (attribute) => +_.get(attribute, 'id') === +attributeId);

      return _.get(attribute, 'attribute_options', []);
    },

    onChangeAttribute(selectedAttr) {
      selectedAttr.attrOptions = [];
      return true;
    }
 }
});