import CommonHelper from './mixins/common-helper'
import FormHelper from './mixins/form-helper'
import FormInputEventsHelper from './mixins/form-input-events-helper'

function init() {}
function order() {}

$('.btn-order').on('click', order);

$(document).ready(function() {
  init();
});

const cart = new Vue({
  el: '#bascketPage',
  mixins: [
    CommonHelper,
    FormHelper,
    FormInputEventsHelper
  ],

  data() {
    return {
      sourceAxiosRequest: null,

      editingProcess: false,
      deletingProcess: false,
      checkoutProcess: false,

      confirmItemsError: null,
      checkoutErrors: null,

      order: null,
      confirmData: null,

      PAYMENT_TYPES: window._PAYMENT_TYPES,
      DELIVERY_TYPES: window._DELIVERY_TYPES
    };
  },

  created() {
    this.init();

    $(document).ready(() => {
      let deliveryNPRoute = '/api/delivery/novaposhta';
      let deliveryJustinRoute = '/api/delivery/justin';

      let $npCity = $('#npCity');
      let $npWarehouse = $('#npWarehouse');

      let $justinCity = $('#justinCity');
      let $justinWarehouse = $('#justinWarehouse');

      this.viaSelect2($npCity, deliveryNPRoute + '/cities');
      this.viaSelect2($npWarehouse, deliveryNPRoute + '/warehouses', () => {
        return {
          cityId: this.confirmData.novaposhta_city_id
        };
      });

      this.viaSelect2($justinCity, deliveryJustinRoute + '/cities');
      this.viaSelect2($justinWarehouse, deliveryJustinRoute + '/warehouses', () => {
        return {
          cityId: this.confirmData.justin_city_id
        };
      });

      $npCity.on('change', (e) => {
        this.confirmData.novaposhta_city_id = +e.target.value;
      });

      $npWarehouse.on('change', (e) => {
        this.confirmData.novaposhta_warehouse_id = +e.target.value;
      });

      $justinCity.on('change', (e) => {
        this.confirmData.justin_city_id = +e.target.value;
      });

      $justinWarehouse.on('change', (e) => {
        this.confirmData.justin_warehouse_id = +e.target.value;
      });
    });
  },

  mounted() {
    $('[data-server-render="true"]').remove();

  },

  computed: {

    totalCount() {
      return _.sumBy(_.get(this.order, 'orderProducts', []), (item) => item.quantity);
    },

    totalAmount() {
      let sum = _.sumBy(
        _.get(this.order, 'orderProducts', []),
        (item) => this.calculateAmount(item.price, item.quantity)
      );
      return _.round(sum, 2)
    },

  },

  methods: {
    init() {
      this.order = window._ORDER;
      this.confirmData = _.merge(this.newConfirmData(), _.clone(this.order));
    },

    viaSelect2($jElement, url, callableParams) {
      $jElement.select2({
        ajax: {
          url: url,
          data: function (params) {
            let addParams = callableParams ? callableParams() : {};

            return _.merge({
              q: params.term,
              max: 100,
              page: params.page || 1,
            }, addParams);
          },

          processResults: function (data, params) {
            let currentPage = params.page || 1;

            let more = currentPage < _.get(data, 'meta.last_page', 0);

            return {
              results: _.get(data, 'data', []),
              pagination: {
                more: more
              }
            };
          }
        }
      });
    },

    possibleCountNumbers(orderProduct) {
      let limit = _.get(orderProduct, 'product.stock_quantity', 0);

      limit = limit >= 5 ? 5 : limit;

      let counters = [];
      for (let i = 1; i <= limit; i++) {

        counters.push(i);
      }

      return counters;
    },

    newConfirmData() {
      return {
        payment_type: window._PAYMENT_TYPES.CASH_PAYMENT,
        delivery_type: window._DELIVERY_TYPES.NOVA_POSHTA,
        name: '',
        phone_number: '',
        email: '',
        novaposhta_city_id: null,
        novaposhta_warehouse_id: null,
        justin_city_id: null,
        justin_warehouse_id: null
      };
    },

    callbackCalcAmount(attribute) {
      return (item) => {
        return _.get(item, attribute);
      };
    },

    calculateAmount(price, quantity) {
      return _.floor(price * quantity, 2);
    },

    recalculateAmounts(item, quantity) {
      item.amount = this.calculateAmount(_.get(item, 'price', 0), quantity);
    },

    sendRequestSavingItem(order, index) {
      if (this.sourceAxiosRequest)
        this.sourceAxiosRequest.cancel();

      this.editItem(order, index);
    },

    editItem(item, index = 0) {
      this.sourceAxiosRequest = axios.CancelToken.source();

      this.editingProcess = true;
      this.confirmItemsError = null;

      axios
        .put('cart/edit-item/' + _.get(item, 'id'), {
          quantity: _.get(item, 'quantity', 1)
        })
        .then((response) => {
          this.sourceAxiosRequest = null;
        })
        .catch((error) => _.forEach(
          _.get(error, ['response', 'data'], []),
          (error, key) => {
            this.confirmItemsError = this.confirmItemsError || {};
            this.confirmItemsError['orders.' + index + '.' + key] = error;
          }
        ))
        .then(() => this.editingProcess = false);
    },

    deleteItem(item) {
      this.deletingProcess = true;
      axios
        .delete('cart/delete-item/' + _.get(item, 'id'))
        .then((response) => {
          this.order.orderProducts.splice(this.order.orderProducts.indexOf(item), 1);
        })
        .catch((error) => this.deletingProcess = false)
        .then(() => this.deletingProcess = false);
    },

    checkout(quickly = false) {
      if (this.checkoutProcess)
        return false;

      this.checkoutProcess = true;
      this.checkoutErrors = null;

        axios
          .post('checkout', _.merge(this.confirmData, {_quickly: quickly}))
          .then((response) => {
            let data = _.get(response, ['data']);

            switch(data.status) {
              case 'decorated': {
                window.location.replace('success');
              }
              break;

              case 'redirect': {
                let redirectUri = _.get(data, 'redirect_uri');
                if(redirectUri)
                  window.location.replace(redirectUri);
              }
              break;
            }

            throw new Error('Failed.');
          })
          .catch((error) => this.checkoutErrors = _.get(error, ['response', 'data'], []))
          .then(() => this.checkoutProcess = false);
    }
  }
});