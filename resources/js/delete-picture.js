$(document).ready(function () {
  // Predelete picture
  $('.admin-image-delete i').on("click", function () {
    let inp = $(`form input[data-id=${$(this).attr('id')}]`);
    if (inp.length >= 1) return;
    let input = document.createElement('INPUT');
    $(this).parent().parent().next().attr('style', 'opacity: 0.4;');
    $(this).attr('style', 'opacity: 0.4;');
    $(input).attr('data-id', $(this).attr('id'));
    input.name = "deletePicture";
    input.type = 'hidden';
    input.value = 1;
    $('form').append(input);
  });

  // Restore picture
  $('.admin-image-restore i').on("click", function () {

    $(this).parent().parent().next().attr('style', 'opacity: 1;');
    $('.admin-image-delete i').attr('style', 'opacity: 1;');
    $(`form input[data-id=${$(this).attr('data-id')}]`).remove();
  })
});