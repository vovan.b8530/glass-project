<?php
/* @var \App\Services\Payment\Entity\LiqPay\TransactionData $transactionData */
?>

<!DOCTYPE html>
<html>
  <head>
    <title>LiqPay</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, shrink-to-fit=no">
  </head>

  <body>
    <form id="transaction" method="POST" action="{{ $transactionData->getUrl() }}" accept-charset="utf-8">
      <input type="hidden" name="data" value="{{ $transactionData->getData() }}" />
      <input type="hidden" name="signature" value="{{ $transactionData->getSignature() }}" />
      <input type="submit" style="display: none" />
    </form>

    <script>
      document.getElementById("transaction").submit();
    </script>
  </body>

</html>
