<?php
/* @var array $errors */
/* @var \App\Models\Category $category */
?>

@extends('admin.layouts.app')

@section('content')
  <div class="container">
    <br>
    <div class="row">
      <div class="col-12">
        <a href="{{ route('categories.index') }}" class="btn btn-sm btn-secondary">Назад</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <form method="POST"
              @if($category->exists)
              action="{{ route('categories.update', ['category' => $category]) }}"
              @else
              action="{{ route('categories.store') }}"
              @endif
              enctype='multipart/form-data'>
          @csrf
          @if($category->exists)
            @method('PUT')
          @endif
          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'title', 'label' => 'Категория'])
            <input type="text" required class="form-control" name="title" id="inputTitle"
                   value="{{ old('title') ? old('title') : $category->title }}">
          @endcomponent

          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'seo_title', 'label' => 'seo_title'])
            <input type="text" class="form-control" name="seo_title" id="inputTitle"
                   value="{{ old('seo_title') ? old('seo_title') : $category->seo_title }}">
          @endcomponent

          @component('admin.includes.textarea_input', [
              'name' => 'seo_description',
              'label' => 'Seo description',
              'placeholder' => 'Введите seo description',
              'object' => !$category->isNew() ? $category : null,
              'rows' => 5,
              'width' => 'col-sm-12'
          ])@endcomponent
          <br>
          <div class="form-group row">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-sm btn-success">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

