<?php
/* @var \App\Models\Category[] $categories */
?>

@extends('admin.layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Категории</h2>
      </div>
    </div>
    <div class="buttons">
      <div class="button-group form-group">
        <a href="{{ route('categories.create') }}">
          <button type="button" class="btn btn-sm btn-success"><i class="fa fa-pencil" aria-hidden="true"></i> Создать
          </button>
        </a>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-12">
        <table id="sortable" class="table table-striped">
          <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">alias</th>
            <th scope="col"></th>
            <th scope="col"></th>
          </tr>
          </thead>
          <tbody>

          @foreach($categories as $i => $category)
            <tr data-index="{{ $i }}" data-id="{{ $category->id }}" data-entity="{{ $category->getTable() }}">
              <th scope="row">{{ $loop->index + 1 }}</th>
              <td>
                {{ $category->title }}</td>
              <td>{{ $category->alias }}</td>
              <td>
                <a href="{{ route('categories.edit', ['category' => $category])}}"
                   data-toggle="tooltip" title="Редактировать">
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </a>
              </td>
              <td>
                <form action="{{route('categories.destroy',['category' => $category])}}" method="POST">
                  @method('DELETE')
                  @csrf
                  <button data-toggle="tooltip" onclick="return confirm('Вы действительно хотите удалить?')" class="fa fa-trash" aria-hidden="true"></button>
                </form>
              </td>
            </tr>
          @endforeach

          </tbody>
        </table>

      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/sortable.js') }}"></script>
@endsection
