<?php
/* @var App\Models\Seo $seo */
?>
@extends('admin.layouts.app')

@section('content')
  <h2 class="content-heading">{{ !$seo->isNew() ? $seo->title : 'Добавление Seo данных' }}</h2>
  <div class="row">
    <div class="col-sm-12">
      <div class="block">
        <form method="post" id="form-projects"
              @if(!$seo->isNew())
              action="{{ route('seo.update', ['seo' => $seo]) }}"
              @else
              action="{{ route('seo.store') }}"
              @endif
              enctype="multipart/form-data">
          @csrf

          @if(!$seo->isNew())
            @method('PUT')
          @endif

          <div class="block-content">
            <div class="row">

              <div class="col-sm-8 block-content-seo-l-padding">

                <div class="tab-content" id="nav-tabContent">
                  <div class="tab-pane fade show active" id="ru-tab" role="tabpanel" aria-labelledby="ru-tab-link">
                    @component('admin.includes.text_input', [
                        'name' => 'title',
                        'label' => 'Seo title',
                        'placeholder' => 'Введите seo title',
                        'object' => !$seo->isNew() ? $seo : null,
                        'width' => 'col-sm-12',
                        'attributes' => 'required'
                    ])@endcomponent
                    @component('admin.includes.textarea_input', [
                        'name' => 'description',
                        'label' => 'Seo description',
                        'placeholder' => 'Введите description',
                        'object' => !$seo->isNew() ? $seo : null,
                        'rows' => 5,
                        'width' => 'col-sm-12',
                        'attributes' => 'required'
                    ])@endcomponent
                  </div>
                </div>

                @component('admin.includes.text_input', [
                    'name' => 'url',
                    'label' => 'Ссылка для перенаправления',
                    'placeholder' => 'Введите ссылку',
                    'object' => !$seo->isNew() ? $seo : null,
                    'width' => 'col-sm-12',
                    'attributes' => 'required'
                ])@endcomponent
              </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-success">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>


@endsection

@section('scripts')

  <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/deleteRestorePicture.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/sortablePictures.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/preloadPicture.js') }}"></script>
  <script>
      $(document).ready(function () {

          window.onkeydown = function (e) {
              if (e.keyCode === 13) {
                  e.preventDefault()
              }
          }

          let $formProj = $('#form-projects'),
              $formProjBtn = $formProj.find('button[type=submit]');

          $formProjBtn.on('click', function(e) {
              setTimeout(() => {
                  window.flash('Необходимо указать "Название проекта" и "Характеристики" для всех версий языков', 'danger');
              }, 500);
          });

      });

  </script>

@endsection