<?php
/* @var \App\Models\Order[] $orders */
?>

@extends('admin.layouts.app')

@section('content')
  <div id="adminPage" class="adminPage page">

    <div class="mcontainer">
      <div class="table-block">
        <table class="table">
          <thead>
          <tr class="header-tab">
            <th class="min-width" scope="col">№</th>
            <th class="min-width" scope="col">Номер заказа</th>
            <th scope="col">Заказ</th>
            <th class="min-width" scope="col">Доставка</th>
            <th scope="col">Оплата</th>
            <th class="min-width3" scope="col">Клиент</th>
            <th class="min-width2" scope="col">Сумма</th>
            <th class="min-width" scope="col">Дата создвния</th>
            <th class="min-width" scope="col">Дата изменения</th>
            <th class="min-width" scope="col-">Статус</th>
          </tr>
          </thead>
          <tbody>
          @foreach($orders as $order)
            <tr>
              <th scope="row">{{ resourceGet($order, 'id') }}</th>
              <th scope="row">{{ resourceGet($order, 'uuid') }}</th>

              <td>
                @foreach(resourceGet($order, 'orderProducts', []) as $orderProduct)
                  {{ resourceGet($orderProduct, 'product.title') }}<br>
                  <b>({{ resourceGet($orderProduct, 'quantity') }} шт.)</b>
                @endforeach
              </td>

              <td>
                {{ resourceGet($order, 'delivery_name') }}<br>
                @if(resourceGet($order, 'delivery_type') == 1)
                  {{ resourceGet($order, 'novaposhtaCity.text') }}<br>
                  {{ resourceGet($order, 'novaposhtaWarehouse.text') }}<br>
                @elseif(resourceGet($order, 'delivery_type') == 2)
                  {{ resourceGet($order, 'justinCity.text') }}<br>
                  {{ resourceGet($order, 'justinWarehouse.text') }}<br>
                @endif
              </td>

              <td>
                {{ resourceGet($order, 'payment_name') }}
              </td>

              <td>
                {{ resourceGet($order, 'name') }}<br>
                {{ resourceGet($order, 'phone_number') }}<br>
                {{ resourceGet($order, 'email') }}<br>
              </td>

              <td>{{ resourceGet($order, 'total_amount') }}</td>

              <td>{{ resourceGet($order, 'created_at') }}</td>
              <td>{{ resourceGet($order, 'updated_at') }}</td>

              <td>
                <select data-id="{{ resourceGet($order, 'id') }}" class="form-control change-status" data-url="">
                  @foreach(\App\Enums\OrderStatuses::$LABELS as $key => $label)
                      <option value="{{$label}}" @if(resourceGet($order, 'status_name') == $label)  selected @endif @if($label == 'new' ) disabled @endif>{{\App\Enums\OrderStatuses::$LABELS_RU[$key]}}</option>
                  @endforeach
                </select>
              </td>

            </tr>
          @endforeach
          </tbody>
        </table>

        {{ $paginator->links() }}
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    $(document).ready(() => {

      $(".change-status").on("change", function (e) {

        let orderId = +$(this).attr('data-id');
        let statusName = $(this).val();

        axios.post(`/admin/orders/${orderId}/${statusName}`, {
        }).then((response) => {
          // console.log(response);
          window.alert(response.data.message);
        });
      })
    });
  </script>
@endsection
