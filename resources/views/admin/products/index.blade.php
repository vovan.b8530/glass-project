<?php
/* @var \Illuminate\Pagination\LengthAwarePaginator $paginator */
/* @var \App\Models\Product[] $products */
?>

@extends('admin.layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Продукты</h2>
      </div>
    </div>
    <div class="buttons">
      <div class="button-group form-group">
        <a href="{{ route('products.create') }}">
          <button type="button" class="btn btn-sm btn-success"><i class="fa fa-pencil" aria-hidden="true"></i> Создать
          </button>
        </a>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-12">
        <table id="sortable" class="table table-striped">
          <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Изображение</th>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
            <th scope="col">Количество</th>
            <th scope="col">На главной</th>
            <th scope="col">Категория</th>
            <th scope="col"></th>
            <th scope="col"></th>
          </tr>
          </thead>
          <tbody>

          @foreach($products as $i => $product)
            <tr data-index="{{ $i }}" data-id="{{ resourceGet($product, 'id')}}"
                data-entity="{{ resourceGet($product, 'getTable')}}">
              <th scope="row">{{ resourceGet($product, 'display_order')}}</th>
              <td>
                <img src="{{ resourceFilledOutGet($product, 'backgroundPicture', '/img/not_photo.jpg') }}"
                     class="card-img-top" alt="...">
              </td>
              <td>{{ resourceGet($product, 'title') }}</td>
              <td>{{ resourceGet($product, 'price') }}</td>
              <td>{{ resourceGet($product, 'stock_quantity') }}</td>
              <td>
                @if(resourceGet($product, 'is_main_page') == 1)
                  На главной
                @endif
              </td>
              <td>{{ resourceGet($product, 'category.title') }}</td>

              <td>
                <a href="{{resourceGet($product, 'routeEdit')}}"
                   data-toggle="tooltip" title="Редактировать">
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </a>
              </td>
              <td>
                <form action="{{resourceGet($product, 'routeDelete')}}" method="POST">
                  @method('DELETE')
                  @csrf
                  <button data-toggle="tooltip" onclick="return confirm('Вы действительно хотите удалить?')" class="fa fa-trash" aria-hidden="true"></button>
                </form>
              </td>

            </tr>
          @endforeach

          {{ $paginator->links() }}

          </tbody>
        </table>

      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/sortable.js') }}"></script>
@endsection
