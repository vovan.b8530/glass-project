<?php
/* @var array $errors */
/* @var \App\Models\Product $product */
/* @var \App\Models\Category[] $categories */
/* @var \App\Models\Product[] $products */
/* @var \App\Models\ProductPicture $picture */
/* @var \App\Models\Attribute[] $attributes */
/* @var \App\Models\AttributeOption[] $productAttributeOptions */
?>

@extends('admin.layouts.app')

@section('styles')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" integrity="sha512-yVvxUQV0QESBt1SyZbNJMAwyKvFTLMyXSyBHDO4BG5t7k/Lw34tyqlSDlKIrIENIzCl+RVUNjmCPG+V/GMesRw==" crossorigin="anonymous" />
@endsection

@section('content')

  <div class="container" id="editProduct">
    <br>
    <div class="row">
      <div class="col-12">
        <a href="{{ route('products.index') }}" class="btn btn-sm btn-secondary">Назад</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <form method="post" id="form-product"
              @if(!$product->isNew())
              action="{{ route('products.update', ['product' => $product]) }}"
              @else
              action="{{ route('products.store') }}"
              @endif
              enctype='multipart/form-data'>
          @csrf

          @if(!$product->isNew())
            @method('PUT')
          @endif
          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'title', 'label' => 'Продукт'])
            <input type="text" required class="form-control" name="title" id="inputTitle"
                   value="{{ old('title') ? old('title') : $product->title }}">
          @endcomponent

          @component('admin.includes.textarea_input', [
              'name' => 'description',
              'label' => 'Описание',
              'placeholder' => 'Введите описание',
              'object' => !$product->isNew() ? $product : null,
              'rows' => 5,
              'width' => 'col-sm-12'
          ])@endcomponent

          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'productIds', 'label' => 'Продукты'])
            <select data-placeholder="Choose a products..." class="form-control chosen-select" id="productIds[]" name="productIds[]" multiple>
              <option value="productIds">Выберите продукты</option>
              @forelse($products as $productItem)
                <option value="{{ $productItem->id }}"
                        @if($productItem->product_hash == $product->product_hash )
                        selected
                  @endif
                >{{ $productItem->title }}</option>
              @empty
              @endforelse
            </select>
          @endcomponent

          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'price', 'label' => 'Цена'])
            <input type="text" class="form-control" name="price" id="inputTitle"
                   value="{{ old('price') ? old('price') : $product->price }}">
          @endcomponent

          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'stock_quantity', 'label' => 'Количество '])
            <input type="text" class="form-control" name="stock_quantity" id="inputTitle"
                   value="{{ old('stock_quantity') ? old('stock_quantity') : $product->stock_quantity }}">
          @endcomponent

          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'is_main_page', 'label' => 'На главную'])
            <select class="form-control" id="product_id" name="is_main_page">
              <option value="">Выберите на главную</option>
              @forelse($picturesMainPage as $indexType => $typeName)
                <option value="{{ $indexType }}"
                        @if($typeName == old('type'))
                        selected
                        @elseif(isset($product) && $indexType == $product->is_main_page)
                        selected
                  @endif
                >{{ $typeName }}</option>
                @empty
                @endforelse
                </option>
            </select>
          @endcomponent

          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'category_id', 'label' => 'Категория'])
            <select class="form-control" id="category_id" name="category_id" required>
              <option value="">Выберите категорию</option>
              @forelse($categories as $category)
                <option value="{{ $category->id }}"
                        @if($category->id == old('category_id'))
                        selected
                        @elseif(isset($product) && $category->id == $product->category_id)
                        selected
                  @endif
                >{{ $category->title }}</option>
              @empty
              @endforelse
            </select>
          @endcomponent

          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'seo_title', 'label' => 'seo_title'])
            <input type="text" class="form-control" name="seo_title" id="inputTitle"
                   value="{{ old('seo_title') ? old('seo_title') : $product->seo_title }}">
          @endcomponent

          @component('admin.includes.textarea_input', [
              'name' => 'seo_description',
              'label' => 'Seo description',
              'placeholder' => 'Введите seo description',
              'object' => !$product->isNew() ? $product : null,
              'rows' => 5,
              'width' => 'col-sm-12'
          ])@endcomponent

          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'alias', 'label' => 'alias'])
            <input type="text" class="form-control" name="alias" id="inputTitle"
                   value="{{ old('alias') ? old('alias') : $product->alias }}">
          @endcomponent

          <div class="form-group col-sm-4">
            <h6>Изображения для галереи <small></small></h6>
          </div>

          <div class="form-group col-sm-4">
            <div class="custom-file">
              <input type="file" class="custom-file-input js-custom-file-input-enabled" multiple id="product-images"
                     name="files[]" data-toggle="custom-file-input">
              <label class="custom-file-label" for="product-images">Выберите изображения</label>
            </div>
          </div>

          <div class="row ">
            <ul id="sortable" class="col-12 d-flex flex-wrap" data-entity="{{ $product->getTable() }}">
              @if(!$product->isNew() && $product->pictures)
                @forelse($product->pictures as $i => $picture)
                  <li class="ui-state-default"
                      data-index="{{ $i }}"
                      data-id="{{ $picture->id }}"
                      data-productId="{{ $product->id }}" @if($picture->is_main == 1) style="display:none;" @endif>
                    <div class="col-sm-12">
                      <div class="card card-body bg-light">
                        <div class="icons-flex" style="display: flex;">
                          <span class="admin-image-delete" style="margin-right: 10px;">
                              <i class="fa fa-times-circle-o delete-action-photo" aria-hidden="true"
                                 id="{{ $picture->id }}"></i>
                          </span>
                          <span class="admin-image-restore">
                              <i class="fa fa-window-restore restore-action-photo" aria-hidden="true"
                                 data-id="{{ $picture->id }}"></i>
                          </span>
                        </div>
                        <img src="{{ $picture->getOriginalImagePath() }}" alt=""
                             class="img-fluid">
                      </div>
                    </div>
                  </li>
                @empty
                @endforelse
              @endif
            </ul>
          </div>
          <div class="form-group col-sm-12">
            Главное изображение для продукта <small>( 255х170 px )</small>
          </div>

          <div class="form-group col-sm-4">
            <div class="custom-file">
              <input type="file" class="custom-file-input js-custom-file-input-enabled" id="main-image"
                     name="mainFile" data-toggle="custom-file-input">
              <label class="custom-file-label" for="product-small-image">Выберите изображение</label>
            </div>
          </div>
          <div class="form-group col-sm-8">
            <div id="showFile" class="options-container">
              @if(!$product->isNew() && ($backgroundImage = $product->getBackgroundImagePath()))
                <img width="300" height="200" src="{{ $backgroundImage }}"/>
              @endif
            </div>
          </div>
          <br>
          <div class="form-group col-sm-12">
            Характеристики товара
          </div>

          <div v-for="item in selectedAttrs">
            <select name="attrs[]"
                    class="form-control col-sm-6"
                    @change="onChangeAttribute(item)"
                    v-model="item.attrId">
              <option value="">Выберите атрибут</option>
              <option v-for="attr in attrs"
                      :value="attr.id"
                      v-text="attr.title"></option>
            </select>

            <select name="attrOptions[]"
                    class="form-control col-sm-6"
                    multiple
                    v-model="item.attrOptions">
              <option value="">Выберите опции</option>
              <option v-for="attrOption in filterAttrOptions(item.attrId)"
                      :value="attrOption.id"
                      v-text="attrOption.title"></option>
            </select>
          </div>

          <button class="add-specifications" type="button" @click="addNewUserAttr">Добавить характеристики</button>

          <div class="form-group row bottom-pd">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-sm btn-success">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/preload-picture.js') }}"></script>
  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/delete-pictures.js') }}"></script>
  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/sortable-pictures.js') }}"></script>
  <script src="{{ \EscapeWork\Assets\Facades\Asset::v('js/tinymce/jquery.tinymce.min.js') }}"></script>
  <script src="{{ \EscapeWork\Assets\Facades\Asset::v('js/tinymce/tinymce.min.js') }}"></script>
  <script src="{{ \EscapeWork\Assets\Facades\Asset::v('js/wysiwyg.js') }}"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js" integrity="sha512-rMGGF4wg1R73ehtnxXBt5mbUfN9JUJwbk21KMlnLZDJh7BkPmeovBuddZCENJddHYYMkCh9hPFnPmS9sspki8g==" crossorigin="anonymous"></script>

  <script type="text/javascript">
    var _ATTRS = @json($attributes);
    var _SELECTED_ATTRS = @json($productAttributeOptions);
  </script>

  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/edit_product.js') }}"></script>
@endsection
