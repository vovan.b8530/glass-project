<div id="mySidenav" class="sidenav">
  <ul>
    <li @if(Route::is('categories.index')) class="current" @endif>
      <a href="{{ route('categories.index') }}">
        <i class="fa fa-tasks" aria-hidden="true"></i>Категории
      </a>
    </li>
    <li @if(Route::is('sub-categories.index')) class="current" @endif>
      <a href="{{ route('sub-categories.index') }}">
        <i class="fa fa-tag" aria-hidden="true"></i>Подкатегории
      </a>
    <li @if(Route::is('attributes.index')) class="current" @endif>
      <a href="{{ route('attributes.index') }}">
        <i class="fa fa-paperclip" aria-hidden="true"></i>Атрибуты
      </a>
    </li>
    <li @if(Route::is('attribute-options.index')) class="current" @endif>
      <a href="{{ route('attribute-options.index') }}">
        <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>Атрибут опции
      </a>
    </li>
    <li @if(Route::is('products.index')) class="current" @endif>
      <a href="{{ route('products.index') }}">
        <i class="fa fa-shopping-bag" aria-hidden="true"></i>Продукты
      </a>
    </li>
    <li @if(Route::is('orders.index')) class="current" @endif>
      <a href="{{ route('orders.index') }}">
        <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Заказы
      </a>
    </li>
    <li @if(Route::is('seo.index')) class="current" @endif>
      <a href="{{ route('seo.index') }}">
        <i class="fa fa-globe" aria-hidden="true"></i>Seo раздел
      </a>
    </li>
    <li><a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a></li>
    <!-- Authentication Links -->
    <li class="nav-item dropdown">
      <a class="nav-link " href="{{ route('logout') }}"
         onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
        <i class="fa fa-sign-out" aria-hidden="true"></i>Выйти ({{ Auth::user()->name }})
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
      </form>
    </li>
  </ul>
  <ul>
    <li>
      <a href="{{ route('site.home') }}" target="_blank"><i class="fa fa-share" aria-hidden="true"></i>На сайт</a>
    </li>
  </ul>
</div>

<span onclick="openNav()" class="bars-btn">
    <i class="fa fa-bars" aria-hidden="true"></i>
</span>
