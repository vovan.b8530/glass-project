<div class="form-group row seo-textarea{{ $errors->has($name) ? ' is-invalid' : '' }}">

  <label for="{{ $name }}" class="col-sm-2 col-form-label">{!! $label !!}</label>
  <div class="col-sm-10">
          <textarea {{ $attributes ?? '' }} class="form-control" id="{{ $name }}" name="{{ $name }}" rows="{{ $rows }}"
                    placeholder="Введите описание"
          >{!! old($name) ?: (isset($object) ? $object->{$name} : '') !!}</textarea>
    @if ($errors->has($name))
      <div class="invalid-feedback">{{ $errors->first($name) }}</div>
    @endif
  </div>
</div>