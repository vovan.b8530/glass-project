<?php
/* @var array $errors */
/* @var \App\Models\Attribute $attribute */
?>

@extends('admin.layouts.app')

@section('content')

  <div class="container">
    <br>
    <div class="row">
      <div class="col-12">
        <a href="{{ route('attributes.index') }}" class="btn btn-sm btn-secondary">Назад</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <form method="POST"
              @if($attribute->exists)
              action="{{ route('attributes.update', ['attribute' => $attribute]) }}"
              @else
              action="{{ route('attributes.store') }}"
              @endif
              enctype='multipart/form-data'>
          @csrf
          @if($attribute->exists)
            @method('PUT')
          @endif
          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'title', 'label' => 'Атрибут'])
            <input type="text" required class="form-control" name="title" id="inputTitle"
                   value="{{ old('title') ? old('title') : $attribute->title }}">
          @endcomponent
          <br>
          <div class="form-group col-sm-12">
            <br>
            <label class="css-control css-control-sm css-control-primary css-switch">
              <input type="checkbox" name="is_color" value="1" class="css-control-input"
                     @if(old('is_color') == $attribute::IS_COLOR || (!$attribute->isNew() && $attribute->isColor())) checked @endif>
              <span class="css-control-indicator"></span> Цвет
            </label>
          </div>

          <div class="form-group row">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-sm btn-success">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

