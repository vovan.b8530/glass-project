<?php
/* @var \App\Models\AttributeOption[] $attributeOptions */
?>

@extends('admin.layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Атрибут опции</h2>
      </div>
    </div>
    <div class="buttons">
      <div class="button-group form-group">
        <a href="{{ route('attribute-options.create') }}">
          <button type="button" class="btn btn-sm btn-success"><i class="fa fa-pencil" aria-hidden="true"></i> Создать
          </button>
        </a>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-12">
        <table id="sortable" class="table table-striped">
          <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Изображение</th>
            <th scope="col">Название</th>
            <th scope="col">alias</th>
            <th scope="col">Родительский атрибут</th>
            <th scope="col"></th>
            <th scope="col"></th>
          </tr>
          </thead>
          <tbody>

          @foreach($attributeOptions as $i => $attributeOption)
            <tr data-index="{{ $i }}" data-id="{{ $attributeOption->id }}"
                data-entity="{{ $attributeOption->getTable() }}">
              <th scope="row">{{ $loop->index + 1 }}</th>
              <td>
                @isset($attributeOption->picture_file_name)
                  <img src="{{ $attributeOption->assetAbsolute($attributeOption->picture_file_name) }}" alt=""
                       class="img-fluid">
                @endisset
              </td>
              <td>{{ $attributeOption->title }}</td>
              <td>{{ $attributeOption->alias }}</td>
              <td>{{ isset($attributeOption->attribute) ? $attributeOption->attribute->title : '' }}</td>
              <td>
                <a href="{{ route('attribute-options.edit', ['attribute_option' => $attributeOption])}}"
                   data-toggle="tooltip" title="Редактировать">
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </a>
              </td>
              <td>
                <form action="{{route('attribute-options.destroy',['attribute_option' => $attributeOption])}}"
                      method="POST">
                  @method('DELETE')
                  @csrf
                  <button data-toggle="tooltip" onclick="return confirm('Вы действительно хотите удалить?')" class="fa fa-trash" aria-hidden="true"></button>
                </form>
              </td>
            </tr>
          @endforeach

          </tbody>
        </table>

      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/sortable.js') }}"></script>
@endsection
