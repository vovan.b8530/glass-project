<?php
/* @var array $errors */
/* @var \App\Models\AttributeOption $attributeOption */
/* @var \App\Models\Attribute[] $attributes */
?>

@extends('admin.layouts.app')

@section('content')

  <div class="container">
    <br>
    <div class="row">
      <div class="col-12">
        <a href="{{ route('attribute-options.index') }}" class="btn btn-sm btn-secondary">Назад</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <form method="POST"
              @if(!$attributeOption->isNew())
              action="{{ route('attribute-options.update', ['attribute_option' => $attributeOption]) }}"
              @else
              action="{{ route('attribute-options.store') }}"
              @endif
              enctype='multipart/form-data'>
          @csrf
          @if(!$attributeOption->isNew())
            @method('PUT')
          @endif
          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'title', 'label' => 'Атрибут опции'])
            <input type="text" required class="form-control" name="title" id="inputTitle"
                   value="{{ old('title') ? old('title') : $attributeOption->title }}">
          @endcomponent

          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'attribute_id', 'label' => 'Атрибут'])
            <select class="form-control" id="attribute_id" name="attribute_id" required>
              <option value="">Выберите атрибут</option>
              @forelse($attributes as $attribute)
                <option value="{{ $attribute->id }}"
                        @if($attribute->id == old('attribute_id'))
                        selected
{{--                        @elseif(isset($attributeOption) && $attribute->id == $attributeOption->attribute_id)--}}
                        @elseif(isset($attributeOption) && $attributeOption->attribute_id == $attribute->id)
                        selected
                  @endif
                >{{ $attribute->title }}</option>
              @empty
              @endforelse
            </select>
          @endcomponent

          @component('admin.includes.fileFormGroup', ['errors' => $errors, 'property' => 'file', 'label' => '60х22 (color 25х25)'])
            <input type="file" name="file" class="form-control-file"
                   id="main-image" @if(!isset($attributeOption->picture_file_name))  @endif/>
          @endcomponent

          <div class="col-sm-6 form-group row">
            <div class="col-sm-4">
              <div id="showFile" class="card card-body bg-light">
                @isset($attributeOption->picture_file_name)
                  <div class="icons-flex" style="display: flex;">
                    <span class="admin-image-delete" style="margin-right: 10px;">
                      <i class="fa fa-times-circle-o delete-action-photo"
                         aria-hidden="true" id="{{ $attributeOption->id }}"></i>
                    </span>
                    <span class="admin-image-restore">
                        <i class="fa fa-window-restore restore-action-photo"
                           aria-hidden="true" data-id="{{ $attributeOption->id }}"></i>
                    </span>
                  </div>
                  <img src="{{ $attributeOption->getImagePath() }}" alt=""
                       class="img-fluid">
                @endisset
              </div>
            </div>
          </div>
          <br>
          <div class="form-group row">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-sm btn-success">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/preload-picture.js') }}"></script>
  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/delete-picture.js') }}"></script>
@endsection


