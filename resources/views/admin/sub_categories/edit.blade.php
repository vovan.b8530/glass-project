<?php
/* @var array $errors */
/* @var \App\Models\Category[] $categories */
/* @var \App\Models\SubCategory $subCategory */
?>

@extends('admin.layouts.app')

@section('content')
  <div class="container">
    <br>
    <div class="row">
      <div class="col-12">
        <a href="{{ route('sub-categories.index') }}" class="btn btn-sm btn-secondary">Назад</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <form method="POST"
              @if($subCategory->exists)
              action="{{ route('sub-categories.update', ['sub_category' => $subCategory]) }}"
              @else
              action="{{ route('sub-categories.store') }}"
              @endif
              enctype='multipart/form-data'>
          @csrf
          @if($subCategory->exists)
            @method('PUT')
          @endif
          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'title', 'label' => 'Подкатегория'])
            <input type="text" required class="form-control" name="title" id="inputTitle"
                   value="{{ old('title') ? old('title') : $subCategory->title }}">
          @endcomponent

          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'category_id', 'label' => 'Категория'])
            <select class="form-control" id="category_id" name="category_id" required>
              <option value="">Выберите категорию</option>
              @forelse($categories as $category)
                <option value="{{ $category->id }}"
                        @if($category->id == old('category_id'))
                        selected
                        @elseif(isset($subCategory) && $category->id == $subCategory->category_id)
                        selected
                  @endif
                >{{ $category->title }}</option>
              @empty
              @endforelse
            </select>
          @endcomponent

          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'attribute_option_url', 'label' => 'Фильтр url'])
            <input type="text" required class="form-control" name="attribute_option_url" id="inputTitle"
                   value="{{ old('attribute_option_url') ? old('attribute_option_url') : $subCategory->attribute_option_url }}">
          @endcomponent

          @component('admin.includes.formGroup', ['errors' => $errors, 'property' => 'seo_title', 'label' => 'seo_title'])
            <input type="text" class="form-control" name="seo_title" id="inputTitle"
                   value="{{ old('seo_title') ? old('seo_title') : $subCategory->seo_title }}">
          @endcomponent


          @component('admin.includes.textarea_input', [
              'name' => 'seo_description',
              'label' => 'Seo description',
              'placeholder' => 'Введите seo description',
              'object' => !$subCategory->isNew() ? $subCategory : null,
              'rows' => 5,
              'width' => 'col-sm-12'
          ])@endcomponent
          <br>
          <div class="form-group row">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-sm btn-success">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

