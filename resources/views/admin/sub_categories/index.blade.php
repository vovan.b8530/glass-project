<?php
/* @var \App\Models\SubCategory[] $subCategories */
?>

@extends('admin.layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Подкатегории</h2>
      </div>
    </div>
    <div class="buttons">
      <div class="button-group form-group">
        <a href="{{ route('sub-categories.create') }}">
          <button type="button" class="btn btn-sm btn-success"><i class="fa fa-pencil" aria-hidden="true"></i> Создать
          </button>
        </a>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-12">
        <table id="sortable" class="table table-striped">
          <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">alias</th>
            <th scope="col">Родительская категория</th>
            <th scope="col">Фильтр url</th>
            <th scope="col"></th>
            <th scope="col"></th>
          </tr>
          </thead>
          <tbody>

          @foreach($subCategories as $i => $subCategory)
            <tr data-index="{{ $i }}" data-id="{{ $subCategory->id }}" data-entity="{{ $subCategory->getTable() }}">
              <th scope="row">{{ $loop->index + 1 }}</th>
              <td>{{ $subCategory->title }}</td>
              <td>{{ $subCategory->alias }}</td>
              <td>{{isset($subCategory->category) ? $subCategory->category->title : '' }}</td>
              <td>{{ $subCategory->attribute_option_url }}</td>
              <td>
                <a href="{{ route('sub-categories.edit', ['sub_category' => $subCategory])}}"
                   data-toggle="tooltip" title="Редактировать">
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </a>
              </td>
              <td>
                <form action="{{route('sub-categories.destroy',['sub_category' => $subCategory])}}" method="POST">
                  @method('DELETE')
                  @csrf
                  <button data-toggle="tooltip" onclick="return confirm('Вы действительно хотите удалить?')" class="fa fa-trash" aria-hidden="true"></button>
                </form>
              </td>
            </tr>
          @endforeach

          </tbody>
        </table>

      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/sortable.js') }}"></script>
@endsection
