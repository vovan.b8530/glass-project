@extends('layouts.app')

@section('content')
  <div id="bascketPage" class="basketPage page">
    <div class="basket-wrap">
      <div class="mcontainer">
        <div class="basket-contant basket-data-title">
          <div class="basket__title">Оформление заказа</div>
        </div>
        <div class="back-basket-data-title">
          <a href="{{route('cart.index')}}" class="basket__breadcramps">Вернутся в корзину</a>
        </div>
        <div class="shop-data-wraper flex">
          <div class="shop-data__minor">
            <div class="shop-data__minor-wrap">
              <div class="shop-data__minor-title">Ваш заказ</div>
              {{-- TEMPLATE ORDER ITEM --}}
              @component('cart._product-item-confirm', ['orderProduct' => null]) @endcomponent
              {{-- TEMPLATE ORDER ITEM --}}
            </div>
          </div>
          <div class="shop-data__forms">

            <div class="shop-data__forms-titles flex">
              <div class="one-form__title before-title__active">Оформление заказа</div>
              <div class="two-form__title">Быстрый заказ</div>
            </div>

            <div class="section-forms tab-content">
              <form action="#" class="item-form__one form-actived"  @submit.prevent="checkout">
                <label for="#" class="label-form pos-rel">
                  <span>Фамилия Имя</span>
                  <input type="text"
                         v-model="confirmData.name">
                  <span class="help-block" v-text="getErrorMessage(checkoutErrors, 'name')"></span>
                </label>
                <label for="#" class="label-form pos-rel">
                  <span>Телефон</span>
                  <input type="text"
                         v-model="confirmData.phone_number">
                  <span class="help-block" v-text="getErrorMessage(checkoutErrors, 'phone_number')"></span>
                </label>
                <label for="#" class="label-form">
                  <span>Email</span>
                  <input type="email"
                         v-model="confirmData.email">
                </label>
                <div class="label-form border-top">
                  <span>Перевозчик</span>
                  <select id="" v-model="confirmData.delivery_type">
                    <option value="null">Выберите перевозчика</option>
                    <option value="{{ \App\Enums\DeliveryTypes::NOVA_POSHTA }}">
                      Новая почта
                    </option>
                    <option value="{{ \App\Enums\DeliveryTypes::JUSTIN }}">
                      Justin
                    </option>
                  </select>
                </div>

                <div class="label-form" v-show="confirmData.delivery_type == DELIVERY_TYPES.NOVA_POSHTA">
                  <span>Город</span>
                  <select name="#" id="npCity">
                    <option value=null>Выберите город</option>
                  </select>
                </div>

                <div class="label-form" v-show="confirmData.delivery_type == DELIVERY_TYPES.NOVA_POSHTA">
                  <span>Отделение</span>
                  <select name="#" id="npWarehouse">
                    <option value=null>Выберите отделение</option>
                  </select>
                </div>

                <div class="label-form" v-show="confirmData.delivery_type == DELIVERY_TYPES.JUSTIN">
                  <span>Город</span>
                  <select name="#" id="justinCity">
                    <option value=null>Выберите город</option>
                  </select>
                </div>

                <div class="label-form" v-show="confirmData.delivery_type == DELIVERY_TYPES.JUSTIN">
                  <span>Отделение</span>
                  <select name="#" id="justinWarehouse">
                    <option value=null>Выберите отделение</option>
                  </select>
                </div>

                <div class="label-form border-t-b">
                  <span>Оплата</span>
                  <select name="#" id="" v-model="confirmData.payment_type">
                    <option value=null>Не выбрано</option>
                    <option value="{{ \App\Enums\PaymentTypes::CASH }}">Наложенный платеж</option>
                    <option value="{{ \App\Enums\PaymentTypes::CARD }}">Оплата на карту</option>
                  </select>
                </div>
                <div class="ordering-paragrah__btn label-form flex">
                  <button class="button-shops">Создать заказ</button>
                  <span
                    class="btn-shop-text">Подтверждение заказа, я принимаю условия пользовательского соглашения</span>
                </div>
              </form>

              <form action="#" class="item-form__two"  @submit.prevent="checkout(true)">
                <label for="#" class="label-form pos-rel">
                  <span>Фамилия Имя</span>
                  <input type="text" v-model="confirmData.name" />
                  <span class="help-block" v-text="getErrorMessage(checkoutErrors, 'name')"></span>
                </label>
                <label for="#" class="label-form pos-rel">
                  <span>Телефон</span>
                  <input type="text" v-model="confirmData.phone_number">
                  <span class="help-block" v-text="getErrorMessage(checkoutErrors, 'phone_number')"></span>
                </label>

                <div class="ordering-paragrah__btn label-form flex">
                  <button class="button-shops">Создать заказ</button>
                </div>
              </form>
            </div>

          </div>
        </div>

      </div>
    </div>

  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    var _ORDER = @json($order);
    var _PAYMENT_TYPES = @json(\App\Enums\PaymentTypes::getConstants());
    var _DELIVERY_TYPES = @json(\App\Enums\DeliveryTypes::getConstants());
  </script>

  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/cart.js') }}"></script>
@endsection
