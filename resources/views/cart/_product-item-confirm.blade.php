<?php
/* @var array $orderProduct */
?>

<div class="flex shop-data__minor-item"
     style="display: none"
     v-show="item"
     v-for="(item, index) in order.orderProducts">
  <img class="shop-data__minor-item-img" :src="item.product.backgroundPicture" alt="">
  <div class="shop-data__minor-item-name">
    <span v-text="item.product.title"></span><br>
    <span v-text="item.price"></span>
      <span>грн х</span>
    <span v-text="item.quantity"></span>
  </div>
  <div class="shop-data__minor-price">
    <span v-text="calculateAmount(item.price, item.quantity)"></span>
    <span> грн</span>
  </div>
</div>
