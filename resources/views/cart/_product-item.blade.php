<?php
/* @var array $orderProduct */
?>

<div class="basket-prod__section-items">

  <div class="prod-item"
     style="display: none"
     v-show="item"
     v-for="(item, index) in order.orderProducts">
    <div class="prod-item__img-glesses">
      <img :src="item.product.backgroundPicture" alt="" />
    </div>
    <ul class="prod-item__list">
      <li class="prod__item">
        <div>Название:<span v-text="item.product.title"></span></div>
        <p>₴ <span v-text="item.price"></span></p>
      </li>
      <li class="prod-mobile-hiden">
        <ul>
          <li class="prod__item flex-end-item" v-for="attributeOption in item.product.attributeOptions">
            <div v-text="attributeOption.attribute.title+':'">
              </div>
              <span v-text="attributeOption.title"></span>
            
          </li>

          <li class="prod__item">
            <div class="select-prod__item">
              Количество:
              <span>
                <select v-model="item.quantity" @change="sendRequestSavingItem(item, index)">
                  <option v-for="i in possibleCountNumbers(item)" :value="i" v-text="i"></option>
               </select>
              </span>
            </div>
          </li>
          <li class="prod__item">Итого к оплате: <p>₴ <span v-text="calculateAmount(item.price, item.quantity)"></span></p></li>
        </ul>
      </li>
      <li class="handler-visible-item actived">Показать полностью</li>
      <li class="prod__item-btn">
        <button class="item-btn" @click="deleteItem(item)">Удалить</button>
      </li>
    </ul>
  </div>
</div>
