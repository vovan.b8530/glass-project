@extends('layouts.app')

@section('content')
  <div id="bascketPage" class="basketPage page">
    <div class="basket-wrap">
      <div class="mcontainer">
        <div class="basket-contant">
          <div class="basket__title">Моя корзина</div>
        </div>
        @if(!empty( resourceGet($order, 'orderProducts') ))
          <div class="basket-prod-wrap">
            
              {{-- TEMPLATE ORDER ITEM --}}
              @component('cart._product-item', ['orderProduct' => null]) @endcomponent
              {{-- TEMPLATE ORDER ITEM --}}
            
            <div class="basket-prod__ordering">
              <div class="ordering-paragrah">Предметов: <span v-text="totalCount">1</span></div>
              <div class="ordering-paragrah">Итого: <span>₴ <span v-text="totalAmount"></span></span></div>
              <div class="ordering-paragrah__total-value">К оплате: <span>₴ <span v-text="totalAmount"></span></span>
              </div>
              <div class="ordering-paragrah__btn">
                <a href="{{route('cart.confirm')}}" class="button-shops">Оформление заказа</a>
              </div>
            </div>
            
            @else
              <div class="empty-basket__title">Ваша корзина пуста</div>
            @endif
            
          </div>

      </div>
    </div>

  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    var _ORDER = @json($order);
    var _PAYMENT_TYPES = @json(\App\Enums\PaymentTypes::getConstants());
    var _DELIVERY_TYPES = @json(\App\Enums\DeliveryTypes::getConstants());
  </script>

  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/cart.js') }}"></script>
@endsection

