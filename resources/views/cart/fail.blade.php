@extends('layouts.app')

@section('content')
        <div class="empty-basket__title">
          Ваш заказ #{{ resourceGet($order, 'uuid') }} не принят.{{$message}}.
        </div>
      </div>
@endsection