@extends('layouts.app')

@section('content')
        <div class="empty-basket__title">
          Ваш заказ #{{ resourceGet($order, 'uuid') }} принят. Спасибо за сотрудничество с нами.
        </div>
      </div>
@endsection