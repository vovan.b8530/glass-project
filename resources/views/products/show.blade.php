@extends('layouts.app')

@section('content')
  <div id="productItemPage" class="productItem page">

    <div class="product-item-block">
      <div class="mcontainer">
        <div class="product-block__top-contant">
          <div class="breadcrumbs">
            <ul class="flex">
              <li class="bread-item__link"><a href={{ route('site.products.index')}}>Главная страница</a></li>
              <li class="bread-item__link">
                <a href="{{ route('site.products.index', ['categoryId' => resourceGet($product, 'category.id')]) }}">
                  {{resourceGet($product, 'category.title')}}
                </a>
              </li>
              <li class="bread-item__active"> {{resourceGet($product, 'title')}}</li>
            </ul>
          </div>
        </div>

        <div class="product-block__item-body">
          <div class="item-body-gallery i--slide">
            @foreach(resourceGet($product, 'pictures', []) as $picture)
              <div class="gallery-glasses_item mcol-xs-12 mcol-sm-6">
                <div class="gallery-glasses_item-bg">
                  <img src="{{ resourceFilledOutGet($picture, 'full_link', '/img/not_photo.jpg') }}" alt="">
                </div>
              </div>
            @endforeach
          </div>
          <div class="item-body-contant">
            <div class="product-block__data-title">{{resourceGet($product, 'title')}}</div>
            <div class="product-block__data-color">
              <div class="offer-glasses_item-check-button flex">
                @if(resourceFilledOutGet($product, 'productGroup'))
                  @foreach(resourceFilledOutGet($product, 'productGroup') as $productItem)
                    <a href="{{resourceFilledOutGet($productItem, 'routeShow')}}">
                      @if($productItem['id'] == resourceFilledOutGet($product, 'id'))
                        <div class="btn-link label-one-1 bgc-el">
                        <span class="border-active-el"></span>
                      @else
                      <div class="btn-link label-one-1 bgc-el">
                      @endif
                      @if(resourceFilledOutGet($productItem, 'colorAttributeOption.backgroundPicture'))
                        <img src="{{resourceFilledOutGet($productItem, 'colorAttributeOption.backgroundPicture')}}" alt="">
                      @endif
                        </div>
                    </a>
                  @endforeach
                @endif
              </div>
            </div>
            <div class="product-block__price-block">
              <p class="product-block__price-title">Цена</p>
              <p class="product-block__price-item">{{resourceGet($product, 'price')}} грн</p>
            </div>
            <div class="product-block__btn">
              <button data-product-id="{{ resourceGet($product, 'id') }}" class="button-prod-item btn-cart--order">
                Купить
              </button>
            </div>
            <div class="product-block__description-item prod-i-js">
              <div class="description-item__title">Описание</div>
              <div class="description-item__subtitle mobile-drop-content">
                {!!resourceGet($product, 'description')!!}
              </div>

            </div>
            <div class="product-block__details-item prod-i-js">
              <div class="details-item__title">Детали</div>
              <div class="details-item-list mobile-drop-content">
                @foreach(resourceGet($product, 'attributeOptions') as $attributeOption)
                  <div class="details-item-skide">
                    <span>{{resourceFilledOutGet($attributeOption, 'attribute.title')}} {{resourceFilledOutGet($attributeOption, 'title')}}</span>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/product-order.js') }}"></script>
@endsection