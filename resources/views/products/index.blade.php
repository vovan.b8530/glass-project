<?php
/* @var \Illuminate\Pagination\LengthAwarePaginator $paginator */
/* @var \App\Models\Product[] $products */
?>

@extends('layouts.app')

@section('content')
  <div id="productsPage" class="productsPage page">

    <div class="product-top-section">
      <div class="mcontainer">
        <div class="product-banner">
          <div class="banner__title">Basic Algorithm Scripting</div>
          <div class="banner__subtitle">The first thing to do is sort the array from lower to bigger,
            just to make the code easier. This is where sort comes in,
            it needs a callback function so you have to create it.
            Once the array is sorted, then just check for the first
            number that is bigger and return the index. If there is no index for that
            number then you will have to deal with that case too.
          </div>
        </div>
      </div>
    </div>
    <div class="product-filter">
      <div class="mcontainer">
        <div class="products-filters__container">
          <aside class="attribute-sidebar descMenu">
            @if(request()->has('categoryId'))
              @widget('attributeWidget', ['categoryId' => request()->get('categoryId')])
            @endif
          </aside>
          @if(request()->has('categoryId'))
            <a href="{{ route('site.products.index', ['categoryId' => $breadCategory->id])}}"
               class="clear-product-filter">очистить фильтр</a>
          @endif
        </div>
      </div>
    </div>
    <div class="product-block">

      <div class="mcontainer">

        <div class="product-block__top-contant">
          <div class="breadcrumbs">
            <ul class="flex">
              <li class="bread-item__link"><a href="{{ route('site.products.index')}}">Главная страница</a></li>
              @if(request()->has('categoryId'))
                <li class="bread-item__active">
                  <a href="{{ route('site.products.index', ['categoryId' => $breadCategory->id]) }}">
                    {{$breadCategory->title}}
                  </a>
                </li>
              @endif
            </ul>
          </div>

          <div class="sort-select">
            <div class="sort-select__title">Сортировка по:</div>
            <div class="sort-bar">
              @include('includes.sort-form')
            </div>
          </div>
        </div>

        <div class="product-block__items-wrap">
          @foreach($products as $product)
            <div class="offer-glasses_item mcol-xs-12 mcol-sm-6 mcol-md-4">
              <div class="offer-glasses_item-img">
                <a href="{{resourceGet($product, 'routeShow')}}">
                  <img src="{{ resourceFilledOutGet($product, 'backgroundPicture', '/img/not_photo.jpg') }}" alt="">
                </a>
              </div>
              <div class="offer-glasses_item-check-button flex">
                @if(resourceFilledOutGet($product, 'productGroup'))
                  @foreach(resourceFilledOutGet($product, 'productGroup') as $productItem)
                    <a href="{{resourceFilledOutGet($productItem, 'routeShow')}}">
                      @if($productItem['id'] == resourceFilledOutGet($product, 'id'))
                        <div class="btn-link label-one-1 bgc-el">
                        <span class="border-active-el"></span>
                          @else
                          <div class="btn-link label-one-1 bgc-el">
                              @endif
                        @if(resourceFilledOutGet($productItem, 'colorAttributeOption.backgroundPicture'))
                         <img src="{{resourceFilledOutGet($productItem, 'colorAttributeOption.backgroundPicture')}}" alt="">
                        @endif
                      </div>
                    </a>
                  @endforeach
                @endif
              </div>
              <div class="offer-glasses_item-model">{{ resourceGet($product, 'title') }}</div>
              <div class="offer-glasses_item-model">{{ resourceGet($product, 'price') }} <span>₴</span></div>
              <div class="offer-glasses_item-btn">
                <a data-product-id="{{ resourceGet($product, 'id') }}" class="button btn-cart--order">Купить</a>
              </div>
            </div>
          @endforeach

        </div>

        {{ $paginator->links() }}

      </div>
    </div>
    <div class="modal-custom">
      <div class="modal-custom__overlay"></div>
    </div>

  </div>
@endsection

@section('scripts')
  <script>
    $(document).ready(function () {
      $('input.attributeOption-checkbox').on("change", function () {
        $('form.form-products-filters').trigger('submit')();
      });
      $('.sort-by').on("change", function () {
        console.log(5)
        $('.form-sort-by-price').submit();
      });
    });
  </script>

  <script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/product-order.js') }}"></script>
@endsection

