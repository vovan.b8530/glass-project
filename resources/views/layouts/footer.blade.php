<footer class="main-footer">
  <div class="mcontainer">
    <div class="footer-wrap flex spaceBetween">

      <div class="social-block ">
        <div class="footer-title-list">Подписки</div>
        <div class="social-block__links footer-list">
          <a href="#"><img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/ins-white.svg') }}" alt=""></a>
          <a href="#"><img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/fb-white.svg') }}" alt=""></a>
          <a href="#"><img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/pinterest-white.svg') }}" alt=""></a>
          <a href="#"><img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/tiktok-white.svg') }}" alt=""></a>
        </div>
      </div>

      <div class="footer-stock ">
        <div class="footer-title-list">Магазин</div>
        <ul class="footer-list">
          <a href="#">
            <li>Подарочные карты</li>
          </a>
          <a href="#">
            <li>Распродажа</li>
          </a>
          <a href="#">
            <li>Сонцезащитные очки</li>
          </a>
          <a href="#">
            <li>Очки для компьютера</li>
          </a>
        </ul>
      </div>

      <div class="footer-help ">
        <div class="footer-title-list">Помощь</div>
        <ul class="footer-list">
          <a href="#">
            <li>Ответы на вопросы</li>
          </a>
          <a href="#">
            <li>Как сделать онлайн заказ</li>
          </a>
          <a href="#">
            <li>Как выбрать очки по форме лица</li>
          </a>
        </ul>
      </div>

      <div class="about-us-footer ">
       <a href="{{ route('about') }}"><div class="footer-title-list">О нас</div></a>
        <ul class="footer-list">
          <a href="{{ route('contacts') }}"><li>Контакты</li></a>
					<a href="{{ route('faq') }}"><li>Ответы на вопросы</li></a>
					<a href="{{ route('delivery') }}"><li>Отправка и доставка</li></a>
					<a href="{{ route('warranty') }}"><li>Гарантия</li></a>
					<a href="{{ route('backProduct') }}"><li>Возврат и обмен</li></a>
        </ul>
      </div>

    </div>
  </div>
</footer>