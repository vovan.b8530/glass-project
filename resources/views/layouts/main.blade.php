<!DOCTYPE html>
<html lang="en">

<head>
{{--  <title>title</title>--}}
  <!--  -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
{{--  <meta name="description" content="">--}}
  {!! \Artesaos\SEOTools\Facades\SEOTools::generate() !!}

  <meta name="apple-mobile-web-app-capable" content="yes">

  <!-- Microsoft Tiles -->
  <meta name="msapplication-config" content="browserconfig.xml">

  <link rel="icon" type="image/png" href="{{ \EscapeWork\Assets\Facades\Asset::v('favicon.ico') }}">

  <!-- slick slider -->
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->

  <!-- Fonts -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&amp;subset=cyrillic" rel="stylesheet"> -->
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;600;700;800&display=swap"
        rel="stylesheet">

  <link rel="stylesheet" href="{{ \EscapeWork\Assets\Facades\Asset::v('css/main.min.css') }}">

  <!-- Using Select2 from city and warehouses -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>

</head>

<body>

<!--[if lte IE 11]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
  your browser</a> to improve your experience and security.</p>
<![endif]-->


<div class="mainWrapper">

  <div class="contentWrapper">
    <!--  -->
  @yield('header')
  <!--  -->

    <!-- Start of page code insertion here -->

  @yield('content')

  <!-- End of page code insertion here -->


  </div>

  @yield('footer')

</div>
<!--  -->
<!--________SCRIPTS______-->

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

<!-- slick slider -->
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/dotdotdot.min.js') }}"></script>
<!-- <script type="text/javascript" src="js/chosen.jquery.min.js"></script> -->

<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/index.min.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script type="text/javascript" src="{{ \EscapeWork\Assets\Facades\Asset::v('js/app.js') }}"></script>

@yield('scripts')
<!--  -->
</body>

</html>

