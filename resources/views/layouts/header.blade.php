<header class="main-header">
  <div class="mcontainer">
    <div class="nav-menu-wraper flex">
      <div class="menu-list">
        <div class="nav">

          <div class="menu-btn">
            <div class="line line--1"></div>
            <div class="line line--2"></div>
            <div class="line line--3"></div>
          </div>

          <div class="nav-links">
            <aside class="category-sidebar descMenu">
              @widget('categoryWidget')
            </aside>
            <div class="mobile-menu__contacts">
              <div class="mobile-menu__contacts-phone">
                <img class="contacts-phone__img" src="{{ \EscapeWork\Assets\Facades\Asset::v('img/icon-phone.svg') }}"
                     alt="">
                <a href="tel:+380989253060" class="contacts-phone__number">+38(098)-925-30-60</a>
              </div>
              <div class="mobile-menu__social-block social-block">
                <div class="social-block__title">Следите за нами</div>
                <div class="social-block__links">
                  <a href="#"><img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/ins.svg') }}" alt=""></a>
                  <a href="#"><img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/fb.svg') }}" alt=""></a>
                  <a href="#"><img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/pinterest.svg') }}" alt=""></a>
                  <a href="#"><img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/tiktok.svg') }}" alt=""></a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="header-logo">
        <a href="{{route('site.home')}}"><img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/logo.png') }}"
                                              alt=""></a>
      </div>

      @include('includes.search-form')

      <div class="account-block flex">
        <!-- <div class="account-clients">
          <a href="#">
            <img src="/img/account-icon.svg" alt="">
          </a>
        </div> -->
        <div class="basket-block">
          <a href="{{route('cart.index')}}">
            <img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/basket-icon.svg') }}" alt="">
          </a>
        </div>
      </div>
    </div>
  </div>
</header>