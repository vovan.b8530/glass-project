@extends('layouts.app')

@section('content')
  <div id="contactPage" class="contactPage page">

    <div class="mcontainer">
      <div class="common-page-title">Контакты</div>
      <div class="contact-content-wrap">
        <div class="contact-column-item">
          <div class="contact-column-item_title"><img src="./img/phone-contact.svg" alt="">
            <p>Позвоните нам</p></div>
          <div class="contact-el-item"><a href="tel:+38 098 925 30 60">+38 098 925 30 60</a></div>
          <div class="contact-el-item suptext">звонок по тарифам Вашего оператора</div>
        </div>
        <div class="contact-column-item">
          <div class="contact-column-item_title"><img src="./img/contact-messenge.svg" alt="">
            <p>Связь с нами</p></div>
          <div class="contact-el-item"><img src="./img/viber-contact.svg" alt=""><span
              class="viber-contact">Viber </span>
            <p>+3080978145399</p></div>
          <div class="contact-el-item"><img src="./img/tel-contact.svg" alt=""><span class="tl-contact">Telegram </span>
            <p>+3080978145399</p></div>
          <div class="contact-el-item"><img src="./img/fb-contact.svg" alt=""><span class="fb-contact">Facebook</span>
            <p></p></div>
          <div class="contact-el-item"><img src="./img/tiktok-contact.svg" alt=""><span
              class="tiltok-contact">TikTok </span>
            <p>@glasses.house</p></div>
          <div class="contact-el-item"><img src="./img/inst-contact.svg" alt=""><span
              class="inst-contact">Instagram </span>
            <p>@glasses.house.ukraine</p></div>
        </div>
        <div class="contact-column-item">
          <div class="contact-column-item_title"><img src="./img/school-calendar.svg" alt="">
            <p>График работы</p></div>
          <div class="contact-el-item"><p>Ежедневно с 9:00 до 18:00</p></div>
        </div>
      </div>

    </div>

  </div>
@endsection