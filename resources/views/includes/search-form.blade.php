<form action="{{ route('site.products.index') }}" method="GET" class="head-btn-search">
  <input name="q" type="search" aria-label="Search" value="{{ $q ?? '' }}">
    <img class="btn-search" src="{{ \EscapeWork\Assets\Facades\Asset::v('img/icon-search.svg') }}" alt="">
</form>