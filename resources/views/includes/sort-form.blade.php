<form action="{{  url()->full()  }}" method="GET" class="form-sort-by-price">
  @foreach(request()->all() as $key => $value)
    @if(!is_array($value) && $key != 'price')
      <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
    @elseif($key != 'price')
      @foreach($value as $item)
        <input type="hidden" name="{{ $key }}[]" value="{{ $item }}"/>
      @endforeach
    @endif
  @endforeach
  <select name="price" class="sort-by">
    <option value="">Выбирите сортировку</option>
    <option value="asc" @if (isset($_GET['price']) && $_GET['price'] == 'asc') selected @endif>Цена по возростанию
    </option>
    <option value="desc" @if(isset($_GET['price']) && $_GET['price'] == 'desc')  selected @endif>Цена по убыванию
    </option>
  </select>
</form>

