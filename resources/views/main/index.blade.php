<?php
/* @var \App\Models\Product[] $products */
?>

@extends('layouts.app')

@section('content')
  <div id="homePage" class="homePage page">

    <div class="banner-section">
      <div class="mcontainer">
        <div class="banner-section__container">
          <div class="banner-section__img">
            <img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/home-banner-top.png') }}" alt="">
          </div>
          <div class="banner-section__container-info">
            <p class="banner-section__container-text">Найди свою идеальную пару</p>
            <div class="banner-section__container-btn wrap-buton">
              <a class="button" href="{{route('site.products.index')}}"> В магазин</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="glases-style">
      <div class="mcontainer">
        <div class="glases-style__wrap">
          <div class="glases-style__title">Лучшие очки для твоего стиля</div>
          <div class="glases-style__subtitle">
            Очки это не только хорошая защита от солнечных лучей или же синего света экранов.
            В первую очередь это аксессуар который выделяет тебя и позволяет завершить твой ежедневный образ. С нами
            ты всегда
            сможешь оставаться в тренде и безопасности для своих глаз.
          </div>
          <div class="glasses-style__btn wrap-buton">
            <a class="button" href="{{route('site.products.index')}}"> В магазин</a>
          </div>

          <div class="offer-glasses flex wrap">
            @foreach($products as $product)
              <div class="offer-glasses_item mcol-xs-12 mcol-sm-6 mcol-md-4">
                <div class="offer-glasses_item-img">
                  <a href="{{resourceGet($product, 'routeShow')}}">
                    <img src="{{ resourceFilledOutGet($product, 'backgroundPicture', '/img/not_photo.jpg') }}" alt="">
                  </a>
                </div>
                <div class="offer-glasses_item-check-button flex">
                  @if(resourceFilledOutGet($product, 'productGroup'))
                    @foreach(resourceFilledOutGet($product, 'productGroup') as $productItem)
                      <a href="{{resourceFilledOutGet($productItem, 'routeShow')}}">
                        @if($productItem['id'] == resourceFilledOutGet($product, 'id'))
                          <div class="btn-link label-one-1 bgc-el">
                            <span class="border-active-el"></span>
                            @else
                              <div class="btn-link label-one-1 bgc-el">
                                @endif
                          @if(resourceFilledOutGet($productItem, 'colorAttributeOption.backgroundPicture'))
                           <img src="{{resourceFilledOutGet($productItem, 'colorAttributeOption.backgroundPicture')}}" alt="">
                          @endif
                        </div>
                      </a>
                    @endforeach
                  @endif
                </div>
                <div class="offer-glasses_item-model">{{resourceFilledOutGet($product, 'title')}} <span>{{ resourceGet($product, 'price') }} ₴ </span></div>
              </div>
            @endforeach

          </div>
        </div>
      </div>
    </div>
    <div class="temporary-offers">
      <div class="mcontainer">
        <div class="offer-card-wrap flex wrap">

          <div class="offer-card__item mcol-xs-12 mcol-md-4">

            <div class="offer-card-wrap__item-img">
              <img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/baner-offer-new.png') }}" alt="">
            </div>
            <div class="offer-card-wrap-contant">
              <div class="offer-card-wrap__item-title">Новое</div>
              <div class="offer-card-wrap__item-subtitle">Будьте в курсе всех последних тенденций и новинок</div>
            </div>
            <div class="offer-card-wrap__item-btn wrap-buton">
              <a class="button" href="{{route('site.products.index')}}"> В магазин</a>
            </div>

          </div>

          <div class="offer-card__item mcol-xs-12 mcol-md-4">

            <div class="offer-card-wrap__item-img">
              <img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/baner-offer-sale.png') }}" alt="">
            </div>
            <div class="offer-card-wrap-contant">
              <div class="offer-card-wrap__item-title">Скидки</div>
              <div class="offer-card-wrap__item-subtitle">Получите скидку до 50% на многие аксессуары</div>
            </div>
            <div class="offer-card-wrap__item-btn wrap-buton">
              <a class="button" href="{{route('site.products.index')}}"> В магазин</a>
            </div>

          </div>

          <div class="offer-card__item mcol-xs-12 mcol-md-4">

            <div class="offer-card-wrap__item-img">
              <img src="{{ \EscapeWork\Assets\Facades\Asset::v('img/baner-offer-garant.png') }}" alt="">
            </div>
            <div class="offer-card-wrap-contant">
              <div class="offer-card-wrap__item-title">Гарантии</div>
              <div class="offer-card-wrap__item-subtitle">При покупке гарантия 14 дней</div>
            </div>
            <div class="offer-card-wrap__item-btn wrap-buton">
              <a class="button" href="{{route('site.products.index')}}"> Подробнее</a>

            </div>

          </div>
        </div>
      </div>
    </div>
    <div class="bottom-banner">
      <div class="mcontainer">
        <div class="bottom-banner-wrap slide">

          <div class="bottom-banner-item">
            <div class="bottom-banner-item__img banner-b-t">
              <img class="" data-defoult-img="./img/banner-1-bottom.jpg" data-mobile="./img/mobile-banner-bottom.png"
                   src="{{ \EscapeWork\Assets\Facades\Asset::v('img/banner-1-bottom.jpg') }}" alt="">
            </div>
            <div class="bottom-banner-item__contant b-t">
              <div class="bottom-banner-btn wrap-buton">
                <a class="button" href="{{route('site.products.index')}}"> Выбрать</a>
              </div>
            </div>
          </div>

          <div class="bottom-banner-item">
            <div class="bottom-banner-item__img banner-b-bd">
              <img class="" data-defoult-img="./img/banner-2-bottom.jpg" data-mobile="./img/mobile-banner-bottom.png"
                   src="{{ \EscapeWork\Assets\Facades\Asset::v('img/banner-2-bottom.jpg') }}" alt="">
            </div>
            <div class="bottom-banner-item__contant b-bd">
              <div class="bottom-banner-btn wrap-buton">
                <a class="button" href="{{route('site.products.index')}}"> Выбрать</a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
@endsection