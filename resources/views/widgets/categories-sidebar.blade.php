<?php
/* @var \App\Models\Category[] $categories */
/* @var \App\Models\SubCategory $subcategory */
?>

<ul class="item-list">
  @foreach($categories as $category)
    <li class="icon-plus item-menu">
      <a href="{{ route('site.products.index', ['categoryId' => $category->id]) }}"
         class="link">{{$category->title}}</a>
      <ul class="subitem-hiden">
        @foreach($category->subCategories as $subcategory)
          <li>
            <a href="{{$subcategory->attribute_option_url}}">{{$subcategory->title}}</a>
          </li>
        @endforeach
      </ul>
    </li>
  @endforeach
</ul>