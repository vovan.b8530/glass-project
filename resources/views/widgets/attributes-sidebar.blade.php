<?php
/* @var \App\Models\AttributeOption[] $attributes */
/* @var \App\Models\AttributeOption $attributeOption */
/* @var array $selected */
?>

<form method="GET" action="{{ url()->full() }}" class="form form-products-filters">
  @foreach(request()->all() as $key => $value)
    @if(!is_array($value))
      <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
    @elseif($key != 'attributeOptionsIds')
      @foreach($value as $item)
        <input type="hidden" name="{{ $key }}[]" value="{{ $item }}"/>
      @endforeach
    @endif
  @endforeach
  <div class="products-filter-wrap flex">
    @foreach($attributes as $attribute)
      @if($attribute->isColor())
        <div class="acc-item colors">


                  <span class="acc-button">
                      {{$attribute->title}}
                  </span>
                  <div class="acc-contant">
                    <div class="prod-wrap">
                      @foreach($attribute->attributeOptions as $attributeOption)
                        <div class="form-row-secondary">
                          <div class="checkbox-item">
                            <label class="checkbox-label">
                              <input class="form-checkbox attributeOption-checkbox"
                                     type="checkbox"
                                     name="attributeOptionsIds[]"
                                     @if(in_array($attributeOption->id, $selected)) checked @endif
                                     value="{{ $attributeOption->id }}">
                              <div class="form-checkbox-subtitle">
                                  @isset($attributeOption->picture_file_name)
                                    <img src="{{$attributeOption->assetAbsolute($attributeOption->picture_file_name)}}"
                                         alt="">
                                  @endisset
                                </div>
                                <div class="contant-label">
                                  <div>
                                </div>
                                <div>{{$attributeOption->title}}</div>
                              </div>
                            </label>
                          </div>
                        </div>
                      @endforeach
                    </div>
                  </div>


          @else
            <div class="acc-item flex-wrap-filter">


              <span class="acc-button">
              {{$attribute->title}}
               </span>
              <div class="acc-contant">
                <div class="prod-wrap">
                  @foreach($attribute->attributeOptions as $attributeOption)
                    <div class="form-row-secondary">
                      <div class="checkbox-item">
                        <label class="checkbox-label">
                          <input class="form-checkbox attributeOption-checkbox"
                                 type="checkbox"
                                 name="attributeOptionsIds[]"
                                 @if(in_array($attributeOption->id, $selected)) checked @endif
                                 value="{{ $attributeOption->id }}">
                          <div class="form-checkbox-subtitle">

                            </div>
                            <div class="contant-label">
                              <div>
                                @isset($attributeOption->picture_file_name)
                                  <img src="{{$attributeOption->assetAbsolute($attributeOption->picture_file_name)}}"
                                       alt="">
                                @endisset
                            </div>
                            <div>{{$attributeOption->title}}</div>
                          </div>
                        </label>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>


       @endif


           {{--   <span class="acc-button">
            {{$attribute->title}}
        </span>
              <div class="acc-contant">
                <div class="prod-wrap">
                  @foreach($attribute->attributeOptions as $attributeOption)
                    <div class="form-row-secondary">
                      <div class="checkbox-item">
                        <label class="checkbox-label">
                          <input class="form-checkbox attributeOption-checkbox"
                                 type="checkbox"
                                 name="attributeOptionsIds[]"
                                 @if(in_array($attributeOption->id, $selected)) checked @endif
                                 value="{{ $attributeOption->id }}">
                          <div class="form-checkbox-subtitle">

                            </div>
                            <div class="contant-label">
                              <div>
                                @isset($attributeOption->picture_file_name)
                                  <img src="{{$attributeOption->assetAbsolute($attributeOption->picture_file_name)}}"
                                       alt="">
                                @endisset
                            </div>
                            <div>{{$attributeOption->title}}</div>
                          </div>
                        </label>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>--}}
            </div>
            @endforeach
        </div>
        <button id="refreshAttributeOptionSidebar" type="submit" style="display: none"></button>
  </div>
</form>

